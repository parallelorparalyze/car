﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Car
{
    class PerformanceCalculator
    {
        private long lastTime = 0;
        private long timeElapsed = 0;
        int currentFPS = 0;
        private ArrayList perSecond = new ArrayList();
        private ArrayList total = new ArrayList();
        public PerformanceDTO getPerformance()
        {
            if(total.Count > 0)
            {
                long avg = 0;
                foreach (long t in total)
                {
                    avg += t;
                }
                return new PerformanceDTO(avg / total.Count,currentFPS);
            }
            return new PerformanceDTO((long)0,currentFPS);
        }

        public void tick()
        {
            var unixTime = DateTime.Now.ToUniversalTime() -
                new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            long dT = 0;

            if (lastTime != 0)
            {
                dT = (long)unixTime.TotalMilliseconds - lastTime;
                perSecond.Add(dT);
            }

            if (timeElapsed >= 1000)
            {
                timeElapsed = 0;
                long tot = 0;
                long avg = 0;
                foreach (long t in perSecond)
                {
                    tot += t;
                }
                avg = (int)(tot / perSecond.Count);
                currentFPS = perSecond.Count;
                total.Add(avg);
                perSecond.Clear();
            }
            lastTime = (long)unixTime.TotalMilliseconds;
            timeElapsed += dT;
        }
    }
}
