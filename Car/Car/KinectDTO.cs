﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SkeletonPoint = Microsoft.Kinect.SkeletonPoint;

namespace Car
{
    class KinectDTO
    {
        public SkeletonPoint position;
        public bool stop = false;
        public int[] depthList;
        public KinectDTO(SkeletonPoint _pos)
        {
            position = _pos;
        }
        public KinectDTO(bool _stop)
        {
            stop = _stop;
        }
        public KinectDTO(int[] _list)
        {
            depthList = _list;
        }
    }
}
