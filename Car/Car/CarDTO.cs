﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Car
{
    class CarDTO
    {
        public Tuple<float, float, float> position;
        public bool stop = false;
        public bool waypointReached = false;
        public CarDTO(Tuple<float,float,float> _pos)
        {
            position = _pos;
        }
        public CarDTO(bool _stop,bool _wr)
        {
            stop = _stop;
            waypointReached = _wr;
        }
    }
}
