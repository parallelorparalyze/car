﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;

namespace Car
{
    /// <summary>
    /// Interaction logic for CarControl.xaml
    /// </summary>
    /// 
    public partial class CarControl : Window
    {

        private System.Timers.Timer timer;
        public CarControl()
        {
            InitializeComponent();
        }

        private Controller controller = new Controller();
        private Thread controllerThread;

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            timer = new System.Timers.Timer(1000);
            timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            timer.Enabled = true;
            
            controller.initialize();

            controllerThread = new Thread(new ThreadStart(controller.run));
            controllerThread.Start();

        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                controllerThread.Abort();
                controllerThread.Join();
            }
            catch (Exception)
            {

            }

            controller.close();
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal,
                new System.Windows.Forms.MethodInvoker(delegate()
                {
                    this.carPerformance.Text = "Car Performance: " + controller.getCarPerformance();
                    this.controllerPerformance.Text = "Controller Performance: " + controller.getControllerPerformance();
                    this.kinectPerformance.Text = "Kinect Performance: " + controller.getKinectReaderPerformance();
                }
            ));
        }


    }
}
