﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Car
{
    class PerformanceDTO
    {
        public long total = 0;
        public int FPS = 0;

        public PerformanceDTO(long _total, int _fps)
        {
            total = _total;
            FPS = _fps;
        }

        public override String ToString()
        {
            return "FPS: " + FPS + " Mean DT: " + total;
        }
    }
}
