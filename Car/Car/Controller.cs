﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Car
{
    class Controller
    {

        private KinectReader kinect = new KinectReader();
        private Thread kinectThread;

        private CarCommunication comm = new CarCommunication();
        private Thread carThread;

        private PerformanceCalculator pCalc = new PerformanceCalculator();

        private List<Tuple<float, float, float>> waypoints = new List<Tuple<float, float, float>>();
        private const double probeAngle = 2; // angle to check, left and right, of the goal for obstacles
        private const double probeDist = 600;
        private const double dodgeMult = 1;
        private const int numPix = 2;
        private bool ready = true;
        private int[] depth = null;
        private double avoidAngle = 45;

        public void initialize()
        {
            comm.initialize();
            kinect.initialize();
            kinectThread = new Thread(new ThreadStart(kinect.run));
            carThread = new Thread(new ThreadStart(comm.run));
            kinectThread.Start();
            carThread.Start();
        }

        public void close()
        {
            try
            {
                kinectThread.Abort();
                carThread.Abort();
                kinectThread.Join();
                carThread.Join();
            }
            catch (Exception)
            {
            }
        }
        
        public String getCarPerformance()
        {
            return comm.getPerformance().ToString();
        }
        
        public String getControllerPerformance()
        {
            return getPerformance().ToString();
        }
        
        public String getKinectReaderPerformance()
        {
            return kinect.getPerformance().ToString();
        }

        public PerformanceDTO getPerformance()
        {
            return pCalc.getPerformance();
        }

        public void run()
        {
            Tuple<float, float, float> pos =  Tuple.Create<float,float,float>(0,0,0);
            float eps = .05f; // person must move a distance of meters to give a new command
            while (true)
            {
                KinectDTO data = kinect.getData();
                CarDTO carData = comm.getData();
                if (carData != null && carData.waypointReached)
                {
                    ready = true;
                }
                if (ready && waypoints.Count > 0)
                {
                    Tuple<float, float, float> wp = waypoints.ElementAt(0);
                    comm.addData(new CarDTO(wp));
                    waypoints.RemoveAt(0);
                    ready = false;
                }
                if (data != null && data.position != null)
                {
                    if (data.stop)
                    {
                        comm.addData(new CarDTO(true,false));
                    }
                    else if (data.depthList != null)
                    {
                        depth = data.depthList;
                    }
                    else if (data.position != null &&
                        pos.Item1 >= (data.position.X + eps) || pos.Item1 <= (data.position.X - eps) ||
                        pos.Item2 >= (data.position.Y + eps) || pos.Item2 <= (data.position.Y - eps) ||
                        pos.Item3 >= (data.position.Z + eps) || pos.Item3 <= (data.position.Z - eps))
                    {
                        pos = Tuple.Create<float, float, float>(data.position.X, data.position.Y, data.position.Z);
                        /*if (depth != null)
                        {
                            findPath(depth, pos);
                        }*/
                        comm.addData(new CarDTO(pos));
                    }
                }
                pCalc.tick();
            }
        }

        private void findPath(int[] depthList, Tuple<float, float, float> pos)
        {
            // calculate distance and angle to person
            double dist = Math.Sqrt(Math.Pow(pos.Item1, 2) + Math.Pow(pos.Item3, 2)) * 1000;
            double theta = Math.Atan2(pos.Item1, pos.Item3) * 180 / Math.PI;
            //double angle = (KinectReader.horiFOV / 2) - theta;
            bool objectFound = false;
            double objDist = KinectReader.maxDepth;
            // detect object in direct path to goal
            int left = (int)(((theta - probeAngle + (KinectReader.horiFOV / 2)) / KinectReader.horiFOV) * KinectReader.formatWidth);
            int right = (int)(((theta + probeAngle + (KinectReader.horiFOV / 2)) / KinectReader.horiFOV) * KinectReader.formatWidth);
            if (right >= KinectReader.formatWidth)
            {
                right = KinectReader.formatWidth - 1;
            }
            if (left < 0)
            {
                left = 0;
            }
            int found = 0;
            for (int i = left; i < right; i++)
            {
                if (depthList[i] > 0 && depthList[i] < (dist - probeDist))
                {
                    found++;
                    if (found >= numPix)
                    {
                        objectFound = true;
                    }
                    if (depthList[i] < objDist)
                    {
                        objDist = depthList[i];
                    }
                }
            }

            // if object, find alternate route
            if (objectFound)
            {
                double avgLeft = 0;
                double avgRight = 0;
                int num = 0;
                for (int i = 0; i < left; i++)
                {
                    if (depthList[i] != 0)
                    {
                        num++;
                        avgLeft += depthList[i];
                    }
                }
                if (num == 0)
                {
                    avgLeft = KinectReader.maxDepth;
                }
                else
                {
                    avgLeft = avgLeft / num;
                }
                num = 0;
                for (int i = right; i < KinectReader.formatWidth; i++)
                {
                    if (depthList[i] != 0)
                    {
                        num++;
                        avgRight += depthList[i];
                    }
                }
                if (num == 0)
                {
                    avgRight = KinectReader.maxDepth;
                }
                else
                {
                    avgRight = avgRight / num;
                }
                if (avgRight < avgLeft)
                {
                    // more objects found right, GO LEFT
                    if(waypoints.Count == 0)
                    {
                        double x1, z1, x2, z2, nD;
                        double a = theta - avoidAngle;
                        nD = (dist / 2) / Math.Sin(avoidAngle * Math.PI / 180);
                        x1 = Math.Sin(a * Math.PI / 180) * nD * dodgeMult;
                        z1 = Math.Cos(a * Math.PI / 180) * nD * dodgeMult;
                        x2 = Math.Sin(avoidAngle * Math.PI / 180) * dist * dodgeMult;
                        z2 = (Math.Cos(avoidAngle * Math.PI / 180) * dist - nD) * dodgeMult;
                        waypoints.Add(Tuple.Create<float, float, float>((float)x1, 0, (float)z1 / 2));
                        waypoints.Add(Tuple.Create<float, float, float>(0, 0, (float)z1 / 2));
                        waypoints.Add(Tuple.Create<float, float, float>((float)x2, 0, (float)z2 / 2));
                        waypoints.Add(Tuple.Create<float, float, float>(0, 0, (float)z2 / 2));
                        Console.Write("DODGING RIGHT!");
                    }
                }
                else
                {
                    // more objects found left
                    if (waypoints.Count == 0)
                    {
                        double x1, z1, x2, z2, nD;
                        double a = theta + avoidAngle;
                        nD = (dist / 2) / Math.Sin(avoidAngle * Math.PI / 180);
                        x1 = Math.Sin(a * Math.PI / 180) * nD * dodgeMult;
                        z1 = Math.Cos(a * Math.PI / 180) * nD * dodgeMult;
                        x2 = Math.Sin(avoidAngle * Math.PI / 180) * dist * dodgeMult;
                        z2 = (Math.Cos(avoidAngle * Math.PI / 180) * dist - nD) * dodgeMult;
                        waypoints.Add(Tuple.Create<float, float, float>((float)x1, 0, (float)z1 / 2));
                        waypoints.Add(Tuple.Create<float, float, float>(0, 0, (float)z1 / 2));
                        waypoints.Add(Tuple.Create<float, float, float>((float)x2, 0, (float)z2 / 2));
                        waypoints.Add(Tuple.Create<float, float, float>(0, 0, (float)z2 / 2));
                        Console.Write("DODGING LEFT!");

                    }
                }
            }
            else
            {
                // no objects found
                if (waypoints.Count == 0)
                {
                    waypoints.Add(pos);
                }
            }
        }
    }
}
