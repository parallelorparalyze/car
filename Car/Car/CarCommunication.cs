﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Concurrent = System.Collections.Concurrent;
using System.Threading;

namespace Car
{
    class CarCommunication
    {

        private const double distThreshold = 1.0f; // m
        private const int sleepTime = 100; // ms
        private const double leftRadius = .7366f; // in m. Left is +x
        private const double rightRadius = .7366f; // in m. Right is -x
        private const double leftLargeRadius = 2 * leftRadius;
        private const double rightLargeRadius = 2 * rightRadius;
        private const double angleLargeThreshold = 15.0 * Math.PI / 180;
        private const double angleSmallThreshold = 8.0 * Math.PI / 180;
        private const float distReduction = .20f;
        private const float angleDistReduction = 3f;
        private const int brakeTime = 25;
        private const int initialAccel = 20;
        private const int buffSize = 150;
        private const int maxCMDistance = 126;  // maximum distance to send in one byte
        private Boolean ready = true;
        private String msgBuffer = "";
        private Tuple<float,float,float> lastPosition = null;
        private Boolean tightTurn;
        private Tuple<float, float, float> lastSeen = null;
        private bool running = false;

        private class Instruction
        {
            private Tuple<byte, byte> data;

            public void setData(Boolean _forward, Boolean _turn, Boolean _left, Boolean _override, Boolean _tight,Boolean _brake, SByte _distance)
            {
                byte one = 0;
                byte two = (byte)(_distance);
                if (_forward)
                    one |= 0x10;
                if (_turn)
                    one |= 0x20;
                if (_left)
                    one |= 0x40;
                if (_override)
                    one |= 0x08;
                if (_tight)
                    one |= 0x02;
                if (_brake)
                    one |= 0x04;
                two |= 0x01;
                two &= 0x7F;
                data = Tuple.Create<byte, byte>(one, two);
            }

            public String encode()
            {
                return "" + (char)data.Item1 + (char)data.Item2;
            }

        }

        private System.IO.Ports.SerialPort port;
        private Concurrent.ConcurrentQueue<CarDTO> inQueue;
        private Concurrent.ConcurrentQueue<CarDTO> outQueue;
        private Concurrent.ConcurrentQueue<Boolean> selfQueue;
        private List<Instruction> instructions;
        private List<Tuple<float, float, float>> waypoints;
        private PerformanceCalculator pCalc = new PerformanceCalculator();

        public void initialize()
        {
            try
            {
                this.port = new System.IO.Ports.SerialPort();
                this.port.PortName = "COM13";
                this.port.BaudRate = 57600;
                this.port.Parity = System.IO.Ports.Parity.None;
                this.port.DataBits = 8;
                this.port.StopBits = System.IO.Ports.StopBits.One;
                this.port.Handshake = System.IO.Ports.Handshake.None;
                this.port.RtsEnable = true;
                this.port.DtrEnable = true;
                this.port.ReadTimeout = 500;
                this.port.WriteTimeout = 500;
                this.port.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.port_DataReceived);
            }
            catch (Exception)
            {

            }

            try
            {
                this.port.Open();
                this.port.DiscardInBuffer();
                this.port.DiscardOutBuffer();
            }
            catch
            {
                Console.WriteLine("Exception opening port!");
            }

            inQueue = new Concurrent.ConcurrentQueue<CarDTO>();
            outQueue = new Concurrent.ConcurrentQueue<CarDTO>();
            instructions = new List<Instruction>();
            waypoints = new List<Tuple<float, float, float>>();
            selfQueue = new Concurrent.ConcurrentQueue<Boolean>();

        }

        public PerformanceDTO getPerformance()
        {
            return pCalc.getPerformance();
        }

        public void run()
        {
            while (true)
            {
                CarDTO data = getInData();
                Boolean r;
                try
                {
                    selfQueue.TryDequeue(out r);
                    if (r != null && r)
                    {
                        ready = true;
                        outQueue.Enqueue(new CarDTO(false, true));
                    }
                }
                catch (Exception)
                {
                }
                if (data != null)
                {
                    if (data.stop)
                    {
                        Console.WriteLine("BRAKING!");
                        ready = false;
                        Instruction i = new Instruction();
                        i.setData(true, false, false, true, false, false, Convert.ToSByte(10));
                        instructions.Add(i); 
                        i = new Instruction();
                        i.setData(true, false, false, false, false, true, Convert.ToSByte(25));
                        instructions.Add(i);
                        i = new Instruction();
                        i.setData(false, false, false, false, false, false, Convert.ToSByte(5));
                        instructions.Add(i);
                        transmit(instructions);
                    }
                    if (data.position != null && (ready )) //|| tightTurn))
                    {
                        // temporary, use waypoints later to compensate for pathfinding
                        ready = false;
                        createPathToWaypoint(data.position);
                        transmit(instructions);
                    }
                    
                }
                pCalc.tick();
                //Thread.Sleep(sleepTime); // do something better later
            }

        }

        public void close()
        {
            if (this.port.IsOpen)
            {
                this.port.Close();
            }
        }

        public CarDTO getData()
        {
            try
            {
                CarDTO first;
                outQueue.TryDequeue(out first);
                return first;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void addData(CarDTO _data)
        {
            inQueue.Enqueue(_data);
        }

        private CarDTO getInData()
        {
            try
            {
                CarDTO first;
                inQueue.TryDequeue(out first);
                return first;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void addOutData(CarDTO _data)
        {
            outQueue.Enqueue(_data);
        }

        private void createPathToWaypoint(Tuple<float, float, float> _pos)
        {
            /*if (Math.Sqrt((_pos.Item1 * _pos.Item1) + (_pos.Item3 * _pos.Item3)) >= distThreshold)
            {
                return;
            }
            */

            // find turning distance
            double r = rightRadius;
            bool tightTurn = false;
            bool turnLeft = false;
            if (_pos.Item1 > 0)
            {
                // turn left
                r = leftRadius;
                turnLeft = true;
            }
            /*
            Tuple<float,float> biased = Tuple.Create<float,float>((Math.Abs(_pos.Item1)-r)/r,Math.Abs(_pos.Item3)/r);
            
            double u;
            Tuple<double,double> t1, t2,tan;

            //find tangent points to turning circle
            u = (biased.Item1 + Math.Sqrt(Math.Pow(biased.Item1, 2) * Math.Pow(biased.Item2, 2) + Math.Pow(biased.Item2, 4) - Math.Pow(biased.Item2, 2))) / (Math.Pow(biased.Item1, 2) + Math.Pow(biased.Item2, 2));
            t1 = Tuple.Create<double,double>(u,Math.Sqrt(1-Math.Pow(u,2)));
            u = (biased.Item1 - Math.Sqrt(Math.Pow(biased.Item1, 2) * Math.Pow(biased.Item2, 2) + Math.Pow(biased.Item2, 4) - Math.Pow(biased.Item2, 2))) / (Math.Pow(biased.Item1, 2) + Math.Pow(biased.Item2, 2));
            t2 = Tuple.Create<double,double>(u,Math.Sqrt(1-Math.Pow(u,2)));

            // find closer tangent
            tan = t1;
            if(Math.Sqrt(Math.Pow(t2.Item1,2)+Math.Pow(t2.Item2,2)) < Math.Sqrt(Math.Pow(t1.Item1,2)+Math.Pow(t1.Item2,2)))
            {
                tan = t2;
            }

            double c = Math.Sqrt(Math.Pow((tan.Item1+r)*r,2)+Math.Pow(tan.Item2*r,2)); // distance from 0,0,0
            double theta = Math.Asin(c/(2*r))*2;
            double dist = Math.Sqrt(Math.Pow((tan.Item1-biased.Item1)*r,2)+Math.Pow((tan.Item2-biased.Item2)*r,2));
            */
            //double dist;
            double curveDist = 0;
            double straightDist = 0;
            double theta = Math.Atan2(_pos.Item1, _pos.Item3);
            double brake = 0;
            Instruction i;
            instructions.Clear();

            // We need to go strait. Use default distance. No other calculations.
            // TODO: Base this on angle rather than ditances!!!!
            if(Math.Abs(theta) < angleSmallThreshold)   // just straight
            {
                /*dist = .25 * (_pos.Item3); // quarter of a run for now! 
                instructions.Clear();
                i.setData(true, false, false, true, Convert.ToSByte(dist * 5), true); // converting m to cm
                instructions.Add(i);
                */
                straightDist = Math.Sqrt(Math.Pow(_pos.Item1,2) + Math.Pow(_pos.Item3,2));
                running = true;


                curveDist = curveDist * angleDistReduction * 100;
                straightDist = straightDist * 100 * distReduction;
                i = new Instruction();
                if (straightDist > maxCMDistance)
                {
                    straightDist = maxCMDistance;
                }
                i.setData(true, false, false, true, false, false, Convert.ToSByte(straightDist));
                instructions.Add(i);
                i = new Instruction();
                i.setData(false, false, false, false, false, false, Convert.ToSByte(2));
                instructions.Add(i);
            }
            else if (Math.Abs(theta) < angleLargeThreshold)  // turn and then straight
            {
                /*double curveDist = 2 * r * (theta * .5 / Math.PI);
                dist = .25 * (Math.Sqrt(Math.Pow(_pos.Item1, 2) + Math.Pow(_pos.Item3, 2)) - (2 * r )); // quarter of a run for now!
                Boolean turnLeft = false;
                if (_pos.Item1 > 0)
                {
                    turnLeft = true;
                }
                instructions.Clear();
                i.setData(true, false, false, true, Convert.ToSByte(dist * 5), true); // converting m to cm
                instructions.Add(i);
                i = new Instruction();
                i.setData(true, true, turnLeft, false, Convert.ToSByte(Math.Abs(curveDist * 500)), false); // converting m to cm
                instructions.Add(i);
                i = new Instruction();
                i.setData(true, true, turnLeft, false, Convert.ToSByte(Math.Abs(curveDist * 500)), false); // converting m to cm
                instructions.Add(i);*/
                
                // use small radius
                
                if (_pos.Item1 > 0)
                {
                    r = leftLargeRadius;
                }
                else
                {
                    r = rightLargeRadius;
                }
                running = true;
                curveDist = Math.Abs(r * (theta * .5 / Math.PI));
                straightDist = (Math.Sqrt(Math.Pow(_pos.Item1, 2) + Math.Pow(_pos.Item3, 2)) - r);


                curveDist = curveDist * angleDistReduction * 100;
                straightDist = straightDist * 100 * distReduction;
                i = new Instruction();
                if (straightDist > maxCMDistance)
                {
                    straightDist = maxCMDistance;
                }
                if (curveDist > maxCMDistance)
                {
                    curveDist = maxCMDistance;
                }
                i.setData(true, true, turnLeft, true, false, false, Convert.ToSByte(curveDist));
                instructions.Add(i);
                i = new Instruction();
                i.setData(true, false, false, false, false, false, Convert.ToSByte(straightDist));
                instructions.Add(i);
                i = new Instruction();
                i.setData(false, false, false, false, false, false, Convert.ToSByte(2));
                instructions.Add(i);
            }
            else    // just turn
            {
                /*double curveDist = r * (theta * .5 / Math.PI);
                dist = .25 * (Math.Sqrt(Math.Pow(_pos.Item1, 2) + Math.Pow(_pos.Item3, 2)) - r); // quarter of a run for now!
                Boolean turnLeft = false;
                if (_pos.Item1 > 0)
                {
                    turnLeft = true;
                }
                instructions.Clear();
                i.setData(true, false, false, true, Convert.ToSByte(dist * 5), true); // converting m to cm
                instructions.Add(i);
                i = new Instruction();
                i.setData(true, true, turnLeft, false, Convert.ToSByte(Math.Abs(curveDist * 500)), true); // converting m to cm
                instructions.Add(i);
                i = new Instruction();
                i.setData(true, true, turnLeft, false, Convert.ToSByte(Math.Abs(curveDist * 500)), true); // converting m to cm
                instructions.Add(i);*/
                curveDist = Math.Abs(r * (theta * .5 / Math.PI));
                tightTurn = true;
                if (running)
                {
                    brake = 120;
                    running = false;
                }


                curveDist = curveDist * angleDistReduction * 100;
                straightDist = straightDist * 100 * distReduction;

                i = new Instruction();
                if (curveDist > maxCMDistance)
                {
                    curveDist = maxCMDistance;
                }
                i.setData(true, true, turnLeft, true, true, false, Convert.ToSByte(curveDist));
                instructions.Add(i);
                i = new Instruction();
                i.setData(false, true, turnLeft, false, false, false, Convert.ToSByte(2));
                instructions.Add(i);
            }
            

            // quick instruction seed
            // Check if the car is going into a rapid turn, if so then skip the strait for faster turn tracking.
            // TODO: Base this on angle!
            /*if (!(((_pos.Item1 < -.5f) || (_pos.Item1 > .5f)) && (_pos.Item3 < 1.5f)))
            {
                for (int j = 0; j < 19; j++)
                {
                    i = new Instruction();
                    i.setData(true, false, false, false, Convert.ToSByte(Math.Abs(dist * 5)), true); // converting m to cm
                    instructions.Add(i);
                }
            }

            // Have to do this, can't be in the if.
            i = new Instruction();
            i.setData(false, false, false, false, Convert.ToSByte(0), true);
            instructions.Add(i);
            */

            // Check if the car lost the human due to a turn.
            /*if ((_pos.Item3 > 1.5f) && ((_pos.Item1 < -.75f) || (_pos.Item1 > .75f)))
            {
                tightTurn = true;
            }
            else if (tightTurn == true)
            {
                tightTurn = false;
            }
            else
            {
                tightTurn = false;
            }*/

            // convert to cm
            curveDist = curveDist * angleDistReduction * 100;
            straightDist = straightDist * 100 * distReduction;

            /*
            if (brake > 0)
            {
                i = new Instruction();
                i.setData(true, false, true, false, false, true, Convert.ToSByte(50));
                instructions.Add(i);
            }
            else
            {

                // add initial accelleration
                i = new Instruction();

                i.setData(true, false, false, true, false, false, Convert.ToSByte(initialAccel)); // converting m to cm
                instructions.Add(i);
            }
            

            // add turning if it exists
            while (curveDist > maxCMDistance)
            {
                i = new Instruction();
                i.setData(true, true, turnLeft, false, tightTurn, false, Convert.ToSByte(maxCMDistance));
                instructions.Add(i);
                curveDist -= maxCMDistance;
            }
            if (curveDist > 0)
            {
                i = new Instruction();
                i.setData(true, true, turnLeft, false, tightTurn, false, Convert.ToSByte(curveDist));
                instructions.Add(i);
            }


            // add straight if it exists
            while (straightDist > maxCMDistance)
            {
                i = new Instruction();
                i.setData(true, false, false, false, false, false, Convert.ToSByte(maxCMDistance));
                instructions.Add(i);
                curveDist -= maxCMDistance;
            }
            if (straightDist > 0)
            {
                i = new Instruction();
                i.setData(true, false, false, false, false, false, Convert.ToSByte(straightDist));
                instructions.Add(i);
            }
            // add brakes if needed

            

            */
            // add stop instruction
            

            if (instructions.Count > buffSize)
            {
                Console.Write("BUFFER SIZE WAY TOO LARGE!!!");
            }

        }

        private void transmit(List<Instruction> _path)
        {
            foreach (Instruction i in _path)
            {
                this.port.Write(i.encode());
            }
        }

        private void port_DataReceived
            (object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            string str = this.port.ReadExisting();
            //Console.Write(str);
            msgBuffer += str;
            String[] buffs = msgBuffer.Split('\n');
            foreach(String s in buffs)
            {
                if (s.Equals("More!"))
                {
                    //Thread.Sleep(1000); // delay to count for deceleration. TEMPORARY
                    selfQueue.Enqueue(true);
                }
            }
            if (buffs.Length > 1)
            {
                msgBuffer = buffs[buffs.Length - 1];
                foreach (String s in buffs)
                {
                    if (!s.Equals(msgBuffer))
                    {
                        //Console.WriteLine(s);
                    }
                }
            }
            
            
        }
    }
}