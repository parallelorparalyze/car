﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.IO;
using Microsoft.Kinect;
using Concurrent = System.Collections.Concurrent;

namespace Car
{
    class KinectReader
    {
        private KinectSensor sensor;
        private Concurrent.ConcurrentQueue<KinectDTO> inQueue;
        private Concurrent.ConcurrentQueue<KinectDTO> outQueue;
        private int trackedId = 0;
        private bool stopped = false;
        private const int timeForCalculateAngleChange = 1000; // in milliseconds
        private System.Timers.Timer timer;

        public const double vertFOV = 43; // 43 degrees
        public const double horiFOV = 57; // 57 degrees
        public const double height = .2032; // in m, about 8 inches
        public const int formatHeight = 60; // pixels
        public const int formatWidth = 80; // pixels
        public const DepthImageFormat formatResolution = DepthImageFormat.Resolution80x60Fps30;
        public const int maxDepth = 3500; // max depth in mm
        public const int minDepth = 1000; // min depth in mm
        public const int depthThreshold = 500; // cm

        private DepthImagePixel[] depthPixels;
        private DepthImagePixel[] idealPixels;
        private int[] depthList;

        private PerformanceCalculator pCalc = new PerformanceCalculator();

        public KinectReader()
        {
            
        }

        public void initialize()
        {
            // Use KinectSensorChooser in Microsoft.Kinect.Toolkit in future to protect 
            // against plug/unplug while running
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.sensor = potentialSensor;
                    break;
                }
            }

            if (this.sensor != null)
            {
                this.sensor.DepthStream.Enable(formatResolution);
                this.sensor.SkeletonStream.Enable();
                this.sensor.SkeletonFrameReady += this.SensorSkeletonFrameReady;
                this.depthPixels = new DepthImagePixel[this.sensor.DepthStream.FramePixelDataLength];
                this.sensor.DepthFrameReady += this.SensorDepthFrameReady;

                depthList = new int[formatWidth];

                try
                {
                    this.sensor.Start();
                    timer = new System.Timers.Timer(timeForCalculateAngleChange);
                    timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                    timer.Enabled = true;
                    calculateIdealPixels();
                }
                catch (IOException e)
                {
                    this.sensor = null;
                    Console.WriteLine(Properties.Resources.NoKinectReady);
                    throw new IOException(Properties.Resources.NoKinectReady, e);
                }
            }
            if (this.sensor == null)
            {
                throw new IOException(Properties.Resources.KinectInitializingError);
            }

            inQueue = new Concurrent.ConcurrentQueue<KinectDTO>();
            outQueue = new Concurrent.ConcurrentQueue<KinectDTO>();
        }

        public void run()
        {

        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            calculateIdealPixels();
        }

        private void calculateIdealPixels()
        {
            idealPixels = new DepthImagePixel[formatHeight];
            double kinectAngle = sensor.ElevationAngle;
            double anglePerPixel = vertFOV / formatHeight;
            int horizonHeight = (int)((-(kinectAngle) / vertFOV) * formatHeight + formatHeight / 2);
            // assuming 0 angle here, should really calculate this
            if (horizonHeight < 0)
            {
                horizonHeight = 0;
            }
            else if (horizonHeight > formatHeight)
            {
                horizonHeight = formatHeight;
            }

            for (int i = 0; i < horizonHeight; i++)
            {
                short depth = 0;
                double angle = Math.Abs(kinectAngle - (vertFOV / 2) + anglePerPixel * i);
                depth = (short)(1000 * height / Math.Sin(angle * Math.PI / 180)); // convert to mm
                if (depth > maxDepth || depth < minDepth || depth < 0)
                {
                    depth = 0;
                }
                idealPixels[i].Depth = depth;
            }
            for (int i = horizonHeight; i < formatHeight; i++)
            {
                idealPixels[i].Depth = 0;
            }
        }

        public void close()
        {
            if (this.sensor != null)
            {
                this.sensor.Stop();
            }
        }

        public KinectDTO getData()
        {
            try
            {
                KinectDTO first;
                outQueue.TryDequeue(out first);
                return first;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void addData(KinectDTO _data)
        {
            inQueue.Enqueue(_data);
        }

        public PerformanceDTO getPerformance()
        {
            return pCalc.getPerformance();
        }

        private void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            Skeleton[] skeletons = new Skeleton[0];
            pCalc.tick();
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    skeletonFrame.CopySkeletonDataTo(skeletons);
                    
                }
            }

            if (skeletons.Length != 0)
            {
                Skeleton closest = null;
                Skeleton tracked = null;
                if (trackedId != 0)
                {
                    foreach (Skeleton skel in skeletons)
                    {
                        if (skel != null && skel.TrackingState == SkeletonTrackingState.Tracked && skel.TrackingId == trackedId)
                        {
                            tracked = skel;
                        }
                    }
                }
                foreach (Skeleton skel in skeletons)
                {
                    if (skel.TrackingState == SkeletonTrackingState.Tracked)
                    {
                        if(closest == null || 
                            Math.Sqrt((skel.Position.X*skel.Position.X)+(skel.Position.Z*skel.Position.Z)) <  
                            Math.Sqrt((closest.Position.X*closest.Position.X)+(closest.Position.Z*closest.Position.Z)))
                        {
                            closest = skel;
                        }
                    }
                }
                if (tracked != null)
                {
                    outQueue.Enqueue(new KinectDTO(tracked.Position));
                    stopped = false;
                }
                else if (closest != null)
                {
                    outQueue.Enqueue(new KinectDTO(closest.Position));
                    trackedId = closest.TrackingId;
                    stopped = false;
                }
                else
                {
                    trackedId = 0;
                    if (!stopped)
                    {
                        outQueue.Enqueue(new KinectDTO(true));
                        stopped = true;
                    }
                }
            }
        }

        private void SensorDepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
        {
            using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
            {
                if (depthFrame != null)
                {
                    pCalc.tick();
                    // Copy the pixel data from the image to a temporary array
                    depthFrame.CopyDepthImagePixelDataTo(this.depthPixels);

                    // Get the min and max reliable depth for the current frame
                    int minDepth = depthFrame.MinDepth;
                    int maxDepth = depthFrame.MaxDepth;

                    depthList = new int[formatWidth];

                    for (int i = 0; i < formatWidth; i++)
                    {
                        int smallest = 0;
                        for (int j = 0; j < formatHeight; j++)
                        {

                            short aD = depthPixels[j * formatWidth + i].Depth;
                            int idealDepth = idealPixels[formatHeight - (j + 1)].Depth;
                            int d = (aD >= minDepth && aD <= maxDepth && idealDepth >= minDepth && idealDepth <= maxDepth ? (Math.Abs(aD - idealDepth) > depthThreshold ? aD : 0) : 0);
                            if (d > 0)
                            {
                                if (smallest == 0)
                                {
                                    smallest = d;
                                }
                                else if (d < smallest)
                                {
                                    smallest = d;
                                }
                            }
                        }

                        int depth = maxDepth;
                        if (smallest > 0)
                        {
                            depth = (int)(smallest * formatHeight / maxDepth);
                        }
                        if (depth > (formatHeight - 1))
                        {
                            depth = formatHeight - 1;
                        }

                        depthList[i] = smallest;
                    }
                    outQueue.Enqueue(new KinectDTO(depthList));
                }
            }
        }
    }
}
