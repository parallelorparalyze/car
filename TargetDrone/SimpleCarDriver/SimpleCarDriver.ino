#include <avr/interrupt.h>

volatile unsigned int Start_Pulse =0;
volatile unsigned int Stop_Pulse =0;
volatile unsigned int Pulse_Width =0;

volatile int Test=0;
volatile int Test2=0;
volatile int Temp=0;
volatile int Counter=0;
volatile byte PPM_Counter=0;
volatile int PWM_RAW[8] = {
  2400,2400,2400,2400,2400,2400,2400,2400};
int All_PWM=1500;

long timer=0;
long timer2=0;

const int MIN_SPEED = 10;
const int MAX_SPEED = 90;
const int MIN_STEERING = 10;
const int MAX_STEERING = 90;

volatile int speedSetting = 50;
volatile int steeringSetting = 50;

volatile int previousBytes[2] = { 0, 0 };  // for state machine

void setup() 
{
  Init_PWM1(); //OUT2&3 
  Init_PWM3(); //OUT6&7
  Init_PWM5(); //OUT0&1
  Init_PPM_PWM4(); //OUT4&5
  Serial.begin(57600);
} 

void loop() 
{ 
  int incomingByte;
  if (Serial.available())
  {
    incomingByte = Serial.read();
    //Serial.println("Previous Bytes:"); 
    //Serial.println(previousBytes[0],DEC);
    //Serial.println(previousBytes[1],DEC);
    //Serial.println("Incoming byte:"); 
    //Serial.println(incomingByte,DEC);
    if (previousBytes[0] == 68 && previousBytes[1] == 32)  // D and space  
    {
      if(incomingByte >= MIN_SPEED && incomingByte <= MAX_SPEED)
      {
        speedSetting = incomingByte;
        Serial.print("Speed Set: ");
        Serial.println(speedSetting,DEC);
        int ch1PWM = 1500 + ((speedSetting - 50) * 10);
        Serial.print("Speed PWM: ");
        Serial.println(ch1PWM,DEC);
        OutputCh(1, ch1PWM);
        delay(500);
        OutputCh(1, 1300);
         delay(1500);
        OutputCh(1, 1500);
      }
      else
      {
        Serial.print("Speed range error: ");
        Serial.println(incomingByte,DEC);
      }
    }
    if (previousBytes[0] == 83 && previousBytes[1] == 32) // S and space   
    {
      if(incomingByte >= MIN_STEERING && incomingByte <= MAX_STEERING)
      {
        steeringSetting = incomingByte;
        Serial.print("Steering Set: ");
        Serial.println(steeringSetting,DEC);
        int ch0PWM = 1500 + ((steeringSetting - 50) * 10);
        Serial.print("Steering PWM: ");
        Serial.println(ch0PWM,DEC);
        OutputCh(0, ch0PWM);;       
      }
      else
      {
        Serial.print("Steering range error: ");
        Serial.println(incomingByte,DEC);
      }
    }                    
    previousBytes[0] = previousBytes[1]; 
    previousBytes[1] = incomingByte;
  }
  delay(20);
}


