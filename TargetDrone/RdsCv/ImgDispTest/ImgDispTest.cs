using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;

namespace Robotics.ImgDispTest
{
    [Contract(Contract.Identifier)]
    [DisplayName("ImgDispTest")]
    [Description("ImgDispTest service (no description provided)")]
    class ImgDispTestService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        ImgDispTestState _state = new ImgDispTestState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ImgDispTest", AllowMultipleInstances = true)]
        ImgDispTestOperations _mainPort = new ImgDispTestOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        /// <summary>
        /// Service constructor
        /// </summary>
        public ImgDispTestService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();

//            Console.WriteLine("Service starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new ImgDispTestState();
            }

            SaveState(_state);
            
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Disp Image Handler
        /// </summary>
        /// <param name="request">SmoothImageRequest</param>
        /// <returns></returns>
        [ServiceHandler(ServiceHandlerBehavior.Concurrent)]
        public virtual IEnumerator<ITask> DispImageHandler(DispImage request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
//                Console.WriteLine("Displaying");
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForDisplay);

                //ImageViewer iv = new ImageViewer();
                //iv.Image = img;
                //iv.ShowDialog();

                CvInvoke.cvNamedWindow("DispTest");
                CvInvoke.cvShowImage("DispTest", img);
                CvInvoke.cvWaitKey(1);
            }
            yield break;
        }
    }
}


