using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using Robotics.ImgDispTest;

namespace Robotics.ImgDispTest
{
    /// <summary>
    /// ImgDispTest contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgDispTest
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/09/imgdisptest.html";
    }

    /// <summary>
    /// ImgDispTest state
    /// </summary>
    [DataContract]
    public class ImgDispTestState
    {
    }

    /// <summary>
    /// ImgDispTest main operations port
    /// </summary>
    [ServicePort]
    public class ImgDispTestOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, DispImage>
    {
    }

    /// <summary>
    /// ImgDispTest get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgDispTestState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgDispTestState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image Display Operation
    /// </summary>
    public class DispImage : Update<DispImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public DispImage()
        {
        }

        /// <summary>
        /// Constructor with DispImageRequest
        /// </summary>
        public DispImage(DispImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Disp Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class DispImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForDisplay;

        /// <summary>
        /// Constructor
        /// </summary>
        public DispImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public DispImageRequest(byte[,,] request)
        {
            ImageForDisplay = request;
        }
    }

    /// <summary>
    /// ImgDispTest subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


