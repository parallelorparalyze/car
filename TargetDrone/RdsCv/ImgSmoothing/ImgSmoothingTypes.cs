using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using Robotics.ImgSmoothing;

namespace Robotics.ImgSmoothing
{
    /// <summary>
    /// ImgSmoothing contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgSmoothing
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imgsmoothing.html";
    }

    /// <summary>
    /// ImgSmoothing state
    /// </summary>
    [DataContract]
    public class ImgSmoothingState
    {
        /// <summary>
        /// Smoothing Type : 1 -> Blur, 2 -> Median, 3 -> Gaussian, 4 -> Bilateral (default Gaussian)
        /// </summary>
        [DataMember]
        [Description("Smooth types : 1 -> Blur, 2 -> Median, 3 -> Gaussian, 4 -> Bilateral (default Gaussian)")]
        public int smoothType;

        /// <summary>
        /// Parameter 1
        /// </summary>
        [DataMember]
        [Description("Specifies param1.")]
        public int param1;

        /// <summary>
        /// Parameter 2
        /// </summary>
        [DataMember]
        [Description("Specifies param2.")]
        public int param2;

        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImgSmoothing main operations port
    /// </summary>
    [ServicePort]
    public class ImgSmoothingOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, SmoothImage, NotifyImageSmoothing>
    {
    }

    /// <summary>
    /// ImgSmoothing get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgSmoothingState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgSmoothingState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image Smoothing Operation
    /// </summary>
    public class SmoothImage : Update<SmoothImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public SmoothImage()
        {
        }

        /// <summary>
        /// Constructor with SmoothImageRequest
        /// </summary>
        public SmoothImage(SmoothImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Smooth Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class SmoothImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForSmoothing;

        /// <summary>
        /// Constructor
        /// </summary>
        public SmoothImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public SmoothImageRequest(byte[,,] request)
        {
            ImageForSmoothing = request;
        }
    }

    /// <summary>
    /// Image Smoothing Notification Operation
    /// </summary>
    [DisplayName("ImageSmoothed")]
    [Description("Indicates that image has been smoothed.")]
    public class NotifyImageSmoothing : Update<SmoothingResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImageSmoothing()
        {
        }

        /// <summary>
        /// Constructor with SmoothingResult
        /// </summary>
        public NotifyImageSmoothing(SmoothingResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image Smoothing Result
    /// </summary>
    [DataContract]
    public class SmoothingResult
    {
        /// <summary>
        /// Smoothing Result
        /// </summary>
        [DataMember]
        [Description("The resulting smoothed image data")]
        public byte[,,] SmoothedImage;
        
        /// <summary>
        /// Constructor
        /// </summary>
        public SmoothingResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public SmoothingResult(byte[,,] result)
        {
            SmoothedImage = result;
        }
    }

    /// <summary>
    /// ImgSmoothing subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


