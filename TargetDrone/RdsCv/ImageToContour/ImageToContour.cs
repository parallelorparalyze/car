using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;
using disp = Robotics.ImgDispTest.Proxy;

namespace Robotics.ImageToContour
{
    [Contract(Contract.Identifier)]
    [DisplayName("ImageToContour")]
    [Description("ImageToContour service (no description provided)")]
    class ImageToContourService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "ImageToContour.Config.xml")]
        ImageToContourState _state = new ImageToContourState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ImageToContour", AllowMultipleInstances = true)]
        ImageToContourOperations _mainPort = new ImageToContourOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private ImageToContourOperations _internalPort = new ImageToContourOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [Partner("ImgDispTest", Contract = disp.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        disp.ImgDispTestOperations _dispPort = new disp.ImgDispTestOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        //private volatile bool jobDone = false;

        /// <summary>
        /// Service constructor
        /// </summary>
        public ImageToContourService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
//            Console.WriteLine("Service Starting...");
//            LogInfo("Image To Contour starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new ImageToContourState();
            }

            if (_state.orderNumber < 1)
            {
                _state.orderNumber = 1;
            }

            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<NotifyContourCalculation>(true, _internalPort, ContourCalculationHandler),
                            Arbiter.Receive<capt.NotifyImageCapture>(true, _imgCaptNotify, ImageNotificationHandler),
                            Arbiter.Receive<CalcContourFromImage>(true, _internalPort, CalcContourFromImageHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
            //if (!jobDone)
            //{
//                LogInfo("Image notification from capture arrived");
                if ((update.Body.Order + 1) == _state.orderNumber)
                {
//                    Console.WriteLine("Image from camera received");
//                    LogInfo("Image from camera received");
                    Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
                    _internalPort.Post(new CalcContourFromImage(new CalcContourFromImageRequest(img.Data)));
                    //jobDone = true;
                }
                else
                {
//                    Console.WriteLine("Not my image");
//                    LogInfo("Contour : Not my image");
                }
            //}
            //else
            //{
//            //    Console.WriteLine("My job's done");
//            //    LogInfo("Contour : My job's done");
            //}
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image capture success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void ContourCalculationHandler(NotifyContourCalculation update)
        {
//            LogInfo("Conv Done");
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
            if (_state.isLast == true)
            {
                _dispPort.DispImage(update.Body.ContourImgData);
            }
            else
            {
                _imgCaptPort.NotifyImageProcessing(update.Body.ContourImgData, _state.orderNumber);
            }
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Calculate Contour From Image Handler
        /// </summary>
        /// <param name="request">CalcContourFromImageRequest</param>
        /// <returns></returns>
        public void CalcContourFromImageHandler(CalcContourFromImage request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageDataForContour);
                
                Image<Gray, byte> gimg = img.Convert<Gray, byte>();
                gimg = gimg.ThresholdBinary(new Gray(100), new Gray(255));
                Contour<Point> cp;
                MemStorage store = new MemStorage();
                cp = gimg.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST, store);
                CvInvoke.cvZero(gimg.Ptr);
                CvInvoke.cvDrawContours(gimg.Ptr, cp.Ptr, new MCvScalar(255, 0, 0), new MCvScalar(0, 0, 255), 2, 2, Emgu.CV.CvEnum.LINE_TYPE.CV_AA, Point.Empty);

                img = gimg.Convert<Bgr, byte>();

                _internalPort.Post(new NotifyContourCalculation(new ContourResult(img.Data, cp)));
//                Console.WriteLine("Writing data...");

                /* ImageViewer iv = new ImageViewer();
                iv.Image = gimg;
                iv.ShowDialog(); */
            }
        }
    }
}


