using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using Robotics.ImageToContour;

namespace Robotics.ImageToContour
{
    /// <summary>
    /// ImageToContour contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImageToContour
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imagetocontour.html";
    }

    /// <summary>
    /// ImageToContour state
    /// </summary>
    [DataContract]
    public class ImageToContourState
    {
        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImageToContour main operations port
    /// </summary>
    [ServicePort]
    public class ImageToContourOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, CalcContourFromImage, NotifyContourCalculation>
    {
    }

    /// <summary>
    /// ImageToContour get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImageToContourState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImageToContourState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Contour Calculation operation
    /// </summary>
    public class CalcContourFromImage : Update<CalcContourFromImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public CalcContourFromImage()
        {
        }

        /// <summary>
        /// Constructor with CalcContourFromImageRequest
        /// </summary>
        public CalcContourFromImage(CalcContourFromImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Calculate Contour From Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class CalcContourFromImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageDataForContour;

        /// <summary>
        /// Constructor
        /// </summary>
        public CalcContourFromImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public CalcContourFromImageRequest(byte[,,] request)
        {
            ImageDataForContour = request;
        }
    }

    /// <summary>
    /// Contour Calculation Notification Operation
    /// </summary>
    [DisplayName("CalculatedHistogram")]
    [Description("Indicates that a histogram has been calculated.")]
    public class NotifyContourCalculation : Update<ContourResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyContourCalculation()
        {
        }

        /// <summary>
        /// Constructor with HistResult
        /// </summary>
        public NotifyContourCalculation(ContourResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Contour Calculation Result
    /// </summary>
    [DataContract]
    public class ContourResult
    {
        /// <summary>
        /// Contour Image data
        /// </summary>
        [DataMember]
        [Description("The output Image data with Contours")]
        public byte[,,] ContourImgData;

        /// <summary>
        /// Contour Result
        /// </summary>
        [DataMember]
        [Description("The resulting Contour")]
        public Contour<Point> contourPoints;

        /// <summary>
        /// Constructor
        /// </summary>
        public ContourResult()
        {
        }
        /// <summary>
        /// Constructor with ImageHistResult
        /// </summary>
        public ContourResult (byte[,,] cntrImg, Contour<Point> cp)
        {
            ContourImgData = cntrImg;
            contourPoints = cp;
        }
    }

    /// <summary>
    /// ImageToContour subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


