using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Robotics.ImgSobel;
using System.Drawing;


namespace Robotics.ImgSobel
{
    /// <summary>
    /// ImgSobel contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgSobel
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imgsobel.html";
    }

    /// <summary>
    /// ImgSobel state
    /// </summary>
    [DataContract]
    public class ImgSobelState
    {
        /// <summary>
        /// x-order default 1.
        /// </summary>
        [DataMember]
        [Description("x-order default 1")]
        public int XOrder;

        /// <summary>
        /// y-order default 0.
        /// </summary>
        [DataMember]
        [Description("y-order default 0")]
        public int YOrder;

        /// <summary>
        /// Aperture Size (1,3,5,7 only) default 3, -1 for Scharr.
        /// </summary>
        [DataMember]
        [Description("Aperture Size (1,3,5,7 only) default 3, -1 for Scharr")]
        public int Aperture;

        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImgSobel main operations port
    /// </summary>
    [ServicePort]
    public class ImgSobelOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, SobelImage, NotifyImageSobel>
    {
    }

    /// <summary>
    /// ImgSobel get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgSobelState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgSobelState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image Sobel Operation
    /// </summary>
    public class SobelImage : Update<SobelImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public SobelImage()
        {
        }

        /// <summary>
        /// Constructor with SobelImageRequest
        /// </summary>
        public SobelImage(SobelImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Sobel Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class SobelImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForSobel;

        /// <summary>
        /// Constructor
        /// </summary>
        public SobelImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public SobelImageRequest(byte[,,] request)
        {
            ImageForSobel = request;
        }
    }

    /// <summary>
    /// Image Sobel Notification Operation
    /// </summary>
    [DisplayName("ImageSobel done.")]
    [Description("Indicates that image Sobel is done.")]
    public class NotifyImageSobel : Update<SobelResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImageSobel()
        {
        }

        /// <summary>
        /// Constructor with SobelResult
        /// </summary>
        public NotifyImageSobel(SobelResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image Sobel Result
    /// </summary>
    [DataContract]
    public class SobelResult
    {
        /// <summary>
        /// Sobel Result
        /// </summary>
        [DataMember]
        [Description("The resulting Sobel image data")]
        public byte[,,] SobelResImage;

        /// <summary>
        /// Constructor
        /// </summary>
        public SobelResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public SobelResult(byte[,,] result)
        {
            SobelResImage = result;
        }
    }

    /// <summary>
    /// ImgSobel subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


