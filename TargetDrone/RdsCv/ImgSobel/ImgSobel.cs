using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;
using disp = Robotics.ImgDispTest.Proxy;

namespace Robotics.ImgSobel
{
    [Contract(Contract.Identifier)]
    [DisplayName("ImgSobel")]
    [Description("ImgSobel service (no description provided)")]
    class ImgSobelService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "ImgSobel.Config.xml")]
        ImgSobelState _state = new ImgSobelState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ImgSobel", AllowMultipleInstances = true)]
        ImgSobelOperations _mainPort = new ImgSobelOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private ImgSobelOperations _internalPort = new ImgSobelOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [Partner("ImgDispTest", Contract = disp.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        disp.ImgDispTestOperations _dispPort = new disp.ImgDispTestOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        //private volatile bool jobDone = false;

        /// <summary>
        /// Service constructor
        /// </summary>
        public ImgSobelService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
//            Console.WriteLine("Service starting...");
//            LogInfo("Image Sobel starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new ImgSobelState();
            }

            // Initialize state
            if ((_state.XOrder <= 0) && (_state.YOrder <= 0))
            {
                _state.XOrder = 1;
                _state.YOrder = 0;
            }

            if ((_state.Aperture != -1) &&
                (_state.Aperture != 1) &&
                (_state.Aperture != 3) &&
                (_state.Aperture != 5) &&
                (_state.Aperture != 7)
               )
            {
                _state.Aperture = 3;
            }

            if (_state.orderNumber < 1)
            {
                _state.orderNumber = 1;
            }

            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<NotifyImageSobel>(true, _internalPort, ImageSobelHandler),
                            Arbiter.Receive<capt.NotifyImageCapture>(true, _imgCaptNotify, ImageNotificationHandler),
                            Arbiter.Receive<SobelImage>(true, _internalPort, SobelImageHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
            //if (!jobDone)
            //{
//                LogInfo("Image notification from capture arrived");
                if ((update.Body.Order + 1) == _state.orderNumber)
                {
//                    Console.WriteLine("Image from camera received");
//                    LogInfo("Image from camera received");
                    Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
                    _internalPort.Post(new SobelImage(new SobelImageRequest(img.Data)));
                    //jobDone = true;
                }
                else
                {
//                    Console.WriteLine("Not my image");
//                    LogInfo("Sobel : Not my image");
                }
            //}
            //else
            //{
//            //    Console.WriteLine("My job's done");
//            //    LogInfo("Sobel : My job's done");
            //}
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image capture success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void ImageSobelHandler(NotifyImageSobel update)
        {
//            LogInfo("Conv Done");
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
            if (_state.isLast == true)
            {
                _dispPort.DispImage(update.Body.SobelResImage);
            }
            else
            {
                _imgCaptPort.NotifyImageProcessing(update.Body.SobelResImage, _state.orderNumber);
            }
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Sobel Image Handler
        /// </summary>
        /// <param name="request">SobelImageRequest</param>
        /// <returns></returns>
        public void SobelImageHandler(SobelImage request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForSobel);

                Image<Gray, byte> gimg = img.Convert<Gray, byte>();
                Image<Gray, float> gbig = new Image<Gray, float>(img.Size);
                CvInvoke.cvSobel(gimg, gbig, _state.XOrder, _state.YOrder, _state.Aperture);
                img = gbig.Convert<Bgr, byte>();

                _internalPort.Post(new NotifyImageSobel(new SobelResult(img.Data)));
//                Console.WriteLine("Writing data...");

                /* ImageViewer iv = new ImageViewer();
                iv.Image = img;
                iv.ShowDialog(); */
            }
        }
    }
}


