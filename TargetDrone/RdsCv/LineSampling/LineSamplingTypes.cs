using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using Robotics.LineSampling;

namespace Robotics.LineSampling
{
    /// <summary>
    /// LineSampling contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for LineSampling
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/09/linesampling.html";
    }

    /// <summary>
    /// LineSampling state
    /// </summary>
    [DataContract]
    public class LineSamplingState
    {
        /// <summary>
        /// Line Segment for sampling.
        /// </summary>
        [DataMember]
        [Description("Line Segment for sampling")]
        public LineSegment2D LineForSampling;

        /// <summary>
        /// Connectivity type, 1 -> 8-Connected, 2 -> 4-Connected (Default 1)
        /// </summary>
        [DataMember]
        [Description("Connectivity type, 1 -> 8-Connected, 2 -> 4-Connected (Default 1)")]
        public int ConnectivityType;
    }

    /// <summary>
    /// LineSampling main operations port
    /// </summary>
    [ServicePort]
    public class LineSamplingOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, SampleLine, NotifySampleLine>
    {
    }

    /// <summary>
    /// LineSampling get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<LineSamplingState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<LineSamplingState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image Laplace Operation
    /// </summary>
    public class SampleLine : Update<SampleLineRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public SampleLine()
        {
        }

        /// <summary>
        /// Constructor with SampleLineRequest
        /// </summary>
        public SampleLine(SampleLineRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Sample Line Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class SampleLineRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForLineSampling;

        /// <summary>
        /// Constructor
        /// </summary>
        public SampleLineRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public SampleLineRequest(byte[,,] request)
        {
            ImageForLineSampling = request;
        }
    }

    /// <summary>
    /// Sample Line Notification Operation
    /// </summary>
    [DisplayName("SampleLine done.")]
    [Description("Indicates that line sampling is done.")]
    public class NotifySampleLine : Update<SampleLineResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifySampleLine()
        {
        }

        /// <summary>
        /// Constructor with SampleLineResult
        /// </summary>
        public NotifySampleLine(SampleLineResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Sample Line Result
    /// </summary>
    [DataContract]
    public class SampleLineResult
    {
        /// <summary>
        /// SampleLine Result
        /// </summary>
        [DataMember]
        [Description("The resulting Sample Line data")]
        public byte[,] SampleLineRes;

        /// <summary>
        /// Constructor
        /// </summary>
        public SampleLineResult()
        {
        }
        /// <summary>
        /// Constructor with SampleLine Data Result (byte[,])
        /// </summary>
        public SampleLineResult(byte[,] result)
        {
            SampleLineRes = result;
        }
    }

    /// <summary>
    /// LineSampling subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


