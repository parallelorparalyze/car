using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;

namespace Robotics.LineSampling
{
    [Contract(Contract.Identifier)]
    [DisplayName("LineSampling")]
    [Description("LineSampling service (no description provided)")]
    class LineSamplingService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "LineSampling.Config.xml")]
        LineSamplingState _state = new LineSamplingState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/LineSampling", AllowMultipleInstances = true)]
        LineSamplingOperations _mainPort = new LineSamplingOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private LineSamplingOperations _internalPort = new LineSamplingOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        /// <summary>
        /// Service constructor
        /// </summary>
        public LineSamplingService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
//            Console.WriteLine("Service starting...");
//            LogInfo("Line Sampling starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new LineSamplingState();
            }

            // Initialize state
            _state.LineForSampling = new LineSegment2D(new Point(400, 400), new Point(500, 400));
            _state.ConnectivityType = 1;
            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(
                        Arbiter.Receive<NotifySampleLine>(true, _internalPort, LineSampledHandler),
                        Arbiter.Receive<capt.NotifyImageCapture>(false, _imgCaptNotify, ImageNotificationHandler)
                    ),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<SampleLine>(true, _internalPort, SampleLineHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
//            Console.WriteLine("Image from camera received");
//            LogInfo("Image from camera received");
            Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
            _internalPort.Post(new SampleLine(new SampleLineRequest(img.Data)));
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image cature success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void LineSampledHandler(NotifySampleLine update)
        {
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Sample Line Handler
        /// </summary>
        /// <param name="request">SampleLineRequest</param>
        /// <returns></returns>
        public void SampleLineHandler(SampleLine request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForLineSampling);
                byte[,] buffer = null;

                switch(_state.ConnectivityType)
                {
                    case 1:
                        buffer = img.Sample(_state.LineForSampling, Emgu.CV.CvEnum.CONNECTIVITY.EIGHT_CONNECTED);
                        break;

                    case 2:
                        buffer = img.Sample(_state.LineForSampling, Emgu.CV.CvEnum.CONNECTIVITY.FOUR_CONNECTED);
                        break;
                }

                _internalPort.Post(new NotifySampleLine(new SampleLineResult(buffer)));
//                Console.WriteLine("Writing data...");
            }
        }
    }
}


