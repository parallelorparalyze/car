using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;
using disp = Robotics.ImgDispTest.Proxy;

namespace Robotics.ImgHoughLines
{
    [Contract(Contract.Identifier)]
    [DisplayName("ImgHoughLines")]
    [Description("ImgHoughLines service (no description provided)")]
    class ImgHoughLinesService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "ImgHoughLines.Config.xml")]
        ImgHoughLinesState _state = new ImgHoughLinesState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ImgHoughLines", AllowMultipleInstances = true)]
        ImgHoughLinesOperations _mainPort = new ImgHoughLinesOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private ImgHoughLinesOperations _internalPort = new ImgHoughLinesOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [Partner("ImgDispTest", Contract = disp.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        disp.ImgDispTestOperations _dispPort = new disp.ImgDispTestOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        //private volatile bool jobDone = false;

        /// <summary>
        /// Service constructor
        /// </summary>
        public ImgHoughLinesService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
//            Console.WriteLine("Service starting...");
//            LogInfo("Image HoughLines starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new ImgHoughLinesState();
            }

            // Initialize state
            if (_state.CannyThreshold <= 0)
            {
                _state.CannyThreshold = 180;
            }

            if (_state.CannyThresholdLinking <= 0)
            {
                _state.CannyThresholdLinking = 120;
            }
            
            if (_state.RhoResolution <= 0)
            {
                _state.RhoResolution = 1;
            }

            if (_state.ThetaResolution <= 0)
            {
                _state.ThetaResolution = Math.PI / 45.0;
            }

            if (_state.Threshold <= 0)
            {
                _state.Threshold = 20;
            }

            if (_state.MinLineWidth <= 0)
            {
                _state.MinLineWidth = 30;
            }

            if (_state.GapBetweenLines <= 0)
            {
                _state.GapBetweenLines = 10;
            }

            if (_state.orderNumber < 1)
            {
                _state.orderNumber = 1;
            }

            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<NotifyImageHoughLines>(true, _internalPort, ImageHoughLinesHandler),
                            Arbiter.Receive<capt.NotifyImageCapture>(true, _imgCaptNotify, ImageNotificationHandler),
                            Arbiter.Receive<HoughLinesImage>(true, _internalPort, HoughLinesImageHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
            //if (!jobDone)
            //{
//                LogInfo("Image notification from capture arrived");
                if ((update.Body.Order + 1) == _state.orderNumber)
                {
//                    Console.WriteLine("Image from camera received");
//                    LogInfo("Image from camera received");
                    Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
                    _internalPort.Post(new HoughLinesImage(new HoughLinesImageRequest(img.Data)));
                    //jobDone = true;
                }
                else
                {
//                    Console.WriteLine("Not my image");
//                    LogInfo("HoughLines : Not my image");
                }
            //}
            //else
            //{
//            //    Console.WriteLine("My job's done");
//            //    LogInfo("HoughLines : My job's done");
            //}
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image capture success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void ImageHoughLinesHandler(NotifyImageHoughLines update)
        {
//            LogInfo("Conv Done");
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
            if (_state.isLast == true)
            {
                _dispPort.DispImage(update.Body.HoughLinesResImg);
            }
            else
            {
                _imgCaptPort.NotifyImageProcessing(update.Body.HoughLinesResImg, _state.orderNumber);
            }
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// HoughLines Image Handler
        /// </summary>
        /// <param name="request">HoughLinesImageRequest</param>
        /// <returns></returns>
        public void HoughLinesImageHandler(HoughLinesImage request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForHoughLines);

                Image<Gray, byte> gimg = img.Convert<Gray, byte>().PyrDown().PyrUp();
                gimg = gimg.Canny(new Gray(_state.CannyThreshold), new Gray(_state.CannyThresholdLinking));

                LineSegment2D[] lines = gimg.HoughLinesBinary(_state.RhoResolution, _state.ThetaResolution, _state.Threshold, _state.MinLineWidth, _state.GapBetweenLines)[0];

                Image<Bgr, Byte> lineImage = img.CopyBlank();
                foreach (LineSegment2D line in lines)
                    lineImage.Draw(line, new Bgr(Color.Green), 2);

                _internalPort.Post(new NotifyImageHoughLines(new HoughLinesResult(lineImage.Data, lines)));
//                Console.WriteLine("Writing data...");

                /* ImageViewer iv = new ImageViewer();
                iv.Image = lineImage;
                iv.ShowDialog(); */
            }
        }
    }
}


