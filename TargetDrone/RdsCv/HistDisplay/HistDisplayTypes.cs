using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using Robotics.HistDisplay;

namespace Robotics.HistDisplay
{
    /// <summary>
    /// HistDisplay contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for HistDisplay
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/09/histdisplay.html";
    }

    /// <summary>
    /// HistDisplay state
    /// </summary>
    [DataContract]
    public class HistDisplayState
    {
        /// <summary>
        /// Histogram Display Bins
        /// </summary>
        [DataMember]
        [Description("Specifies the number of histogram display bins.")]
        public int NoOfBins;
    }

    /// <summary>
    /// HistDisplay main operations port
    /// </summary>
    [ServicePort]
    public class HistDisplayOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, DispHist>
    {
    }

    /// <summary>
    /// HistDisplay get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<HistDisplayState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<HistDisplayState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Histogram Calculation operation
    /// </summary>
    public class DispHist : Update<DispHistRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public DispHist()
        {
        }

        /// <summary>
        /// Constructor with DispHistRequest
        /// </summary>
        public DispHist(DispHistRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Display Hist Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class DispHistRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForHistDisp;

        /// <summary>
        /// Constructor
        /// </summary>
        public DispHistRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public DispHistRequest(byte[,,] request)
        {
            ImageForHistDisp = request;
        }
    }

    /// <summary>
    /// HistDisplay subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


