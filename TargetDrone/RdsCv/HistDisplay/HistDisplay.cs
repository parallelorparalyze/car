using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;

namespace Robotics.HistDisplay
{
    [Contract(Contract.Identifier)]
    [DisplayName("HistDisplay")]
    [Description("HistDisplay service (no description provided)")]
    class HistDisplayService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "HistDisplay.Config.xml")]
        HistDisplayState _state = new HistDisplayState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/HistDisplay", AllowMultipleInstances = true)]
        HistDisplayOperations _mainPort = new HistDisplayOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private HistDisplayOperations _internalPort = new HistDisplayOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        /// <summary>
        /// Service constructor
        /// </summary>
        public HistDisplayService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
//            Console.WriteLine("Starting Service...");
//            LogInfo("Histogram Display starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new HistDisplayState();
            }

            // Initialize state variables
            _state.NoOfBins = 30;
            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(
                        Arbiter.Receive<capt.NotifyImageCapture>(false, _imgCaptNotify, ImageNotificationHandler)
                    ),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<DispHist>(true, _internalPort, DispHistHandler)
                        )
                ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
//            Console.WriteLine("Image from camera received");
//            LogInfo("Image from camera received");
            Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
            _internalPort.Post(new DispHist(new DispHistRequest(img.Data)));
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image cature success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Display Histogram From Image Handler
        /// </summary>
        /// <param name="request">DispHistRequest</param>
        /// <returns></returns>
        public void DispHistHandler(DispHist request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForHistDisp);

                HistogramBox hb = new HistogramBox();
                HistogramViewer hv = new HistogramViewer();
                hb = hv.HistogramCtrl;
                hb.GenerateHistograms(img, _state.NoOfBins);
                hb.Refresh();
                hv.ShowDialog();
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            }
        }
    }
}


