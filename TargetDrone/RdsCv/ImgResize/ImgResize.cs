using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;

namespace Robotics.ImgResize
{
    [Contract(Contract.Identifier)]
    [DisplayName("ImgResize")]
    [Description("ImgResize service (no description provided)")]
    class ImgResizeService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "ImgResize.Config.xml")]
        ImgResizeState _state = new ImgResizeState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ImgResize", AllowMultipleInstances = true)]
        ImgResizeOperations _mainPort = new ImgResizeOperations();

        // <summary>
        /// Internal port to update private state.
        /// </summary>
        private ImgResizeOperations _internalPort = new ImgResizeOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        /// <summary>
        /// Service constructor
        /// </summary>
        public ImgResizeService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
//            Console.WriteLine("Service starting...");
//            LogInfo("Image Resize starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new ImgResizeState();
            }

            // Initialize State Variables
            _state.InterType = 3;

            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(
                        Arbiter.Receive<NotifyImageResizing>(true, _internalPort, ImageResizedHandler),
                        Arbiter.Receive<capt.NotifyImageCapture>(false, _imgCaptNotify, ImageNotificationHandler)
                    ),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<ResizeImage>(true, _internalPort, ResizeImageHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
//            Console.WriteLine("Image from camera received");
//            LogInfo("Image from camera received");
            Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
            _internalPort.Post(new ResizeImage(new ResizeImageRequest(img.Data, 320, 240)));
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image cature success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void ImageResizedHandler(NotifyImageResizing update)
        {
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Resize Image Handler
        /// </summary>
        /// <param name="request">ResizeImageRequest</param>
        /// <returns></returns>
        public void ResizeImageHandler(ResizeImage request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForResizing);
                int width = request.Body.RWidth;
                int height = request.Body.RHeight;
                Image<Bgr, byte> resImg = new Image<Bgr, byte>(width, height);

                switch(_state.InterType)
                {
                    case 1:
                        CvInvoke.cvResize(img.Ptr, resImg.Ptr, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
                        break;

                    case 2:
                        CvInvoke.cvResize(img.Ptr, resImg.Ptr, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                        break;

                    case 3:
                        CvInvoke.cvResize(img.Ptr, resImg.Ptr, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                        break;

                    case 4:
                        CvInvoke.cvResize(img.Ptr, resImg.Ptr, Emgu.CV.CvEnum.INTER.CV_INTER_NN);
                        break;
                }

                _internalPort.Post(new NotifyImageResizing(new ResizingResult(resImg.Data)));

                ImageViewer iv = new ImageViewer();
                iv.Image = resImg;
                iv.ShowDialog();
            }
        }
    }
}


