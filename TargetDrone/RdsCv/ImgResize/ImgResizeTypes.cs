using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using Robotics.ImgResize;

namespace Robotics.ImgResize
{
    /// <summary>
    /// ImgResize contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgResize
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imgresize.html";
    }

    /// <summary>
    /// ImgResize state
    /// </summary>
    [DataContract]
    public class ImgResizeState
    {
        /// <summary>
        /// Resize Interpolation Type : 1 -> Area, 2 -> Cubic, 3 -> Linear, 4 -> NN (default Linear)
        /// </summary>
        [DataMember]
        [Description("Resize Interpolation Type : 1 -> Area, 2 -> Cubic, 3 -> Linear, 4 -> NN (default Linear)")]
        public int InterType;
    }

    /// <summary>
    /// ImgResize main operations port
    /// </summary>
    [ServicePort]
    public class ImgResizeOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, ResizeImage, NotifyImageResizing>
    {
    }

    /// <summary>
    /// ImgResize get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgResizeState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgResizeState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image Resizing Operation
    /// </summary>
    public class ResizeImage : Update<ResizeImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public ResizeImage()
        {
        }

        /// <summary>
        /// Constructor with ResizeImageRequest
        /// </summary>
        public ResizeImage(ResizeImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Resize Image Request Type
    /// </summary>
    [DataContract]
    public class ResizeImageRequest
    {
        [DataMember]
        [Description("Data for Input Image")]
        public byte[,,] ImageForResizing;

        [DataMember]
        [Description("Resized image width")]
        public int RWidth;

        [DataMember]
        [Description("Resized image height")]
        public int RHeight;

        /// <summary>
        /// Constructor
        /// </summary>
        public ResizeImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,]), width and height
        /// </summary>
        public ResizeImageRequest(byte[,,] request, int width, int height)
        {
            ImageForResizing = request;
            RWidth = width;
            RHeight = height;
        }
    }

    /// <summary>
    /// Image Resizing Notification Operation
    /// </summary>
    [DisplayName("ImageResized")]
    [Description("Indicates that image has been resized.")]
    public class NotifyImageResizing : Update<ResizingResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImageResizing()
        {
        }

        /// <summary>
        /// Constructor with ResizingResult
        /// </summary>
        public NotifyImageResizing(ResizingResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image Resizing Result
    /// </summary>
    [DataContract]
    public class ResizingResult
    {
        /// <summary>
        /// Resizing Result
        /// </summary>
        [DataMember]
        [Description("The resulting resized image data")]
        public byte[,,] ResizedImage;

        /// <summary>
        /// Constructor
        /// </summary>
        public ResizingResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public ResizingResult(byte[,,] result)
        {
            ResizedImage = result;
        }
    }

    /// <summary>
    /// ImgResize subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


