using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using Robotics.ImgPyrUp;

namespace Robotics.ImgPyrUp
{
    /// <summary>
    /// ImgPyrUp contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgPyrUp
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imgpyrup.html";
    }

    /// <summary>
    /// ImgPyrUp state
    /// </summary>
    [DataContract]
    public class ImgPyrUpState
    {
        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImgPyrUp main operations port
    /// </summary>
    [ServicePort]
    public class ImgPyrUpOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, PyrUpImage, NotifyImagePyrUp>
    {
    }

    /// <summary>
    /// ImgPyrUp get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgPyrUpState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgPyrUpState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image PyrUp Operation
    /// </summary>
    public class PyrUpImage : Update<PyrUpImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public PyrUpImage()
        {
        }

        /// <summary>
        /// Constructor with PyrUpImageRequest
        /// </summary>
        public PyrUpImage(PyrUpImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// PyrUp Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class PyrUpImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[, ,] ImageForPyrUp;

        /// <summary>
        /// Constructor
        /// </summary>
        public PyrUpImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public PyrUpImageRequest(byte[, ,] request)
        {
            ImageForPyrUp = request;
        }
    }

    /// <summary>
    /// Image PyrUp Notification Operation
    /// </summary>
    [DisplayName("ImagePyrUp done.")]
    [Description("Indicates that image PyrUp is done.")]
    public class NotifyImagePyrUp : Update<PyrUpResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImagePyrUp()
        {
        }

        /// <summary>
        /// Constructor with PyrUpResult
        /// </summary>
        public NotifyImagePyrUp(PyrUpResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image PyrUp Result
    /// </summary>
    [DataContract]
    public class PyrUpResult
    {
        /// <summary>
        /// PyrUp Result
        /// </summary>
        [DataMember]
        [Description("The resulting PyrUp image data")]
        public byte[, ,] PyrUpResImage;

        /// <summary>
        /// Constructor
        /// </summary>
        public PyrUpResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public PyrUpResult(byte[, ,] result)
        {
            PyrUpResImage = result;
        }
    }

    /// <summary>
    /// ImgPyrUp subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


