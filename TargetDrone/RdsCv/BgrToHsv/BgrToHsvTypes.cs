using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using Robotics.BgrToHsv;

namespace Robotics.BgrToHsv
{
    /// <summary>
    /// BgrToHsv contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for BgrToHsv
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/09/bgrtohsv.html";
    }

    /// <summary>
    /// BgrToHsv state
    /// </summary>
    [DataContract]
    public class BgrToHsvState
    {
        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// BgrToHsv main operations port
    /// </summary>
    [ServicePort]
    public class BgrToHsvOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, ConvBgrToHsv, NotifyBgrToHsv>
    {
    }

    /// <summary>
    /// BgrToHsv get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<BgrToHsvState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<BgrToHsvState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Histogram Calculation operation
    /// </summary>
    public class ConvBgrToHsv : Update<ConvBgrToHsvRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public ConvBgrToHsv()
        {
        }

        /// <summary>
        /// Constructor with ConvBgrToHsvRequest
        /// </summary>
        public ConvBgrToHsv(ConvBgrToHsvRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Convert BgrToHsv Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class ConvBgrToHsvRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForBgrToHsv;

        /// <summary>
        /// Constructor
        /// </summary>
        public ConvBgrToHsvRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public ConvBgrToHsvRequest(byte[,,] request)
        {
            ImageForBgrToHsv = request;
        }
    }

    /// <summary>
    /// BgrToHsv Notification Operation
    /// </summary>
    [DisplayName("CalculatedBgrToHsv")]
    [Description("Indicates that a BgrToHsv is done.")]
    public class NotifyBgrToHsv : Update<BgrToHsvResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyBgrToHsv()
        {
        }

        /// <summary>
        /// Constructor with BgrToHsvResult
        /// </summary>
        public NotifyBgrToHsv(BgrToHsvResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// BgrToHsv Result
    /// </summary>
    [DataContract]
    public class BgrToHsvResult
    {
        /// <summary>
        /// BgrToHsv Result
        /// </summary>
        [DataMember]
        [Description("The resulting Hsv Image data")]
        public byte[,,] HsvImg;

        /// <summary>
        /// Constructor
        /// </summary>
        public BgrToHsvResult()
        {
        }
        /// <summary>
        /// Constructor with BgrToHsvResult
        /// </summary>
        public BgrToHsvResult(byte[,,] result)
        {
            HsvImg = result;
        }
    }

    /// <summary>
    /// BgrToHsv subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


