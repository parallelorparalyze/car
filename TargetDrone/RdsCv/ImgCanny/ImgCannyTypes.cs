using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Robotics.ImgCanny;
using System.Drawing;

namespace Robotics.ImgCanny
{
    /// <summary>
    /// ImgCanny contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgCanny
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imgcanny.html";
    }

    /// <summary>
    /// ImgCanny state
    /// </summary>
    [DataContract]
    public class ImgCannyState
    {
        /// <summary>
        /// Canny Threshold to find intital segments of strong edges.
        /// </summary>
        [DataMember]
        [Description("Canny Threshold to find intital segments of strong edges")]
        public int CannyThreshold;

        /// <summary>
        /// Canny Threshold for edge linking.
        /// </summary>
        [DataMember]
        [Description("Canny Threshold for edge linking")]
        public int CannyThresholdLinking;

        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImgCanny main operations port
    /// </summary>
    [ServicePort]
    public class ImgCannyOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, CannyImage, NotifyImageCanny>
    {
    }

    /// <summary>
    /// ImgCanny get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgCannyState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgCannyState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image Canny Operation
    /// </summary>
    public class CannyImage : Update<CannyImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public CannyImage()
        {
        }

        /// <summary>
        /// Constructor with CannyImageRequest
        /// </summary>
        public CannyImage(CannyImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Canny Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class CannyImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForCanny;

        /// <summary>
        /// Constructor
        /// </summary>
        public CannyImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public CannyImageRequest(byte[,,] request)
        {
            ImageForCanny = request;
        }
    }

    /// <summary>
    /// Image Canny Notification Operation
    /// </summary>
    [DisplayName("ImageCanny done.")]
    [Description("Indicates that image Canny is done.")]
    public class NotifyImageCanny : Update<CannyResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImageCanny()
        {
        }

        /// <summary>
        /// Constructor with CannyResult
        /// </summary>
        public NotifyImageCanny(CannyResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image Canny Result
    /// </summary>
    [DataContract]
    public class CannyResult
    {
        /// <summary>
        /// Canny Result
        /// </summary>
        [DataMember]
        [Description("The resulting Canny image data")]
        public byte[,,] CannyResImage;

        /// <summary>
        /// Constructor
        /// </summary>
        public CannyResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public CannyResult(byte[,,] result)
        {
            CannyResImage = result;
        }
    }

    /// <summary>
    /// ImgCanny subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


