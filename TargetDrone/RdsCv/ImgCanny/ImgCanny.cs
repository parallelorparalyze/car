using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;
using disp = Robotics.ImgDispTest.Proxy;

namespace Robotics.ImgCanny
{
    [Contract(Contract.Identifier)]
    [DisplayName("ImgCanny")]
    [Description("ImgCanny service (no description provided)")]
    class ImgCannyService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "ImgCanny.Config.xml")]
        ImgCannyState _state = new ImgCannyState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ImgCanny", AllowMultipleInstances = false)]
        ImgCannyOperations _mainPort = new ImgCannyOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private ImgCannyOperations _internalPort = new ImgCannyOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [Partner("ImgDispTest", Contract = disp.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        disp.ImgDispTestOperations _dispPort = new disp.ImgDispTestOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        //private volatile bool jobDone = false;

        /// <summary>
        /// Service constructor
        /// </summary>
        public ImgCannyService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start(); 
//            Console.WriteLine("Service starting...");
//            LogInfo("Image Canny starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new ImgCannyState();
            }

            // Initialize state
            if (_state.CannyThreshold <= 0)
            {
                _state.CannyThreshold = 180;
            }
            
            if (_state.CannyThresholdLinking <= 0)
            {
                _state.CannyThresholdLinking = 120;
            }

            if (_state.orderNumber < 1)
            {
                _state.orderNumber = 1;
            }

            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<NotifyImageCanny>(true, _internalPort, ImageCannyHandler),
                            Arbiter.Receive<capt.NotifyImageCapture>(true, _imgCaptNotify, ImageNotificationHandler),
                            Arbiter.Receive<CannyImage>(true, _internalPort, CannyImageHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
            //if (!jobDone)
            //{
//                LogInfo("Image notification from capture arrived");
                if ((update.Body.Order + 1) == _state.orderNumber)
                {
//                    Console.WriteLine("Image from camera received");
//                    LogInfo("Image from camera received");
                    Image<Bgr, byte> img = new Image<Bgr,byte>(update.Body.CaptureResImage);
                    _internalPort.Post(new CannyImage(new CannyImageRequest(img.Data)));
                    //jobDone = true;
                }
                else
                {
//                    Console.WriteLine("Not my image");
//                    LogInfo("Canny : Not my image");
                }
            //}
            //else
            //{
//            //    Console.WriteLine("My job's done");
//            //    LogInfo("Canny : My job's done");
            //}
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image capture success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void ImageCannyHandler(NotifyImageCanny update)
        {
//            LogInfo("Conv Done");
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
            if (_state.isLast == true)
            {
                _dispPort.DispImage(update.Body.CannyResImage);
            }
            else
            {
                _imgCaptPort.NotifyImageProcessing(update.Body.CannyResImage, _state.orderNumber);
            }
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Canny Image Handler
        /// </summary>
        /// <param name="request">CannyImageRequest</param>
        /// <returns></returns>
        public void CannyImageHandler(CannyImage request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForCanny);

                Image<Gray, byte> gimg = img.Convert<Gray, byte>().PyrDown().PyrUp();
                gimg = gimg.Canny(new Gray(_state.CannyThreshold), new Gray(_state.CannyThresholdLinking));

                img = gimg.Convert<Bgr, byte>();

                //yield return Arbiter.Receive(
                //    false,
                //    TimeoutPort(5000),
                //    delegate(DateTime time)
                //    { }
                //    );

                _internalPort.Post(new NotifyImageCanny(new CannyResult(img.Data)));
//                Console.WriteLine("Writing data...");
            }
        }
    }
}


