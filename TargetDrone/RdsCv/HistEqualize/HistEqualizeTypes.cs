using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using Robotics.HistEqualize;

namespace Robotics.HistEqualize
{
    /// <summary>
    /// HistEqualize contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for HistEqualize
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/09/histequalize.html";
    }

    /// <summary>
    /// HistEqualize state
    /// </summary>
    [DataContract]
    public class HistEqualizeState
    {
        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// HistEqualize main operations port
    /// </summary>
    [ServicePort]
    public class HistEqualizeOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, EqualizeHist, NotifyEqualizeHist>
    {
    }

    /// <summary>
    /// HistEqualize get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<HistEqualizeState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<HistEqualizeState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image EqualizeHist Operation
    /// </summary>
    public class EqualizeHist : Update<EqualizeHistRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public EqualizeHist()
        {
        }

        /// <summary>
        /// Constructor with EqualizeHistRequest
        /// </summary>
        public EqualizeHist(EqualizeHistRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// EqualizeHist Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class EqualizeHistRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForEqualizeHist;

        /// <summary>
        /// Constructor
        /// </summary>
        public EqualizeHistRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public EqualizeHistRequest(byte[, ,] request)
        {
            ImageForEqualizeHist = request;
        }
    }

    /// <summary>
    /// Image EqualizeHist Notification Operation
    /// </summary>
    [DisplayName("Image EqualizeHist done.")]
    [Description("Indicates that image EqualizeHist is done.")]
    public class NotifyEqualizeHist : Update<EqualizeHistResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyEqualizeHist()
        {
        }

        /// <summary>
        /// Constructor with EqualizeHistResult
        /// </summary>
        public NotifyEqualizeHist(EqualizeHistResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image EqualizeHist Result
    /// </summary>
    [DataContract]
    public class EqualizeHistResult
    {
        /// <summary>
        /// EqualizeHist Result
        /// </summary>
        [DataMember]
        [Description("The resulting EqualizeHist image data")]
        public byte[,,] EqualizeHistResImage;

        /// <summary>
        /// Constructor
        /// </summary>
        public EqualizeHistResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public EqualizeHistResult(byte[, ,] result)
        {
            EqualizeHistResImage = result;
        }
    }

    /// <summary>
    /// HistEqualize subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


