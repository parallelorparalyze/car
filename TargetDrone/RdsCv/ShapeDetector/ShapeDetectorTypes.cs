using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Robotics.ShapeDetector;
using System.Drawing;

namespace Robotics.ShapeDetector
{
    /// <summary>
    /// ShapeDetector contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ShapeDetector
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/11/shapedetector.html";
    }

    /// <summary>
    /// ShapeDetector state
    /// </summary>
    [DataContract]
    public class ShapeDetectorState
    {
        /// <summary>
        /// Canny Threshold to find intital segments of strong edges.
        /// </summary>
        [DataMember]
        [Description("Canny Threshold to find intital segments of strong edges")]
        public int CannyThreshold;

        /// <summary>
        /// Canny Threshold for edge linking.
        /// </summary>
        [DataMember]
        [Description("Canny Threshold for edge linking")]
        public int CannyThresholdLinking;

        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ShapeDetector main operations port
    /// </summary>
    [ServicePort]
    public class ShapeDetectorOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, DetectShapes, NotifyShapeDetection>
    {
    }

    /// <summary>
    /// ShapeDetector get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ShapeDetectorState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ShapeDetectorState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image HoughLines Operation
    /// </summary>
    public class DetectShapes : Update<DetectShapesRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public DetectShapes()
        {
        }

        /// <summary>
        /// Constructor with DetectShapesRequest
        /// </summary>
        public DetectShapes(DetectShapesRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// HoughLines Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class DetectShapesRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[, ,] ImageForShapeDetection;

        /// <summary>
        /// Constructor
        /// </summary>
        public DetectShapesRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public DetectShapesRequest(byte[, ,] request)
        {
            ImageForShapeDetection = request;
        }
    }

    /// <summary>
    /// Image Shape Detection Notification Operation
    /// </summary>
    [DisplayName("Shape Detection done.")]
    [Description("Indicates that image shape detection is done.")]
    public class NotifyShapeDetection : Update<DetectShapesResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyShapeDetection()
        {
        }

        /// <summary>
        /// Constructor with HoughLinesResult
        /// </summary>
        public NotifyShapeDetection(DetectShapesResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image Shape Detection Result
    /// </summary>
    [DataContract]
    public class DetectShapesResult
    {
        /// <summary>
        /// DetectShapes Result Image data
        /// </summary>
        [DataMember]
        [Description("The resulting DetectShapes Image Data")]
        public byte[, ,] DetectShapesResImg;

        /// <summary>
        /// Constructor
        /// </summary>
        public DetectShapesResult()
        {
        }
        /// <summary>
        /// Constructor with Detect Shapes Result Image
        /// </summary>
        public DetectShapesResult(byte[, ,] img)
        {
            DetectShapesResImg = img;
        }
    }

    /// <summary>
    /// ShapeDetector subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


