using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;
using disp = Robotics.ImgDispTest.Proxy;

namespace Robotics.ShapeDetector
{
    [Contract(Contract.Identifier)]
    [DisplayName("ShapeDetector")]
    [Description("ShapeDetector service (no description provided)")]
    class ShapeDetectorService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "ShapeDetector.Config.xml")]
        ShapeDetectorState _state = new ShapeDetectorState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ShapeDetector", AllowMultipleInstances = true)]
        ShapeDetectorOperations _mainPort = new ShapeDetectorOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private ShapeDetectorOperations _internalPort = new ShapeDetectorOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [Partner("ImgDispTest", Contract = disp.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        disp.ImgDispTestOperations _dispPort = new disp.ImgDispTestOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        /// <summary>
        /// Service constructor
        /// </summary>
        public ShapeDetectorService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
//            Console.WriteLine("Service starting...");
//            LogInfo("Shape Detector starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new ShapeDetectorState();
            }

            // Initialize state
            if (_state.CannyThreshold <= 0)
            {
                _state.CannyThreshold = 180;
            }

            if (_state.CannyThresholdLinking <= 0)
            {
                _state.CannyThresholdLinking = 120;
            }

            if (_state.orderNumber < 1)
            {
                _state.orderNumber = 1;
            }

            _state.isLast = true;

            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<NotifyShapeDetection>(true, _internalPort, ImageShapeDetectionHandler),
                            Arbiter.Receive<capt.NotifyImageCapture>(true, _imgCaptNotify, ImageNotificationHandler),
                            Arbiter.Receive<DetectShapes>(true, _internalPort, DetectShapesHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
            //if (!jobDone)
            //{
//            LogInfo("Image notification from capture arrived");
            if ((update.Body.Order + 1) == _state.orderNumber)
            {
//                Console.WriteLine("Image from camera received");
//                LogInfo("Image from camera received");
                Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
                _internalPort.Post(new DetectShapes(new DetectShapesRequest(img.Data)));
                //jobDone = true;
            }
            else
            {
//                Console.WriteLine("Not my image");
//                LogInfo("ShapeDetector : Not my image");
            }
            //}
            //else
            //{
//            //    Console.WriteLine("My job's done");
//            //    LogInfo("HoughLines : My job's done");
            //}
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image capture success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void ImageShapeDetectionHandler(NotifyShapeDetection update)
        {
//            LogInfo("Conv Done");
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
            if (_state.isLast == true)
            {
                _dispPort.DispImage(update.Body.DetectShapesResImg);
            }
            else
            {
                _imgCaptPort.NotifyImageProcessing(update.Body.DetectShapesResImg, _state.orderNumber);
            }
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// DetectShapes Handler
        /// </summary>
        /// <param name="request">DetectShapesRequest</param>
        /// <returns></returns>
        public void DetectShapesHandler(DetectShapes request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForShapeDetection);

                Image<Gray, byte> gray = img.Convert<Gray, byte>().PyrDown().PyrUp();
                gray = gray.Canny(new Gray(_state.CannyThreshold), new Gray(_state.CannyThresholdLinking));

                List<Triangle2DF> triangleList = new List<Triangle2DF>();
                List<MCvBox2D> boxList = new List<MCvBox2D>();

                MemStorage storage = new MemStorage();
                for (Contour<Point> contours = gray.FindContours(); contours != null; contours = contours.HNext)
                {
                    Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.05, storage);

                    if (contours.Area > 250) //only consider contours with area greater than 250
                    {
                        if (currentContour.Total == 3) //The contour has 3 vertices, it is a triangle
                        {
                            Point[] pts = currentContour.ToArray();
                            triangleList.Add(new Triangle2DF(
                               pts[0],
                               pts[1],
                               pts[2]
                               ));
                        }
                        else if (currentContour.Total == 4) //The contour has 4 vertices.
                        {
                            bool isRectangle = true;
                            Point[] pts = currentContour.ToArray();
                            LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                            for (int i = 0; i < edges.Length; i++)
                            {
                                double angle = Math.Abs(
                                   edges[(i + 1) % edges.Length].GetExteriorAngleDegree(edges[i]));
                                if (angle < 80 || angle > 100)
                                {
                                    isRectangle = false;
                                    break;
                                }
                            }

                            if (isRectangle) boxList.Add(currentContour.GetMinAreaRect());
                        }
                    }
                }

                Image<Bgr, byte> triangleRectangleImage = img.CopyBlank();
                foreach (Triangle2DF triangle in triangleList)
                    triangleRectangleImage.Draw(triangle, new Bgr(Color.DarkBlue), 2);
                foreach (MCvBox2D box in boxList)
                    triangleRectangleImage.Draw(box, new Bgr(Color.DarkOrange), 2);

                _internalPort.Post(new NotifyShapeDetection(new DetectShapesResult(triangleRectangleImage.Data)));
//                Console.WriteLine("Writing data...");

                img.Dispose();
                gray.Dispose();
                triangleRectangleImage.Dispose();
            }
        }
    }
}


