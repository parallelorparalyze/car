using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Robotics.ImgHoughCircles;
using System.Drawing;

namespace Robotics.ImgHoughCircles
{
    /// <summary>
    /// ImgHoughCircles contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgHoughCircles
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imghoughcircles.html";
    }

    /// <summary>
    /// ImgHoughCircles state
    /// </summary>
    [DataContract]
    public class ImgHoughCirclesState
    {
        /// <summary>
        /// Canny Threshold to find intital segments of strong edges.
        /// </summary>
        [DataMember]
        [Description("Canny Threshold to find intital segments of strong edges")]
        public int CannyThreshold;

        /// <summary>
        /// Accumulator Threshold at center  detection stage.
        /// </summary>
        [DataMember]
        [Description("Accumulator Threshold at center detection stage")]
        public int AccumulatorThreshold;

        /// <summary>
        /// Resolution of accumulator.
        /// </summary>
        [DataMember]
        [Description("Resolution of accumulator")]
        public double dp;

        /// <summary>
        /// Min dist between centers.
        /// </summary>
        [DataMember]
        [Description("Min dist between centers")]
        public double MinDist;

        /// <summary>
        /// Min radius.
        /// </summary>
        [DataMember]
        [Description("Min radius")]
        public int MinRadius;

        /// <summary>
        /// Max radius.
        /// </summary>
        [DataMember]
        [Description("Max radius")]
        public int MaxRadius;

        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImgHoughCircles main operations port
    /// </summary>
    [ServicePort]
    public class ImgHoughCirclesOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, HoughCirclesImage, NotifyImageHoughCircles>
    {
    }

    /// <summary>
    /// ImgHoughCircles get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgHoughCirclesState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgHoughCirclesState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image HoughCircles Operation
    /// </summary>
    public class HoughCirclesImage : Update<HoughCirclesImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public HoughCirclesImage()
        {
        }

        /// <summary>
        /// Constructor with HoughCirclesImageRequest
        /// </summary>
        public HoughCirclesImage(HoughCirclesImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// HoughCircles Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class HoughCirclesImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForHoughCircles;

        /// <summary>
        /// Constructor
        /// </summary>
        public HoughCirclesImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public HoughCirclesImageRequest(byte[,,] request)
        {
            ImageForHoughCircles = request;
        }
    }

    /// <summary>
    /// Image HoughCircles Notification Operation
    /// </summary>
    [DisplayName("ImageHoughCircles done.")]
    [Description("Indicates that image HoughCircles is done.")]
    public class NotifyImageHoughCircles : Update<HoughCirclesResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImageHoughCircles()
        {
        }

        /// <summary>
        /// Constructor with HoughCirclesResult
        /// </summary>
        public NotifyImageHoughCircles(HoughCirclesResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image HoughCircles Result
    /// </summary>
    [DataContract]
    public class HoughCirclesResult
    {
        /// <summary>
        /// HoughCircles Result Image Data
        /// </summary>
        [DataMember]
        [Description("The resulting HoughCircles Image Data")]
        public byte[,,] HoughCirclesResImg;

        /// <summary>
        /// HoughCircles Result
        /// </summary>
        [DataMember]
        [Description("The resulting HoughCircles")]
        public CircleF[] HoughCirclesRes;

        /// <summary>
        /// Constructor
        /// </summary>
        public HoughCirclesResult()
        {
        }
        /// <summary>
        /// Constructor with HoughCircles Result (CircleF[])
        /// </summary>
        public HoughCirclesResult(byte[,,] img, CircleF[] result)
        {
            HoughCirclesResImg = img;
            HoughCirclesRes = result;
        }
    }

    /// <summary>
    /// ImgHoughCircles subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


