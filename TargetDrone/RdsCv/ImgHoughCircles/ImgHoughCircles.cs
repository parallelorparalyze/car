using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;
using disp = Robotics.ImgDispTest.Proxy;

namespace Robotics.ImgHoughCircles
{
    [Contract(Contract.Identifier)]
    [DisplayName("ImgHoughCircles")]
    [Description("ImgHoughCircles service (no description provided)")]
    class ImgHoughCirclesService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "ImgHoughCircles.Config.xml")]
        ImgHoughCirclesState _state = new ImgHoughCirclesState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ImgHoughCircles", AllowMultipleInstances = true)]
        ImgHoughCirclesOperations _mainPort = new ImgHoughCirclesOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private ImgHoughCirclesOperations _internalPort = new ImgHoughCirclesOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [Partner("ImgDispTest", Contract = disp.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        disp.ImgDispTestOperations _dispPort = new disp.ImgDispTestOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        //private volatile bool jobDone = false;

        /// <summary>
        /// Service constructor
        /// </summary>
        public ImgHoughCirclesService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 
            base.Start();
//            Console.WriteLine("Service starting...");
//            LogInfo("Image HoughCircles starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new ImgHoughCirclesState();
            }

            // Initialize state
            if (_state.CannyThreshold <= 0)
            {
                _state.CannyThreshold = 180;
            }

            if (_state.AccumulatorThreshold <= 0)
            {
                _state.AccumulatorThreshold = 120;
            }

            if (_state.dp <= 0.0)
            {
                _state.dp = 5.0;
            }

            if (_state.MinDist <= 0.0)
            {
                _state.MinDist = 10.0;
            }

            if (_state.MinRadius <= 0)
            {
                _state.MinRadius = 5;
            }

            if (_state.MaxRadius < 0)
            {
                _state.MaxRadius = 0;
            }

            if (_state.orderNumber < 1)
            {
                _state.orderNumber = 1;
            }
            
            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<NotifyImageHoughCircles>(true, _internalPort, ImageHoughCirclesHandler),
                            Arbiter.Receive<capt.NotifyImageCapture>(true, _imgCaptNotify, ImageNotificationHandler),
                            Arbiter.Receive<HoughCirclesImage>(true, _internalPort, HoughCirclesImageHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
            //if (!jobDone)
            //{
//                LogInfo("Image notification from capture arrived");
                if ((update.Body.Order + 1) == _state.orderNumber)
                {
//                    Console.WriteLine("Image from camera received");
//                    LogInfo("Image from camera received");
                    Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
                    _internalPort.Post(new HoughCirclesImage(new HoughCirclesImageRequest(img.Data)));
                    //jobDone = true;
                }
                else
                {
//                    Console.WriteLine("Not my image");
//                    LogInfo("HoughCircles : Not my image");
                }
            //}
            //else
            //{
//            //    Console.WriteLine("My job's done");
//            //    LogInfo("HoughCircles : My job's done");
            //}
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image capture success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void ImageHoughCirclesHandler(NotifyImageHoughCircles update)
        {
//            LogInfo("Conv Done");
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
            if (_state.isLast == true)
            {
                _dispPort.DispImage(update.Body.HoughCirclesResImg);
            }
            else
            {
                _imgCaptPort.NotifyImageProcessing(update.Body.HoughCirclesResImg, _state.orderNumber);
            }
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// HoughCircles Image Handler
        /// </summary>
        /// <param name="request">HoughCirclesImageRequest</param>
        /// <returns></returns>
        public void HoughCirclesImageHandler(HoughCirclesImage request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForHoughCircles);

                Image<Gray, byte> gimg = img.Convert<Gray, byte>().PyrDown().PyrUp();

                CircleF[] circles = gimg.HoughCircles(new Gray(_state.CannyThreshold), new Gray(_state.AccumulatorThreshold), _state.dp, _state.MinDist, _state.MinRadius, _state.MaxRadius)[0];

                Image<Bgr, Byte> circleImage = img.CopyBlank();
                foreach (CircleF circle in circles)
                    circleImage.Draw(circle, new Bgr(Color.Brown), 1);

                _internalPort.Post(new NotifyImageHoughCircles(new HoughCirclesResult(circleImage.Data, circles)));
//                Console.WriteLine("Writing data...");

                /* ImageViewer iv = new ImageViewer();
                iv.Image = circleImage;
                iv.ShowDialog(); */
            }
        }
    }
}


