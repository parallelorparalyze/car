using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;
using disp = Robotics.ImgDispTest.Proxy;

namespace Robotics.FaceDetection
{
    [Contract(Contract.Identifier)]
    [DisplayName("FaceDetection")]
    [Description("FaceDetection service (no description provided)")]
    class FaceDetectionService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "FaceDetection.Config.xml")]
        FaceDetectionState _state = new FaceDetectionState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/FaceDetection", AllowMultipleInstances = true)]
        FaceDetectionOperations _mainPort = new FaceDetectionOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private FaceDetectionOperations _internalPort = new FaceDetectionOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [Partner("ImgDispTest", Contract = disp.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        disp.ImgDispTestOperations _dispPort = new disp.ImgDispTestOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        private HaarCascade haar;

        /// <summary>
        /// Service constructor
        /// </summary>
        public FaceDetectionService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
//            Console.WriteLine("Service starting...");
//            LogInfo("Face Detection starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new FaceDetectionState();
            }

            // Initialize state
            if (_state.ScaleFactor <= 0)
            {
                _state.ScaleFactor = 1.4;
            }

            if (_state.MinNeighbors < 1)
            {
                _state.MinNeighbors = 4;
            }

            if (_state.orderNumber < 1)
            {
                _state.orderNumber = 1;
            }

            SaveState(_state);

            haar = new HaarCascade("haarcascade_frontalface_alt2.xml");

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<NotifyFaceDetection>(true, _internalPort, FaceDetectNotificationHandler),
                            Arbiter.Receive<capt.NotifyImageCapture>(true, _imgCaptNotify, ImageNotificationHandler),
                            Arbiter.Receive<DetectFace>(true, _internalPort, DetectFaceHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
            //if (!jobDone)
            //{
//            LogInfo("Image notification from capture arrived");
            if ((update.Body.Order + 1) == _state.orderNumber)
            {
//                Console.WriteLine("Image from camera received");
//                LogInfo("Image from camera received");
                Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
                _internalPort.Post(new DetectFace(new DetectFaceRequest(img.Data)));
                //jobDone = true;
            }
            else
            {
//                Console.WriteLine("Not my image");
//                LogInfo("FaceDetection : Not my image");
            }
            //}
            //else
            //{
//            //    Console.WriteLine("My job's done");
//            //    LogInfo("HoughCircles : My job's done");
            //}
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image capture success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void FaceDetectNotificationHandler(NotifyFaceDetection update)
        {
//            LogInfo("Conv Done");
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
            if (_state.isLast == true)
            {
                _dispPort.DispImage(update.Body.FaceDetectResImg);
            }
            else
            {
                _imgCaptPort.NotifyImageProcessing(update.Body.FaceDetectResImg, _state.orderNumber);
            }
        }


        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Face Detection Handler
        /// </summary>
        /// <param name="request">DetectFaceRequest</param>
        /// <returns></returns>
        public void DetectFaceHandler(DetectFace request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForDetectFace);

                Image<Gray, byte> grayImg = img.Convert<Gray, byte>();


#pragma warning disable 612,618
                var faces = grayImg.DetectHaarCascade(
#pragma warning restore 612,618
                                    haar,
                                    _state.ScaleFactor,
                                    _state.MinNeighbors,
                                    Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
                                    new Size(img.Width/8, img.Height/8)
                                    )[0];

                foreach (var face in faces)
                {
                    img.Draw(face.rect, new Bgr(0, double.MaxValue, 0), 3);
                }

                _internalPort.Post(new NotifyFaceDetection(new FaceDetectResult(img.Data)));
//                Console.WriteLine("Writing data...");
            }
        }
    }
}


