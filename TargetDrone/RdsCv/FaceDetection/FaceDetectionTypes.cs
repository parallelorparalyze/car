using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Robotics.FaceDetection;
using System.Drawing;

namespace Robotics.FaceDetection
{
    /// <summary>
    /// FaceDetection contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for FaceDetection
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/11/facedetection.html";
    }

    /// <summary>
    /// FaceDetection state
    /// </summary>
    [DataContract]
    public class FaceDetectionState
    {
        /// <summary>
        /// Scale Factor for haar face detection
        /// </summary>
        [DataMember]
        [Description("Scale Factor for haar face detection")]
        public double ScaleFactor;

        /// <summary>
        /// Minimum neighbors for classification as face
        /// </summary>
        [DataMember]
        [Description("Minimum neighbors for classification as face")]
        public int MinNeighbors;

        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// FaceDetection main operations port
    /// </summary>
    [ServicePort]
    public class FaceDetectionOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, DetectFace, NotifyFaceDetection>
    {
    }

    /// <summary>
    /// FaceDetection get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<FaceDetectionState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<FaceDetectionState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image HoughCircles Operation
    /// </summary>
    public class DetectFace : Update<DetectFaceRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public DetectFace()
        {
        }

        /// <summary>
        /// Constructor with DetectFaceRequest
        /// </summary>
        public DetectFace(DetectFaceRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// DetectFace Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class DetectFaceRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[, ,] ImageForDetectFace;

        /// <summary>
        /// Constructor
        /// </summary>
        public DetectFaceRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public DetectFaceRequest(byte[, ,] request)
        {
            ImageForDetectFace = request;
        }
    }

    /// <summary>
    /// Detect Face Notification Operation
    /// </summary>
    [DisplayName("Face Detection done.")]
    [Description("Indicates that face detection is done.")]
    public class NotifyFaceDetection : Update<FaceDetectResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyFaceDetection()
        {
        }

        /// <summary>
        /// Constructor with FaceDetectResult
        /// </summary>
        public NotifyFaceDetection(FaceDetectResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// FaceDetect Result
    /// </summary>
    [DataContract]
    public class FaceDetectResult
    {
        /// <summary>
        /// FaceDetect Result Image Data
        /// </summary>
        [DataMember]
        [Description("The resulting Face Detect Image Data")]
        public byte[, ,] FaceDetectResImg;

        /// <summary>
        /// Constructor
        /// </summary>
        public FaceDetectResult()
        {
        }
        /// <summary>
        /// Constructor with FaceDetect Result
        /// </summary>
        public FaceDetectResult(byte[, ,] img)
        {
            FaceDetectResImg = img;
        }
    }

    /// <summary>
    /// FaceDetection subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


