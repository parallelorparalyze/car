using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using Robotics.ImgPyrDown;

namespace Robotics.ImgPyrDown
{
    /// <summary>
    /// ImgPyrDown contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgPyrDown
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imgpyrdown.html";
    }

    /// <summary>
    /// ImgPyrDown state
    /// </summary>
    [DataContract]
    public class ImgPyrDownState
    {
        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImgPyrDown main operations port
    /// </summary>
    [ServicePort]
    public class ImgPyrDownOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, PyrDownImage, NotifyImagePyrDown>
    {
    }

    /// <summary>
    /// ImgPyrDown get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgPyrDownState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgPyrDownState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image PyrDown Operation
    /// </summary>
    public class PyrDownImage : Update<PyrDownImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public PyrDownImage()
        {
        }

        /// <summary>
        /// Constructor with PyrDownImageRequest
        /// </summary>
        public PyrDownImage(PyrDownImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// PyrDown Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class PyrDownImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForPyrDown;

        /// <summary>
        /// Constructor
        /// </summary>
        public PyrDownImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public PyrDownImageRequest(byte[,,] request)
        {
            ImageForPyrDown = request;
        }
    }

    /// <summary>
    /// Image PyrDown Notification Operation
    /// </summary>
    [DisplayName("ImagePyrDown done.")]
    [Description("Indicates that image PyrDown is done.")]
    public class NotifyImagePyrDown : Update<PyrDownResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImagePyrDown()
        {
        }

        /// <summary>
        /// Constructor with PyrDownResult
        /// </summary>
        public NotifyImagePyrDown(PyrDownResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image PyrDown Result
    /// </summary>
    [DataContract]
    public class PyrDownResult
    {
        /// <summary>
        /// PyrDown Result
        /// </summary>
        [DataMember]
        [Description("The resulting PyrDown image data")]
        public byte[, ,] PyrDownResImage;

        /// <summary>
        /// Constructor
        /// </summary>
        public PyrDownResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public PyrDownResult(byte[, ,] result)
        {
            PyrDownResImage = result;
        }
    }

    /// <summary>
    /// ImgPyrDown subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


