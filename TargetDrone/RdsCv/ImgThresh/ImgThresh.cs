using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;
using disp = Robotics.ImgDispTest.Proxy;

namespace Robotics.ImgThresh
{
    [Contract(Contract.Identifier)]
    [DisplayName("ImgThresh")]
    [Description("ImgThresh service (no description provided)")]
    class ImgThreshService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "ImgThresh.Config.xml")]
        ImgThreshState _state = new ImgThreshState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ImgThresh", AllowMultipleInstances = true)]
        ImgThreshOperations _mainPort = new ImgThreshOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private ImgThreshOperations _internalPort = new ImgThreshOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [Partner("ImgDispTest", Contract = disp.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        disp.ImgDispTestOperations _dispPort = new disp.ImgDispTestOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        //private volatile bool jobDone = false;

        /// <summary>
        /// Service constructor
        /// </summary>
        public ImgThreshService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
//            Console.WriteLine("Service Starting...");
//            LogInfo("Image Thresholding starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new ImgThreshState();
            }

            // Initialize state variables

            if ((_state.ThreshType < 1) || (_state.ThreshType > 5))
            {
                _state.ThreshType = 1;
            }

            if (_state.Threshold <= 0)
            {
                _state.Threshold = 100;
            }

            if (_state.MaxValue <= 0)
            {
                _state.MaxValue = 200;
            }

            if (_state.orderNumber < 1)
            {
                _state.orderNumber = 1;
            }

            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<NotifyImageThresholding>(true, _internalPort, ImageThresholdedHandler),
                            Arbiter.Receive<capt.NotifyImageCapture>(true, _imgCaptNotify, ImageNotificationHandler),
                            Arbiter.Receive<ThresholdImage>(true, _internalPort, ThresholdImageHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
            //if (!jobDone)
            //{
//                LogInfo("Image notification from capture arrived");
                if ((update.Body.Order + 1) == _state.orderNumber)
                {
//                    Console.WriteLine("Image from camera received");
//                    LogInfo("Image from camera received");
                    Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
                    _internalPort.Post(new ThresholdImage(new ThresholdImageRequest(img.Data)));
                   // jobDone = true;
                }
                else
                {
//                    Console.WriteLine("Not my image");
//                    LogInfo("Thresholding : Not my image");
                }
            //}
            //else
            //{
//            //    Console.WriteLine("My job's done");
//            //    LogInfo("Thresholding : My job's done");
            //}
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image capture success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void ImageThresholdedHandler(NotifyImageThresholding update)
        {
//            LogInfo("Conv Done");
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
            if (_state.isLast == true)
            {
                _dispPort.DispImage(update.Body.ThresholdedImage);
            }
            else
            {
                _imgCaptPort.NotifyImageProcessing(update.Body.ThresholdedImage, _state.orderNumber);
            }
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Threshold Image Handler
        /// </summary>
        /// <param name="request">ThresholdImageRequest</param>
        /// <returns></returns>
        public void ThresholdImageHandler(ThresholdImage request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForThresholding);
                switch (_state.ThreshType)
                {
                    case 1:
                        CvInvoke.cvThreshold(img.Ptr, img.Ptr, _state.Threshold, _state.MaxValue, Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY);
                        break;

                    case 2:
                        CvInvoke.cvThreshold(img.Ptr, img.Ptr, _state.Threshold, _state.MaxValue, Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY_INV);
                        break;

                    case 3:
                        CvInvoke.cvThreshold(img.Ptr, img.Ptr, _state.Threshold, _state.MaxValue, Emgu.CV.CvEnum.THRESH.CV_THRESH_TOZERO);
                        break;

                    case 4:
                        CvInvoke.cvThreshold(img.Ptr, img.Ptr, _state.Threshold, _state.MaxValue, Emgu.CV.CvEnum.THRESH.CV_THRESH_TOZERO_INV);
                        break;

                    case 5:
                        CvInvoke.cvThreshold(img.Ptr, img.Ptr, _state.Threshold, _state.MaxValue, Emgu.CV.CvEnum.THRESH.CV_THRESH_TRUNC);
                        break;
                }

                _internalPort.Post(new NotifyImageThresholding(new ThresholdingResult(img.Data)));
//                Console.WriteLine("Writing data...");

                /* ImageViewer iv = new ImageViewer();
                iv.Image = img;
                iv.ShowDialog(); */
            }
        }
    }
}


