using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Robotics.ImgThresh;
using System.Drawing;

namespace Robotics.ImgThresh
{
    /// <summary>
    /// ImgThresh contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgThresh
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imgthresh.html";
    }

    /// <summary>
    /// ImgThresh state
    /// </summary>
    [DataContract]
    public class ImgThreshState
    {
        /// <summary>
        /// Threshold Type : 1 -> Binary, 2 -> BinaryInv, 3 -> ToZero, 4 -> ToZeroInv, 5 -> Trunc (default Binary)
        /// </summary>
        [DataMember]
        [Description("Threshold Type : 1 -> Binary, 2 -> BinaryInv, 3 -> ToZero, 4 -> ToZeroInv, 5 -> Trunc (default Binary)")]
        public int ThreshType;

        /// <summary>
        /// Threshold value
        /// </summary>
        [DataMember]
        [Description("Specifies Threshold value.")]
        public double Threshold;

        /// <summary>
        /// Max value
        /// </summary>
        [DataMember]
        [Description("Specifies Max value.")]
        public double MaxValue;

        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImgThresh main operations port
    /// </summary>
    [ServicePort]
    public class ImgThreshOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, ThresholdImage, NotifyImageThresholding>
    {
    }

    /// <summary>
    /// ImgThresh get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgThreshState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgThreshState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image Thresholding Operation
    /// </summary>
    public class ThresholdImage : Update<ThresholdImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public ThresholdImage()
        {
        }

        /// <summary>
        /// Constructor with ThresholdImageRequest
        /// </summary>
        public ThresholdImage(ThresholdImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Threshold Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class ThresholdImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForThresholding;

        /// <summary>
        /// Constructor
        /// </summary>
        public ThresholdImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public ThresholdImageRequest(byte[,,] request)
        {
            ImageForThresholding = request;
        }
    }

    /// <summary>
    /// Image Thresholding Notification Operation
    /// </summary>
    [DisplayName("ImageThresholded")]
    [Description("Indicates that image has been Thresholded.")]
    public class NotifyImageThresholding : Update<ThresholdingResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImageThresholding()
        {
        }

        /// <summary>
        /// Constructor with ThresholdingResult
        /// </summary>
        public NotifyImageThresholding(ThresholdingResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image Thresholding Result
    /// </summary>
    [DataContract]
    public class ThresholdingResult
    {
        /// <summary>
        /// Thresholding Result
        /// </summary>
        [DataMember]
        [Description("The resulting Thresholded image data")]
        public byte[,,] ThresholdedImage;

        /// <summary>
        /// Constructor
        /// </summary>
        public ThresholdingResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public ThresholdingResult(byte[,,] result)
        {
            ThresholdedImage = result;
        }
    }

    /// <summary>
    /// ImgThresh subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


