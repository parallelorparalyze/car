using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.UI;
using Emgu.Util;
using Emgu.CV.Structure;

namespace Robotics.ImageCapt
{
    /// <summary>
    /// ImageCapt contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifier for ImageCapt
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/09/imagecapt.html";
    }

    /// <summary>
    /// ImageCapt state
    /// </summary>
    [DataContract]
    public class ImageCaptState
    {
        /// <summary>
        /// Image Input Type : 0 -> Camera, 1 -> File
        /// </summary>
        [DataMember]
        public int ImgInputType;

        /// <summary>
        /// Filename to read image from
        /// </summary>
        [DataMember]
        public string ImgFileName;

        /// <summary>
        /// Number of image processing services in orchestration
        /// </summary>
        [DataMember]
        public int NumServices;
    }

    /// <summary>
    /// ImageCapt main operations port
    /// </summary>
    [ServicePort]
    public class ImageCaptOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, NotifyImageCapture, NotifyImageProcessing>
    {
    }

    /// <summary>
    /// ImageCapt get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImageCaptState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImageCaptState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image Capture Notification Operation
    /// </summary>
    [DisplayName("ImageCaptureNotification")]
    [Description("Indicates that image capture is done.")]
    public class NotifyImageCapture : Update<CaptureResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImageCapture()
        {
        }

        /// <summary>
        /// Constructor with CaptureResult
        /// </summary>
        public NotifyImageCapture(CaptureResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image Processing Notification Operation
    /// </summary>
    [DisplayName("ImageProcessingNotification")]
    [Description("Indicates that processed image data has arrived.")]
    public class NotifyImageProcessing : Update<ProcessingResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImageProcessing()
        {
        }
    }

    /// <summary>
    /// Image Capture Result
    /// </summary>
    [DataContract]
    public class CaptureResult
    {
        /// <summary>
        /// Capture Result
        /// </summary>
        [DataMember]
        [Description("The resulting Capture image data")]
        public byte[,,] CaptureResImage;

        /// <summary>
        /// Order Number
        /// </summary>
        [DataMember]
        [Description("The order number for arbitration listening services")]
        public int Order;

        /// <summary>
        /// Constructor
        /// </summary>
        public CaptureResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (Bitmap)
        /// </summary>
        public CaptureResult(byte[,,] result, int order)
        {
            CaptureResImage = result;
            Order = order;
        }
    }

    /// <summary>
    /// Image Processing Result
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class ProcessingResult
    {
        /// <summary>
        /// Processing Result
        /// </summary>
        [DataMember, DataMemberConstructor]
        [Description("The resulting processed image data")]
        public byte[,,] ProcessingResImage;

        /// <summary>
        /// New Order Number
        /// </summary>
        [DataMember, DataMemberConstructor]
        [Description("The new order number for arbitration listening services")]
        public int newOrder;

        /// <summary>
        /// Constructor
        /// </summary>
        public ProcessingResult()
        {
        }

        /// <summary>
        /// Constructor with Image Data Request (byte[,,]) and order number
        /// </summary>
        public ProcessingResult(byte[, ,] request, int orderNum)
        {
            ProcessingResImage = request;
            newOrder = orderNum;
        }
    }

    /// <summary>
    /// ImageCapt subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


