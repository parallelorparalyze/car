using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using Robotics.BgrToGray;

namespace Robotics.BgrToGray
{
    /// <summary>
    /// BgrToGray contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for BgrToGray
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/09/bgrtogray.html";
    }

    /// <summary>
    /// BgrToGray state
    /// </summary>
    [DataContract]
    public class BgrToGrayState
    {
        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// BgrToGray main operations port
    /// </summary>
    [ServicePort]
    public class BgrToGrayOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, ConvBgrToGray, NotifyBgrToGray>
    {
    }

    /// <summary>
    /// BgrToGray get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<BgrToGrayState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<BgrToGrayState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Histogram Calculation operation
    /// </summary>
    public class ConvBgrToGray : Update<ConvBgrToGrayRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public ConvBgrToGray()
        {
        }

        /// <summary>
        /// Constructor with ConvBgrToGrayRequest
        /// </summary>
        public ConvBgrToGray(ConvBgrToGrayRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Convert BgrToGray Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class ConvBgrToGrayRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForBgrToGray;

        /// <summary>
        /// Constructor
        /// </summary>
        public ConvBgrToGrayRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public ConvBgrToGrayRequest(byte[,,] request)
        {
            ImageForBgrToGray = request;
        }
    }

    /// <summary>
    /// BgrToGray Notification Operation
    /// </summary>
    [DisplayName("CalculatedBgrToGray")]
    [Description("Indicates that a BgrToGray is done.")]
    public class NotifyBgrToGray : Update<BgrToGrayResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyBgrToGray()
        {
        }

        /// <summary>
        /// Constructor with BgrToGrayResult
        /// </summary>
        public NotifyBgrToGray(BgrToGrayResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// BgrToGray Result
    /// </summary>
    [DataContract]
    public class BgrToGrayResult
    {
        /// <summary>
        /// BgrToGray Result
        /// </summary>
        [DataMember]
        [Description("The resulting Gray Image data")]
        public byte[,,] GrayImg;

        /// <summary>
        /// Constructor
        /// </summary>
        public BgrToGrayResult()
        {
        }
        /// <summary>
        /// Constructor with BgrToGrayResult
        /// </summary>
        public BgrToGrayResult(byte[,,] result)
        {
            GrayImg = result;
        }
    }

    /// <summary>
    /// BgrToGray subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


