using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;
using disp = Robotics.ImgDispTest.Proxy;

namespace Robotics.ImgPyrSeg
{
    [Contract(Contract.Identifier)]
    [DisplayName("ImgPyrSeg")]
    [Description("ImgPyrSeg service (no description provided)")]
    class ImgPyrSegService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "ImgPyrSeg.Config.xml")]
        ImgPyrSegState _state = new ImgPyrSegState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ImgPyrSeg", AllowMultipleInstances = true)]
        ImgPyrSegOperations _mainPort = new ImgPyrSegOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private ImgPyrSegOperations _internalPort = new ImgPyrSegOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [Partner("ImgDispTest", Contract = disp.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        disp.ImgDispTestOperations _dispPort = new disp.ImgDispTestOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        //private volatile bool jobDone = false;

        /// <summary>
        /// Service constructor
        /// </summary>
        public ImgPyrSegService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
//            Console.WriteLine("Service starting...");
//            LogInfo("Image PyrSeg starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new ImgPyrSegState();
            }

            // Initialize state
            if (_state.Level <= 0)
            {
                _state.Level = 4;
            }

            if (_state.Threshold1 <= 0)
            {
                _state.Threshold1 = 200;
            }

            if (_state.Threshold2 <= 0)
            {
                _state.Threshold2 = 30;
            }

            if (_state.orderNumber < 1)
            {
                _state.orderNumber = 1;
            }

            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<NotifyImagePyrSeg>(true, _internalPort, ImagePyrSegHandler),
                            Arbiter.Receive<capt.NotifyImageCapture>(true, _imgCaptNotify, ImageNotificationHandler),
                            Arbiter.Receive<PyrSegImage>(true, _internalPort, PyrSegImageHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
            //if (!jobDone)
            //{
//                LogInfo("Image notification from capture arrived");
                if ((update.Body.Order + 1) == _state.orderNumber)
                {
//                    Console.WriteLine("Image from camera received");
//                    LogInfo("Image from camera received");
                    Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
                    _internalPort.Post(new PyrSegImage(new PyrSegImageRequest(img.Data)));
                    //jobDone = true;
                }
                else
                {
//                    Console.WriteLine("Not my image");
//                    LogInfo("PyrSeg : Not my image");
                }
            //}
            //else
            //{
//            //    Console.WriteLine("My job's done");
//            //    LogInfo("PyrSeg : My job's done");
            //}
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image capture success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void ImagePyrSegHandler(NotifyImagePyrSeg update)
        {
//            LogInfo("Conv Done");
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
            if (_state.isLast == true)
            {
                _dispPort.DispImage(update.Body.PyrSegResImage);
            }
            else
            {
                _imgCaptPort.NotifyImageProcessing(update.Body.PyrSegResImage, _state.orderNumber);
            }
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// PyrSeg Image Handler
        /// </summary>
        /// <param name="request">PyrSegImageRequest</param>
        /// <returns></returns>
        public void PyrSegImageHandler(PyrSegImage request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForPyrSeg);
                Image<Bgr, byte> newImg = new Image<Bgr, byte>(img.Size);
                MemStorage store = new MemStorage();
                MCvSeq seq = new MCvSeq();

                CvInvoke.cvPyrSegmentation(img.Ptr, newImg.Ptr, store.Ptr, out seq.ptr, _state.Level, _state.Threshold1, _state.Threshold2);

                _internalPort.Post(new NotifyImagePyrSeg(new PyrSegResult(newImg.Data)));
//                Console.WriteLine("Writing data...");

                /* ImageViewer iv = new ImageViewer();
                iv.Image = newImg;
                iv.ShowDialog(); */
            }
        }
    }
}


