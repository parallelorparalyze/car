using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Robotics.ImgPyrSeg;
using System.Drawing;

namespace Robotics.ImgPyrSeg
{
    /// <summary>
    /// ImgPyrSeg contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgPyrSeg
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imgpyrseg.html";
    }

    /// <summary>
    /// ImgPyrSeg state
    /// </summary>
    [DataContract]
    public class ImgPyrSegState
    {
        /// <summary>
        /// Level of Pyramids value
        /// </summary>
        [DataMember]
        [Description("Specifies Level of Pyramids.")]
        public int Level;

        /// <summary>
        /// Threshold1 value for establishing links
        /// </summary>
        [DataMember]
        [Description("Specifies Threshold1 value for establishing links.")]
        public double Threshold1;

        /// <summary>
        /// Threshold2 value for segment clustering
        /// </summary>
        [DataMember]
        [Description("Specifies Threshold2 value for segment clustering.")]
        public double Threshold2;

        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImgPyrSeg main operations port
    /// </summary>
    [ServicePort]
    public class ImgPyrSegOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, PyrSegImage, NotifyImagePyrSeg>
    {
    }

    /// <summary>
    /// ImgPyrSeg get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgPyrSegState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgPyrSegState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image PyrSeg Operation
    /// </summary>
    public class PyrSegImage : Update<PyrSegImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public PyrSegImage()
        {
        }

        /// <summary>
        /// Constructor with PyrSegImageRequest
        /// </summary>
        public PyrSegImage(PyrSegImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// PyrSeg Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class PyrSegImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForPyrSeg;

        /// <summary>
        /// Constructor
        /// </summary>
        public PyrSegImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public PyrSegImageRequest(byte[,,] request)
        {
            ImageForPyrSeg = request;
        }
    }

    /// <summary>
    /// Image PyrSeg Notification Operation
    /// </summary>
    [DisplayName("ImagePyrSeg done.")]
    [Description("Indicates that image PyrSeg is done.")]
    public class NotifyImagePyrSeg : Update<PyrSegResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImagePyrSeg()
        {
        }

        /// <summary>
        /// Constructor with PyrSegResult
        /// </summary>
        public NotifyImagePyrSeg(PyrSegResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image PyrSeg Result
    /// </summary>
    [DataContract]
    public class PyrSegResult
    {
        /// <summary>
        /// PyrSeg Result
        /// </summary>
        [DataMember]
        [Description("The resulting PyrSeg image data")]
        public byte[, ,] PyrSegResImage;

        /// <summary>
        /// Constructor
        /// </summary>
        public PyrSegResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public PyrSegResult(byte[, ,] result)
        {
            PyrSegResImage = result;
        }
    }

    /// <summary>
    /// ImgPyrSeg subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


