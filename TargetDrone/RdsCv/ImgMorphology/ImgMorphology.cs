using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using capt = Robotics.ImageCapt.Proxy;
using disp = Robotics.ImgDispTest.Proxy;

namespace Robotics.ImgMorphology
{
    [Contract(Contract.Identifier)]
    [DisplayName("ImgMorphology")]
    [Description("ImgMorphology service (no description provided)")]
    class ImgMorphologyService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        [InitialStatePartner(Optional = true, ServiceUri = "ImgMorphology.Config.xml")]
        ImgMorphologyState _state = new ImgMorphologyState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ImgMorphology", AllowMultipleInstances = true)]
        ImgMorphologyOperations _mainPort = new ImgMorphologyOperations();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private ImgMorphologyOperations _internalPort = new ImgMorphologyOperations();

        [Partner("ImageCapture", Contract = capt.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        capt.ImageCaptOperations _imgCaptPort = new capt.ImageCaptOperations();
        capt.ImageCaptOperations _imgCaptNotify = new capt.ImageCaptOperations();

        [Partner("ImgDispTest", Contract = disp.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        disp.ImgDispTestOperations _dispPort = new disp.ImgDispTestOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        //private volatile bool jobDone = false;

        /// <summary>
        /// Service constructor
        /// </summary>
        public ImgMorphologyService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
//            Console.WriteLine("Service starting...");
//            LogInfo("Image Morphology starting...");

            // Make sure that we have an initial state!
            if (_state == null)
            {
                _state = new ImgMorphologyState();
            }

            // Initialize state variables
            if ((_state.MorphType < 1) || (_state.MorphType > 5))
            {
                _state.MorphType = 1;
            }
            
            if (_state.Iterations <= 0 )
            {
                _state.Iterations = 1;
            }

            if (_state.StrucElemCols <= 0 )
            {
                _state.StrucElemCols = 5;
            }

            if (_state.StrucElemRows <= 0 )
            {
                _state.StrucElemRows = 5;
            }

            if (_state.StrucElemAnchorX < 0 )
            {
                _state.StrucElemAnchorX = 0;
            }

            if (_state.StrucElemAnchorY < 0 )
            {
                _state.StrucElemAnchorY = 0;
            }

            if ((_state.StrucElemShape < 1) || (_state.StrucElemShape > 3))
            {
                _state.StrucElemShape = 3;
            }

            if (_state.orderNumber < 1)
            {
                _state.orderNumber = 1;
            }
            
            SaveState(_state);

            SpawnIterator(SubscribeToCapture);

            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(),
                    new ConcurrentReceiverGroup
                        (
                            Arbiter.Receive<NotifyImageMorphology>(true, _internalPort, ImageMorphologyHandler),
                            Arbiter.Receive<capt.NotifyImageCapture>(true, _imgCaptNotify, ImageNotificationHandler),
                            Arbiter.Receive<MorphImage>(true, _internalPort, MorphImageHandler)
                        )
            ));
        }

        public void ImageNotificationHandler(capt.NotifyImageCapture update)
        {
            //if (!jobDone)
            //{
//                LogInfo("Image notification from capture arrived");
                if ((update.Body.Order + 1) == _state.orderNumber)
                {
//                    Console.WriteLine("Image from camera received");
//                    LogInfo("Image from camera received");
                    Image<Bgr, byte> img = new Image<Bgr, byte>(update.Body.CaptureResImage);
                    _internalPort.Post(new MorphImage(new MorphImageRequest(img.Data)));
                    //jobDone = true;
                }
                else
                {
//                    Console.WriteLine("Not my image");
//                    LogInfo("Morphology : Not my image");
                }
            //}
            //else
            //{
//             //   Console.WriteLine("My job's done");
//            //    LogInfo("Morphology : My job's done");
            //}
        }

        // Handler for subscribing to Image Capture Service
        IEnumerator<ITask> SubscribeToCapture()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the webcam
            capt.Subscribe subscribe = new capt.Subscribe();
            subscribe.NotificationPort = _imgCaptNotify;

            _imgCaptPort.Post(subscribe);

            yield return Arbiter.Choice(
                subscribe.ResponsePort,
                delegate(SubscribeResponseType success)
                { s = success; },
                delegate(Fault f)
                {
                    fault = f;
                }
            );

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to image capture", fault);
//                Console.WriteLine("Subscription to image capture failed");
                yield break;
            }
            else
            {
//                LogInfo("Subscription to image capture success");
//                Console.WriteLine("Subscription to image capture success");
            }

            yield break;

        }

        public void ImageMorphologyHandler(NotifyImageMorphology update)
        {
//            LogInfo("Conv Done");
            update.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, update);
            if (_state.isLast == true)
            {
                _dispPort.DispImage(update.Body.MorphedImage);
            }
            else
            {
                _imgCaptPort.NotifyImageProcessing(update.Body.MorphedImage, _state.orderNumber);
            }
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Morph Image Handler
        /// </summary>
        /// <param name="request">MorphImageRequest</param>
        /// <returns></returns>
        public void MorphImageHandler(MorphImage request)
        {
            if (_state == null)
                request.ResponsePort.Post(new Fault());
            else
            {
                request.ResponsePort.Post(DefaultUpdateResponseType.Instance);
                Image<Bgr, byte> img = new Image<Bgr, byte>(request.Body.ImageForMorphology);
                StructuringElementEx structEl = null;

                switch (_state.StrucElemShape)
                {
                    case 1:
                        structEl = new StructuringElementEx(_state.StrucElemCols, _state.StrucElemRows, _state.StrucElemAnchorX, _state.StrucElemAnchorY, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_CROSS);
                        break;

                    case 2:
                        structEl = new StructuringElementEx(_state.StrucElemCols, _state.StrucElemRows, _state.StrucElemAnchorX, _state.StrucElemAnchorY, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);
                        break;

                    case 3:
                        structEl = new StructuringElementEx(_state.StrucElemCols, _state.StrucElemRows, _state.StrucElemAnchorX, _state.StrucElemAnchorY, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_RECT);
                        break;
                }

                switch (_state.MorphType)
                {
                    case 1:
                        img._MorphologyEx(structEl, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_BLACKHAT, _state.Iterations);
                        break;

                    case 2:
                        img._MorphologyEx(structEl, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_CLOSE, _state.Iterations);
                        break;

                    case 3:
                        img._MorphologyEx(structEl, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_GRADIENT, _state.Iterations);
                        break;

                    case 4:
                        img._MorphologyEx(structEl, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_OPEN, _state.Iterations);
                        break;

                    case 5:
                        img._MorphologyEx(structEl, Emgu.CV.CvEnum.CV_MORPH_OP.CV_MOP_TOPHAT, _state.Iterations);
                        break;
                }

                _internalPort.Post(new NotifyImageMorphology(new MorphologyResult(img.Data)));

                /* ImageViewer iv = new ImageViewer();
                iv.Image = img;
                iv.ShowDialog(); */
            }
        }
    }

}


