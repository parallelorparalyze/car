using System.Reflection;
using dss = Microsoft.Dss.Core.Attributes;
using interop = System.Runtime.InteropServices;

[assembly: dss.ServiceDeclaration(dss.DssServiceDeclaration.ServiceBehavior)]
[assembly: interop.ComVisible(false)]
[assembly: AssemblyTitle("ImgMorphology")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("ImgMorphology")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("1.0.0.0")]



