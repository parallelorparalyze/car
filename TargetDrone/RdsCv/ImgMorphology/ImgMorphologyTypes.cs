using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using Robotics.ImgMorphology;

namespace Robotics.ImgMorphology
{
    /// <summary>
    /// ImgMorphology contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgMorphology
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imgmorphology.html";
    }

    /// <summary>
    /// ImgMorphology state
    /// </summary>
    [DataContract]
    public class ImgMorphologyState
    {
        /// <summary>
        /// Morphology Operation
        /// </summary>
        [DataMember]
        [Description("Morphology operations : 1 -> Blackhat, 2 -> Close, 3 -> Gradient, 4 -> Open, 5 -> Tophat (default Blackhat)")]
        public int MorphType;

        /// <summary>
        /// Iterations
        /// </summary>
        [DataMember]
        [Description("Iterations for morphology")]
        public int Iterations;

        /// <summary>
        /// Number of columns for structuring element
        /// </summary>
        [DataMember]
        [Description("Number of columns for structuring element")]
        public int StrucElemCols;

        /// <summary>
        /// Number of rows for structuring element
        /// </summary>
        [DataMember]
        [Description("Number of rows for structuring element")]
        public int StrucElemRows;

        /// <summary>
        /// Relative horizontal offset for structuring element anchor point
        /// </summary>
        [DataMember]
        [Description("Relative horizontal offset for structuring element anchor point")]
        public int StrucElemAnchorX;

        /// <summary>
        /// Relative vertical offset for structuring element anchor point
        /// </summary>
        [DataMember]
        [Description("Relative vertical offset for structuring element anchor point")]
        public int StrucElemAnchorY;

        /// <summary>
        /// Shape of structuring element : 1 -> Cross, 2 -> Ellipse, 3 -> Rectangle (default Rectangle)
        /// </summary>
        [DataMember]
        [Description("Shape of structuring element : 1 -> Cross, 2 -> Ellipse, 3 -> Rectangle (default Rectangle)")]
        public int StrucElemShape;

        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImgMorphology main operations port
    /// </summary>
    [ServicePort]
    public class ImgMorphologyOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, MorphImage, NotifyImageMorphology>
    {
    }

    /// <summary>
    /// ImgMorphology get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgMorphologyState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgMorphologyState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image Morphology Operation
    /// </summary>
    public class MorphImage : Update<MorphImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public MorphImage()
        {
        }

        /// <summary>
        /// Constructor with MorphImageRequest
        /// </summary>
        public MorphImage(MorphImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Morph Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class MorphImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForMorphology;

        /// <summary>
        /// Constructor
        /// </summary>
        public MorphImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public MorphImageRequest(byte[, ,] request)
        {
            ImageForMorphology = request;
        }
    }

    /// <summary>
    /// Image Morphology Notification Operation
    /// </summary>
    [DisplayName("ImageMorphed")]
    [Description("Indicates that image has been morphed.")]
    public class NotifyImageMorphology : Update<MorphologyResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImageMorphology()
        {
        }

        /// <summary>
        /// Constructor with MorphologyResult
        /// </summary>
        public NotifyImageMorphology(MorphologyResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image Morphology Result
    /// </summary>
    [DataContract]
    public class MorphologyResult
    {
        /// <summary>
        /// Morphology Result
        /// </summary>
        [DataMember]
        [Description("The resulting morphed image data")]
        public byte[,,] MorphedImage;

        /// <summary>
        /// Constructor
        /// </summary>
        public MorphologyResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public MorphologyResult(byte[,,] result)
        {
            MorphedImage = result;
        }
    }

    /// <summary>
    /// ImgMorphology subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


