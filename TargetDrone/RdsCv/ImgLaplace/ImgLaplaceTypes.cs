using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Robotics.ImgLaplace;
using System.Drawing;

namespace Robotics.ImgLaplace
{
    /// <summary>
    /// ImgLaplace contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgLaplace
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imglaplace.html";
    }

    /// <summary>
    /// ImgLaplace state
    /// </summary>
    [DataContract]
    public class ImgLaplaceState
    {
        /// <summary>
        /// Aperture Size (1,3,5,7 only) default 3.
        /// </summary>
        [DataMember]
        [Description("Aperture Size (1,3,5,7 only) default 3")]
        public int Aperture;

        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImgLaplace main operations port
    /// </summary>
    [ServicePort]
    public class ImgLaplaceOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, LaplaceImage, NotifyImageLaplace>
    {
    }

    /// <summary>
    /// ImgLaplace get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgLaplaceState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgLaplaceState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image Laplace Operation
    /// </summary>
    public class LaplaceImage : Update<LaplaceImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public LaplaceImage()
        {
        }

        /// <summary>
        /// Constructor with LaplaceImageRequest
        /// </summary>
        public LaplaceImage(LaplaceImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Laplace Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class LaplaceImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForLaplace;

        /// <summary>
        /// Constructor
        /// </summary>
        public LaplaceImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public LaplaceImageRequest(byte[,,] request)
        {
            ImageForLaplace = request;
        }
    }

    /// <summary>
    /// Image Laplace Notification Operation
    /// </summary>
    [DisplayName("ImageLaplace done.")]
    [Description("Indicates that image Laplace is done.")]
    public class NotifyImageLaplace : Update<LaplaceResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImageLaplace()
        {
        }

        /// <summary>
        /// Constructor with LaplaceResult
        /// </summary>
        public NotifyImageLaplace(LaplaceResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image Laplace Result
    /// </summary>
    [DataContract]
    public class LaplaceResult
    {
        /// <summary>
        /// Laplace Result
        /// </summary>
        [DataMember]
        [Description("The resulting Laplace image data")]
        public byte[,,] LaplaceResImage;

        /// <summary>
        /// Constructor
        /// </summary>
        public LaplaceResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public LaplaceResult(byte[,,] result)
        {
            LaplaceResImage = result;
        }
    }

    /// <summary>
    /// ImgLaplace subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


