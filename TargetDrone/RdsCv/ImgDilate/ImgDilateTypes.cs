using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Drawing;
using System.Windows.Forms;
using Robotics.ImgDilate;

namespace Robotics.ImgDilate
{
    /// <summary>
    /// ImgDilate contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for ImgDilate
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.rdscv.codeplex.com/2010/08/imgdilate.html";
    }

    /// <summary>
    /// ImgDilate state
    /// </summary>
    [DataContract]
    public class ImgDilateState
    {
        /// <summary>
        /// Dilation Iterations
        /// </summary>
        [DataMember]
        [Description("Specifies the number of dilation iterations.")]
        public int Iterations;

        /// <summary>
        /// Service order number
        /// </summary>
        [DataMember]
        public int orderNumber;

        /// <summary>
        /// Check if last
        /// </summary>
        [DataMember]
        public bool isLast;
    }

    /// <summary>
    /// ImgDilate main operations port
    /// </summary>
    [ServicePort]
    public class ImgDilateOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe, DilateImage, NotifyImageDilation>
    {
    }

    /// <summary>
    /// ImgDilate get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<ImgDilateState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<ImgDilateState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// Image Dilation operation
    /// </summary>
    public class DilateImage : Update<DilateImageRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public DilateImage()
        {
        }

        /// <summary>
        /// Constructor with DilateImageRequest
        /// </summary>
        public DilateImage(DilateImageRequest body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Dilate Image Request Type
    /// </summary>
    [DataContract]
    [DataMemberConstructor]
    public class DilateImageRequest
    {
        [DataMember, DataMemberConstructor]
        [Description("Data for Input Image")]
        public byte[,,] ImageForDilation;

         /// <summary>
        /// Constructor
        /// </summary>
        public DilateImageRequest()
        {
        }
        /// <summary>
        /// Constructor with Image Data Request (byte[,,])
        /// </summary>
        public DilateImageRequest(byte[,,] request)
        {
            ImageForDilation = request;
        }
    }

    /// <summary>
    /// Image Dilation Notification Operation
    /// </summary>
    [DisplayName("ImageDilated")]
    [Description("Indicates that image has been Dilated.")]
    public class NotifyImageDilation : Update<DilationResult, PortSet<DefaultUpdateResponseType, Fault>>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NotifyImageDilation()
        {
        }

        /// <summary>
        /// Constructor with DilationResult
        /// </summary>
        public NotifyImageDilation(DilationResult body)
            : base(body)
        {
        }
    }

    /// <summary>
    /// Image Dilation Result
    /// </summary>
    [DataContract]
    public class DilationResult
    {
        /// <summary>
        /// Dilation Result
        /// </summary>
        [DataMember]
        [Description("The resulting Dilated image")]
        public byte[,,] ImageDilated;

        /// <summary>
        /// Constructor
        /// </summary>
        public DilationResult()
        {
        }
        /// <summary>
        /// Constructor with Image Data Result (byte[,,])
        /// </summary>
        public DilationResult(byte[,,] result)
        {
            ImageDilated = result;
        }
    }

    /// <summary>
    /// ImgDilate subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}


