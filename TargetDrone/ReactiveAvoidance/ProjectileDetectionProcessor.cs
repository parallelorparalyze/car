// // -----------------------------------------------------------------------
// // Team Purple Threads
// // CSE Capstone
// // Arizona State University
// // Copyright (C) 2012  All rights reserved.
// // Contains source whole or in part from Microsoft Robotics Developer Studio
// // Copyright (C) Microsoft Corporation.  All rights reserved.
// // -----------------------------------------------------------------------

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Microsoft.Ccr.Core;
using common = Microsoft.Robotics.Common;
using depthcamsensor = Microsoft.Robotics.Services.DepthCamSensor;
using pm = Microsoft.Robotics.PhysicalModel;
using mskinect = Microsoft.Kinect;


namespace ArizonaStateUniversity.Robotics.Services.ReactiveAvoidanceDrive
{
    /// <summary>
    /// </summary>
    public class ProjectileDetectionProcessor
    {
        private const int BlueIndex = 0;
        private const int GreenIndex = 1;
        private const int RedIndex = 2;

        private readonly Port<ProjectileTracking> _projectileTrackingPort;

        private GetDistanceDelegate getDistanceBehaviour = delegate { return 0; };

        public ProjectileDetectionProcessor(Port<ProjectileTracking> projectileTrackingPort)
        {
            _projectileTrackingPort = projectileTrackingPort;
        }

        public Image<Bgr, Byte> KinectImage { get; set; }

        public Image<Gray, Byte> ColorMask { get; set; }

        private static Bitmap MakeBitmap(int width, int height, byte[] imageData)
        {
            // NOTE: This code implicitly assumes that the width is a multiple
            // of four bytes because Bitmaps have to be longword aligned.
            // We really should look at bmp.Stride to see if there is any padding.
            // However, the width and height come from the webcam and most cameras
            // have resolutions that are multiples of four.

            var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);

            BitmapData data = bmp.LockBits(
                new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.WriteOnly,
                PixelFormat.Format24bppRgb
                );

            Marshal.Copy(imageData, 0, data.Scan0, imageData.Length);

            bmp.UnlockBits(data);

            return bmp;
        }

        public void DetectProjectile(ProjectileDetection projectileDetection)
        {
            int colorImageX;
            int colorImageY;
            if (projectileDetection.ColorImageFormat == mskinect.ColorImageFormat.RgbResolution640x480Fps30)
            {
                colorImageX = 640;
                colorImageY = 480;
            }
            else
            {
                colorImageX = 320;
                colorImageY = 240;
            }

            // get depth frame
            short[] depthImageData = projectileDetection.DepthCamSensorState.DepthImage;

            // get bgr frame
            byte[] bgrImageData = projectileDetection.DepthCamSensorState.VisibleImage;

            Size size = projectileDetection.DepthCamSensorState.DepthImageSize;
            pm.Matrix inverseProjectionMatrix = projectileDetection.DepthCamSensorState.InverseProjectionMatrix;

            // convert bgr frame to bitmap
            Bitmap bgrBmp = MakeBitmap(colorImageX, colorImageY, bgrImageData);

            // convert bitmap to bgr byte frame
            var bgrframe = new Image<Bgr, byte>(bgrBmp);

            // extract the red channel
            Image<Gray, Byte>[] origchannels = bgrframe.Split(); // split bgrframe into components
            Image<Gray, Byte> redframe = origchannels[2]; // hsv, so channels[2] is red

            // filter out all but the red you want
            Image<Gray, byte> redfilter = redframe.ThresholdBinary(new Gray(128), new Gray(220));

            // convert the image to hsv
            Image<Hsv, Byte> hsvframe = bgrframe.Convert<Hsv, Byte>();

            // extract the hue, saturation and value channels
            Image<Gray, Byte>[] channels = hsvframe.Split(); // split hsvframe into components
            Image<Gray, Byte> framesat = channels[1]; // hsv, so channels[1] is saturation.

            // filter out all but the saturation you want
            Image<Gray, byte> satfilter = framesat.ThresholdBinary(new Gray(128), new Gray(255));

            // MUL the two filters to get the parts of the image that are colored and above some brightness
            Image<Gray, byte> colormask = redfilter.Mul(satfilter);

            // mask filters
            colormask._Erode(1);
            colormask._Dilate(5);

            #region CVMoments

            var moment = new MCvMoments();
            CvInvoke.cvMoments(colormask.Ptr, ref moment, 1);

            double moment10 = CvInvoke.cvGetSpatialMoment(ref moment, 1, 0);
            double moment01 = CvInvoke.cvGetSpatialMoment(ref moment, 0, 1);
            double area = CvInvoke.cvGetCentralMoment(ref moment, 0, 0);

            #endregion

            #region GetCoordinates

            if (area != 0)
            {
                var posX = 0;
                var posY = 0;
                var lastX = posX;
                var lastY = posY;
                const LINE_TYPE line = LINE_TYPE.EIGHT_CONNECTED;

                //Dividing moment10 by area gives x-coord
                posX = (int) (moment10/area);

                //Dividing moment01 by area gives y-coord
                posY = (int) (moment01/area);

                var point = new Point(posX, posY);
                var depthpoint = new Point(posX * size.Width / colorImageX, posY * size.Height / colorImageY);

                // get depth at point (x,y)
                float depth = depthImageData[(size.Width * depthpoint.Y) + depthpoint.X];

                var blue = new MCvScalar(255, 0, 0);
                var green = new MCvScalar(0, 255, 0);
                var red = new MCvScalar(0, 0, 255);

                CvInvoke.cvCircle(bgrframe.Ptr, point, 4, green, -1, line, 4);
                CvInvoke.cvCircle(colormask.Ptr, point, 4, green, -1, line, 4);

                pm.Vector3 locationInSpace = common.MathUtilities.ConvertPixelSpaceToViewSpaceInMillimeters(posX, posY,
                                                                                                  (int) depth,
                                                                                                  inverseProjectionMatrix,
                                                                                                  size.Width,
                                                                                                  size.Height);

                var projectileTracking = new ProjectileTracking
                                             {
                                                 ProjectilePosition =
                                                     new pm.Vector4(locationInSpace.X, locationInSpace.Y, locationInSpace.Z,
                                                                 1.0f),
                                                 Timestamp = projectileDetection.DepthCamSensorState.TimeStamp
                                             };

                _projectileTrackingPort.Post(projectileTracking);
            }

            #endregion

            KinectImage = bgrframe;
            ColorMask = colormask;
        }

        #region Nested type: GetDistanceDelegate

        /// <summary>
        ///   A delegate to be initialized with the right kind of Distance calculation behavior
        /// </summary>
        /// <returns> distance in mm </returns>
        private delegate int GetDistanceDelegate(byte firstFrame, byte secondFrame);

        #endregion
    }
}