// -----------------------------------------------------------------------
// Team Purple Threads
// CSE Capstone
// Arizona State University
// 2012
// Contains source whole or in part from Microsoft Robotics Developer Studio
// <copyright file="ProjectileDetectionServiceUI.xaml.cs" company="Microsoft Corporation">
// Copyright (C) Microsoft Corporation.  All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Microsoft.Kinect;
using Microsoft.Robotics.Common;
using Microsoft.Robotics.Services.Sensors.Kinect.Proxy;
using W3C.Soap;

namespace ArizonaStateUniversity.Robotics.Services.ReactiveAvoidanceDrive
{
    /// <summary>
    /// Main UI window - responsible for showing stuff, accepting user input and communicating with ProjectileDetectionService when there is need to talk to Kinect service
    /// </summary>
    public partial class ProjectileDetectionServiceUI : Window
    {
        // Using a DependencyProperty as the backing store.  This enables animation, styling, binding, etc...

        /// <summary>
        /// VisemeProperty
        /// </summary>
        public static readonly DependencyProperty VisemeProperty =
            DependencyProperty.Register("ProjectileDetectionServiceUIString", typeof(int),
                                        typeof(ProjectileDetectionServiceUI));

        /// <summary>
        /// We need this when pre-processing skeletal frame to calculate point positions relative to the depth frame
        /// </summary>
        public static double DisplayWIndowWidth;

        /// <summary>
        /// We need this when pre-processing skeletal frame to calculate point positions relative to the depth frame
        /// </summary>
        public static double DisplayWIndowHeight;

        /// <summary>
        /// Refference to DSS service for sending operations to the Kinect Service
        /// </summary>
        private readonly ReactiveAvoidanceDriveService reactiveAvoidanceDriveService;

        /// <summary>
        /// Used to integrate recent frame readings to come up with a reasonable running FPS average
        /// </summary>
        private readonly List<double> recentFrameRateContainer = new List<double>();

        /// <summary>
        /// Used to claculate running FPS
        /// </summary>
        private double lastFrameUpdatedTime;

        private double runningFPS;

        /// <summary>
        /// Creates a new instance of the user interface
        /// </summary>
        public ProjectileDetectionServiceUI()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Creates a new instance of the user interface
        /// </summary>
        /// <param name="service">the service that handles communication with the Kinect service</param>
        internal ProjectileDetectionServiceUI(ReactiveAvoidanceDriveService service)
            : this()
        {
            reactiveAvoidanceDriveService = service;

            DisplayWIndowHeight = ProjectileDetectionCanvas.Height;
            DisplayWIndowWidth = ProjectileDetectionCanvas.Width;
            Closed += ProjectileDetectionServiceUI_Closed;
            CalculateEffectiveFrameRate();
        }

        /// <summary>
        /// Delete a GDI object
        /// </summary>
        /// <param name="o">The poniter to the GDI object to be deleted</param>
        /// <returns></returns>
        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProjectileDetectionServiceUI_Closed(object sender, EventArgs e)
        {
            MessageBox.Show(
                this,
                Properties.Resources.NodeIsNotClosed,
                Title,
                MessageBoxButton.OK,
                MessageBoxImage.Information);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectileDetectionProcessor"></param>
        public void DrawFrame(ProjectileDetectionProcessor projectileDetectionProcessor)
        {
            RegisterFrameRead();

            DrawRGBImage(projectileDetectionProcessor);

            RgbImage.Opacity = 1.0;


            DrawColorMask(projectileDetectionProcessor);

            ColorMaskImage.Opacity = 1.0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectileDetectionProcessor"></param>
        private void DrawRGBImage(ProjectileDetectionProcessor projectileDetectionProcessor)
        {
            if (null == projectileDetectionProcessor.KinectImage)
            {
                return;
            }
            BitmapSource bitmapSource = ToBitmapSource(projectileDetectionProcessor.KinectImage);

            RgbImage.Source = bitmapSource;
        }

       
        ///// <summary>
        ///// A sergant method for drawing all joints and bones
        ///// </summary>
        ///// <param name="projectileDetectionProcessor">Pre processed frame, incl skeletal data</param>
        private void DrawColorMask(ProjectileDetectionProcessor projectileDetectionProcessor)
        {
            if (null == projectileDetectionProcessor.ColorMask)
            {
                return;
            }

            BitmapSource bitmapSource = ToBitmapSource(projectileDetectionProcessor.ColorMask);

            ColorMaskImage.Source = bitmapSource;
        }

        /// <summary>
        /// Convert an IImage to a WPF BitmapSource. The result can be used in the Set Property of Image.Source
        /// </summary>
        /// <param name="image">The Emgu CV Image</param>
        /// <returns>The equivalent BitmapSource</returns>
        public static BitmapSource ToBitmapSource(IImage image)
        {
            using (Bitmap source = image.Bitmap)
            {
                IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap

                BitmapSource bs = Imaging.CreateBitmapSourceFromHBitmap(
                    ptr,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());

                DeleteObject(ptr); //release the HBitmap
                return bs;
            }
        }

        /// <summary>
        /// Used to calculate FPS
        /// </summary>
        private void RegisterFrameRead()
        {
            recentFrameRateContainer.Add(Utilities.ElapsedSecondsSinceStart - lastFrameUpdatedTime);
            lastFrameUpdatedTime = Utilities.ElapsedSecondsSinceStart;

            // we integrate over last 30 frames - this gives us a reasonably accurate and smooth 'running FPS' figure 
            if (recentFrameRateContainer.Count >= 30)
            {
                recentFrameRateContainer.RemoveAt(0);
                CalculateEffectiveFrameRate();
            }
        }

        /// <summary>
        /// Calc FPS
        /// </summary>
        private void CalculateEffectiveFrameRate()
        {
            double combinedDelayAcrossRecentFrames = recentFrameRateContainer.Sum();

            runningFPS = combinedDelayAcrossRecentFrames / recentFrameRateContainer.Count;

            FPS.Text = string.Format("Effective FPS: {0:##}", 1 / runningFPS);
        }

        /// <summary>
        /// Displays a fault message
        /// </summary>
        /// <param name="fault">Error</param>
        internal void ShowFault(Fault fault)
        {
            var error = Properties.Resources.ErrorMessage;

            if (fault.Reason != null && fault.Reason.Length > 0 && !string.IsNullOrEmpty(fault.Reason[0].Value))
            {
                error = fault.Reason[0].Value;
            }

            MessageBox.Show(
                this,
                error,
                Title,
                MessageBoxButton.OK,
                MessageBoxImage.Error);
        }

    }
}