﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArizonaStateUniversity.Robotics.Services.ReactiveAvoidanceDrive
{
    public class ProjectileTrackingComparer : Comparer<ProjectileTracking>
    {
        public override int Compare(ProjectileTracking x, ProjectileTracking y)
        {
            if (x == null)
            {
                return y == null ? 0 : -1;
            }
            else
            {
                return y == null ? 1 : DateTime.Compare(x.Timestamp, y.Timestamp);
            }
        }
    }
}
