//------------------------------------------------------------------------------
//  <copyright file="ReactiveAvoidanceDriveTypes.cs" company="Microsoft Corporation">
//      Copyright (C) Microsoft Corporation.  All rights reserved.
//  </copyright>
//------------------------------------------------------------------------------

using System.Collections.Generic;

namespace ArizonaStateUniversity.Robotics.Services.ReactiveAvoidanceDrive
{
    using System;
    using Emgu.CV;
    using Microsoft.Ccr.Core;
    using Microsoft.Dss.Core.Attributes;
    using Microsoft.Dss.Core.DsspHttp;
    using Microsoft.Dss.ServiceModel.Dssp;
    using Microsoft.Kinect;
    using Microsoft.Robotics.PhysicalModel;
    using Microsoft.Robotics.Services.DepthCamSensor;
    using W3C.Soap;

    /// <summary>
    /// Contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// Contract identifier
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.asu.edu/2012/02/reactiveavoidancedrive.html";
    }

    /// <summary>
    /// Operations port
    /// </summary>
    public class ReactiveAvoidanceDriveOperationsPort : PortSet<Get, Subscribe, HttpQuery, HttpGet, DsspDefaultGet, DsspDefaultLookup, DsspDefaultDrop, NotifyProjectileTracked>
    {
    }

    /// <summary>
    /// Simple PID controller state and behavior
    /// </summary>     
    [DataContract]
    public class PIDController
    {
        /// <summary>
        /// Default proportional constant
        /// </summary>
        public const double ProportionalGainDefault = 0.25;

        /// <summary>
        /// Default derivative constant
        /// </summary>
        public const double DerivativeGainDefault = 0.05;

        /// <summary>
        /// Default integral constant
        /// </summary>
        public const double IntegralGainDefault = 0.02;

        /// <summary>
        /// Integral constant
        /// </summary>
        [DataMember]
        public double Ki = IntegralGainDefault;

        /// <summary>
        ///  Proportional constant
        /// </summary>
        [DataMember]
        public double Kp = ProportionalGainDefault;

        /// <summary>
        ///  Derivative constant
        /// </summary>
        [DataMember]
        public double Kd = DerivativeGainDefault;

        /// <summary>
        /// Previous error
        /// </summary>
        public double PreviousError;

        /// <summary>
        /// Most recent error
        /// </summary>
        public double CurrentError;

        /// <summary>
        /// Derivative error
        /// </summary>
        public double DerivativeError;

        /// <summary>
        /// Accumulated error
        /// </summary>
        public double IntegralError;

        /// <summary>
        /// Maximum integral error
        /// </summary>
        private const double MaxIntegralError = 2;

        /// <summary>
        /// Update the controller state
        /// </summary>
        /// <param name="newError">The new error value</param>
        /// <param name="updateInterval">Time since last update</param>
        public void Update(double newError, double updateInterval)
        {
            this.PreviousError = this.CurrentError;
            this.CurrentError = newError;
            if (updateInterval > 1)
            {
                // it has taken too long between updates, reset
                this.IntegralError = 0;
            }

            if (updateInterval > 0)
            {
                this.DerivativeError = (this.CurrentError - this.PreviousError) / updateInterval;
                this.IntegralError += this.DerivativeError;
                if (this.IntegralError >= MaxIntegralError ||
                    this.IntegralError <= -MaxIntegralError)
                {
                    this.IntegralError = 0;
                }
            }
        }

        /// <summary>
        /// Calculate control. It does not produce a linear speed
        /// </summary>
        /// <param name="angularSpeed">Calculated angular speed</param>
        /// <param name="speed">Calculated linear speed (not used)</param>
        public void CalculateControl(out double angularSpeed, out double speed)
        {
            angularSpeed = 0;
            angularSpeed =
                (this.CurrentError * this.Kp) +
                (this.IntegralError * this.Ki) +
                (this.DerivativeError * this.Kd);

            speed = 0;

            if (Math.Abs(angularSpeed) > 1)
            {
                angularSpeed = 1 * Math.Sign(angularSpeed);
            }
        }

        /// <summary>
        /// Reset state
        /// </summary>
        public void Reset()
        {
            this.PreviousError = this.CurrentError = this.IntegralError = 0;
        }
    }

    /// <summary>
    /// Service state
    /// </summary>
    [DataContract]
    public class ReactiveAvoidanceDriveState
    {
        
        /// <summary>
        /// Gets or sets robot width in meters
        /// </summary>
        [DataMember]
        public double RobotWidth { get; set; }

        /// <summary>
        /// Gets or sets max power allowed per wheel
        /// </summary>
        [DataMember]
        public double MaxPowerPerWheel { get; set; }

        /// <summary>
        /// Gets or sets the minimum rotation speed
        /// </summary>
        [DataMember]
        public double MinRotationSpeed { get; set; }

        /// <summary>
        /// Gets or sets the depth camera position relative to the floor plane 
        /// and the projection of the center of mass of the robot to the floor plane
        /// </summary>
        [DataMember]
        public Vector4 DepthCameraPosition { get; set; }

        /// <summary>
        /// Gets or sets the reactive avoidance controller state
        /// </summary>
        [DataMember]
        public PIDController Controller { get; set; }

        /// <summary>
        /// Gets or sets the maximum allowed change in Power from one call to SetPower to the next
        /// Smaller numbers will cause smoother accelerations, but can also increase chance of collision with 
        /// obstacles
        /// </summary>
        [DataMember]
        public double MaxDeltaPower { get; set; }

        [DataMember]
        public List<ProjectileTracking> ProjectileTracking { get; set; }

        private double _samplingIntervalInSeconds = 0.075;
        
        [DataMember]
        public double SamplingIntervalInSeconds
        {
            get { return _samplingIntervalInSeconds; }
            set { _samplingIntervalInSeconds = value; }
        }

    }

    [DataContract]
    public class NotifyProjectileTrackedRequest
    {
        [DataMember]
        public ProjectileTracking ProjectileTracking { get; set; }
    }

    [DataContract]
    public class ProjectileTracking
    {
        [DataMember]
        public Vector4 ProjectilePosition { get; set; }

        [DataMember]
        public DateTime Timestamp { get; set; }
    }

    [DataContract]
    public class ProjectileDetection
    {
        [DataMember]
        public DepthCamSensorState DepthCamSensorState { get; set; }

        [DataMember]
        public short[] PreprocessedDepthImage;

        [DataMember]
        public ColorImageFormat ColorImageFormat { get; set; }
    }

    /// <summary>
    /// Get operation
    /// </summary>
    public class Get : Get<GetRequestType, DsspResponsePort<ReactiveAvoidanceDriveState>>
    {
    }

    /// <summary>
    /// ProjectileDetectionService subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    public class NotifyProjectileTracked : Update<NotifyProjectileTrackedRequest, PortSet<DefaultUpdateResponseType, Fault>>
    {
        public NotifyProjectileTracked()
            : base(new NotifyProjectileTrackedRequest())
        {
        }
    }

    /// <summary>
    /// Partner names
    /// </summary>
    [DataContract]
    public class Partners
    {
        /// <summary>
        /// Drive service
        /// </summary>
        [DataMember]
        public const string Drive = "Drive";

        /// <summary>
        /// Depth cam service
        /// </summary>
        [DataMember]
        public const string DepthCamSensor = "DepthCamera";

        /// <summary>
        /// Kinect service
        /// </summary>
        [DataMember]
        public const string Kinect = "KinectService";

        /// <summary>
        /// IR sensor array
        /// </summary>
        [DataMember]
        public const string InfraredSensorArray = "InfraredSensorArray";

        /// <summary>
        /// Sonar analog sensors
        /// </summary>
        [DataMember]
        public const string SonarSensorArray = "SonarSensorArray";

        /// <summary>
        /// Time we are willing to wait for each partner to start
        /// </summary>
        [DataMember]
        public const int PartnerEnumerationTimeoutInSeconds = 120;
    }
}
