// -----------------------------------------------------------------------
// Team Purple Threads
// CSE Capstone
// Arizona State University
// 2012
// Contains source whole or in part from Microsoft Robotics Developer Studio
// <copyright file="MavLinkTypes.cs" company="Microsoft Corporation">
// Copyright (C) Microsoft Corporation.  All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using W3C.Soap;

namespace ArizonaStateUniversity.Robotics.Services.MavLink
{
    public sealed class Contract
    {
        [DataMember] public const string Identifier = "http://schemas.asu.edu/2012/02/mavlink.html";
    }

    [DataContract]
    public class MavLinkState
    {
    }

    [ServicePort]
    public class MavLinkOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get>
    {
    }

    public class Get : Get<GetRequestType, PortSet<MavLinkState, Fault>>
    {
        public Get()
        {
        }

        public Get(GetRequestType body)
            : base(body)
        {
        }

        public Get(GetRequestType body, PortSet<MavLinkState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}