// -----------------------------------------------------------------------
// Team Purple Threads
// CSE Capstone
// Arizona State University
// 2012
// Contains source whole or in part from Microsoft Robotics Developer Studio
// <copyright file="MavLink.cs" company="Microsoft Corporation">
// Copyright (C) Microsoft Corporation.  All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;

namespace ArizonaStateUniversity.Robotics.Services.MavLink
{
    [Contract(Contract.Identifier)]
    [DisplayName("MavLink")]
    [Description("MavLink service (no description provided)")]
    internal class MavLinkService : DsspServiceBase
    {
        [ServicePort("/MavLink", AllowMultipleInstances = true)] private MavLinkOperations _mainPort =
            new MavLinkOperations();

        [ServiceState] private MavLinkState _state = new MavLinkState();

        public MavLinkService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        protected override void Start()
        {
            // 
            // Add service specific initialization here
            // 

            base.Start();
        }
    }
}