// -----------------------------------------------------------------------
// Team Purple Threads
// CSE Capstone
// Arizona State University
// 2012
// Contains source whole or in part from Microsoft Robotics Developer Studio
// <copyright file="KinectCapt.cs" company="Microsoft Corporation">
// Copyright (C) Microsoft Corporation.  All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Emgu.CV;
using Emgu.CV.Structure;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using Microsoft.Dss.Services.SubscriptionManager;
using Microsoft.Robotics.Common;
using Microsoft.Robotics.Services.DepthCamSensor.Proxy;
using Microsoft.Robotics.Services.Sensors.Kinect;
using Microsoft.Robotics.Services.WebCamSensor.Proxy;
using Robotics.ImageCapt;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using depthcamsensorProxy = Microsoft.Robotics.Services.DepthCamSensor.Proxy;
using webcamsensor = Microsoft.Robotics.Services.WebCamSensor.Proxy;
using imagecapt = Robotics.ImageCapt;
using common = Microsoft.Robotics.Common;

//using kinect = Microsoft.Robotics.Services.Sensors.Kinect;
//using kinectProxy = Microsoft.Robotics.Services.Sensors.Kinect.Proxy;

namespace KinectCapt
{
    [Contract(Contract.Identifier)]
    [DisplayName("KinectCapt")]
    [Description("KinectCapt service (no description provided)")]
    [AlternateContract(Robotics.ImageCapt.Contract.Identifier)]
    internal class KinectCaptService : DsspServiceBase
    {
        /// <summary>
        /// The number of milliseconds to wait before beginning to navigate
        /// </summary>
        private const int InitialWaitMilliseconds = 5000;

        private readonly DepthCamSensorOperationsPort _depthCamSensorNotify = new DepthCamSensorOperationsPort();

        /// <summary>
        /// DepthCamSensor partner
        /// </summary>
        [Partner("DepthCamSensor", Contract = Microsoft.Robotics.Services.DepthCamSensor.Proxy.Contract.Identifier,
            CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)] private readonly DepthCamSensorOperationsPort
            _depthCamSensorPort = new DepthCamSensorOperationsPort();

        private readonly Port<Shutdown> _depthCamShutdown = new Port<Shutdown>();

        /// <summary>
        /// Internal port to update private state.
        /// </summary>
        private readonly ImageCaptOperations _internalPort = new ImageCaptOperations();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/KinectCapt", AllowMultipleInstances = true)] private readonly ImageCaptOperations _mainPort =
            new ImageCaptOperations();

        [SubscriptionManagerPartner] private readonly SubscriptionManagerPort _submgrPort =
            new SubscriptionManagerPort();

        /// <summary>
        /// Used to guage frequency of reading the state (which is much lower than that of reading frames)
        /// </summary>
        private double _lastStateReadTime;

        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState] [InitialStatePartner(Optional = true, ServiceUri = "KinectCapt.Config.xml")] private
            ImageCaptState _state = new ImageCaptState();

        private int _subscribedCtr;

        private WebCamSensorOperations _webCamSensorNotify = new WebCamSensorOperations();

        /// <summary>
        /// WebCamSensor partner
        /// </summary>
        [Partner("WebCamSensor", Contract = Microsoft.Robotics.Services.WebCamSensor.Proxy.Contract.Identifier,
            CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)] private WebCamSensorOperations
            _webCamSensorPort = new WebCamSensorOperations();

        ///// <summary>
        ///// KinectService partner
        ///// </summary>
        //[Partner("KinectService", Contract = kinectProxy.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        //kinectProxy.KinectOperations _kinectServicePort = new kinectProxy.KinectOperations();


        /// <summary>
        /// Service constructor
        /// </summary>
        public KinectCaptService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
            // by default, lets show all the good stuff
            IncludeDepth = true;
            IncludeVideo = true;
        }

        ///// <summary>
        ///// We dont want to flood logs with same errors
        ///// </summary>
        //private bool atLeastOneFrameQueryFailed = false;


        /// <summary>
        /// Those are used to set appropriate flags when querying Kinect frame
        /// </summary>
        public bool IncludeDepth { get; set; }

        public bool IncludeVideo { get; set; }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {
            // 
            // Add service specific initialization here
            // 

            base.Start();

            if (_state == null)
            {
                _state = new ImageCaptState();
            }

            if ((_state.ImgInputType < 0) || (_state.ImgInputType > 1))
            {
                _state.ImgInputType = 0;
            }

            if (_state.ImgFileName == null)
            {
                _state.ImgFileName = " ";
            }

            SaveState(_state);

            //MainPortInterleave.CombineWith(Arbiter.Interleave(new TeardownReceiverGroup(), new ExclusiveReceiverGroup(Arbiter.ReceiveWithIterator<depthcamsensorProxy.Replace>(true, _depthCamSensorNotify, DepthCameraUpdateFrameHandler)), new ConcurrentReceiverGroup()));
            MainPortInterleave.CombineWith(Arbiter.Interleave(
                new TeardownReceiverGroup
                    (
                    ),
                new ExclusiveReceiverGroup
                    (
                    Arbiter.Receive<NotifyImageCapture>(true, _internalPort, NotifyImageCaptureHandler)
                    ),
                new ConcurrentReceiverGroup
                    (
                    Arbiter.ReceiveWithIterator<Microsoft.Robotics.Services.DepthCamSensor.Proxy.Replace>(true,
                                                                                                          _depthCamSensorNotify,
                                                                                                          DepthCameraUpdateFrameHandler)
                    )
                                               ));

            SpawnIterator(ConnectDepthCamHandler);
        }

        /// <summary>
        /// Handles Get requests
        /// </summary>
        /// <param name="get">request message</param>
        [ServiceHandler]
        public void GetHandler(Robotics.ImageCapt.Get get)
        {
            // Keep track of the last time we sampled the depth image
            long timeOfPreviousSample = 0;

            long timeOfThisSample = DateTime.UtcNow.Ticks;
            long toWait = (long) _lastStateReadTime - (timeOfThisSample - timeOfPreviousSample);

            // request the depth data
            _depthCamSensorPort.Get().Choice(
                depthResult =>
                    {
                        int width = depthResult.DepthImageSize.Width;
                        int height = depthResult.DepthImageSize.Height;
                        Bitmap bmp = MakeDepthBitmap(width, height, depthResult.DepthImage);
                    },
                failure => { LogError(failure); });
        }


        /// <summary>
        /// Handles Subscribe requests
        /// </summary>
        /// <param name="subscribe">request message</param>
        [ServiceHandler]
        public void SubscribeHandler(Robotics.ImageCapt.Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
            //LogInfo("Subscribed by :" +
            //        subscribe.Body.Subscriber.Split(new[] {'/'}, StringSplitOptions.RemoveEmptyEntries)[2]);
            _subscribedCtr++;

            if (_state.NumServices == _subscribedCtr)
            {
                if (_state.ImgInputType == 0)
                {
                    SpawnIterator(ConnectDepthCamHandler);
                }
                else if (_state.ImgInputType == 1)
                {
                    SpawnIterator(ReadFromFile);
                }
            }
        }

        /// <summary>
        /// Handles NotifyImageCapture requests
        /// </summary>
        /// <param name="notifyimagecapture">request message</param>
        [ServiceHandler]
        public void NotifyImageCaptureHandler(NotifyImageCapture notifyimagecapture)
        {
            notifyimagecapture.ResponsePort.Post(DefaultUpdateResponseType.Instance);
            SendNotification(_submgrPort, notifyimagecapture);
            //Console.WriteLine("Image from camera notification");
            //LogInfo("Image from camera notification");
        }

        /// <summary>
        /// Handles NotifyImageProcessing requests
        /// </summary>
        /// <param name="notifyimageprocessing">request message</param>
        /// <summary>
        /// Handler for incoming processed image data
        /// </summary>
        [ServiceHandler(ServiceHandlerBehavior.Concurrent)]
        public virtual IEnumerator<ITask> NotifyImageProcessingHandler(NotifyImageProcessing notifyimageprocessing)
        {
            if (_state == null)
            {
                notifyimageprocessing.ResponsePort.Post(new Fault());
            }
            else
            {
                //Console.WriteLine("Processed Image Received");
                //LogInfo("Processed Image Received");
                _internalPort.Post(
                    new NotifyImageCapture(new CaptureResult(notifyimageprocessing.Body.ProcessingResImage,
                                                             notifyimageprocessing.Body.newOrder)));
            }
            yield break;
        }

        private IEnumerator<ITask> ReadFromFile()
        {
            var img = new Image<Bgr, byte>(_state.ImgFileName);
            //Bitmap bmp = img.Bitmap;

            _internalPort.Post(new NotifyImageCapture(new CaptureResult(img.Data, 0)));

            yield break;
        }

        private IEnumerator<ITask> ConnectDepthCamHandler()
        {
            Fault fault = null;
            SubscribeResponseType s;

            // Subscribe to the depthcam
            var subscribe = new Microsoft.Robotics.Services.DepthCamSensor.Proxy.Subscribe
                                {NotificationPort = _depthCamSensorNotify, NotificationShutdownPort = _depthCamShutdown};

            _depthCamSensorPort.Post(subscribe);

            yield return Arbiter.Choice(
                //_depthCamSensorPort.Subscribe(_depthCamSensorNotify)
                subscribe.ResponsePort
                , delegate(SubscribeResponseType success) { s = success; },
                delegate(Fault f) { fault = f; }
                );
            if (fault != null)
            {
                LogError(null, "Failed to subscribe to DepthCam", fault);
                yield break;
            }
            yield break;
        }


        private Choice PerformShutdown(ref Port<Shutdown> port)
        {
            var shutdown = new Shutdown();
            if (port != null)
            {
                port.Post(shutdown);
            }
            port = null;

            return Arbiter.Choice(
                shutdown.ResultPort,
                delegate { },
                LogError
                );
        }

        private Bitmap MakeBitmap(int width, int height, byte[] imageData)
        {
            // NOTE: This code implicitly assumes that the width is a multiple
            // of four bytes because Bitmaps have to be longword aligned.
            // We really should look at bmp.Stride to see if there is any padding.
            // However, the width and height come from the webcam and most cameras
            // have resolutions that are multiples of four.

            var bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            BitmapData data = bmp.LockBits(
                new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.WriteOnly,
                PixelFormat.Format24bppRgb
                );

            Marshal.Copy(imageData, 0, data.Scan0, imageData.Length);

            bmp.UnlockBits(data);

            return bmp;
        }

        private static Bitmap MakeDepthBitmap(int width, int height, IList<short> depthData)
        {
            const short MaxDepthDataValue = 4003;
            // NOTE: This code implicitly assumes that the width is a multiple // of four bytes because Bitmaps have to be longword aligned. 
            var buff = new byte[width*height*3];
            var j = 0;
            for (var i = 0; i < width*height; i++)
            {
                // Convert the data to a suitable range
                byte val;
                if (depthData[i] >= MaxDepthDataValue)
                {
                    val = byte.MaxValue;
                }
                else
                {
                    val = (byte)((depthData[i] / (double)MaxDepthDataValue) * byte.MaxValue);
                }
                // Set all R, G and B values the same, i.e. gray scale
                buff[j++] = val;
                buff[j++] = val;
                buff[j++] = val;
            }
            // NOTE: Windows Forms do not support Format16bppGrayScale which // would be the ideal way to display the data. Instead it is
            // converted to RGB with all the color values the same, i.e.
            // 8-bit gray scale.
            var bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            var data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly,
                                    PixelFormat.Format24bppRgb);
            Marshal.Copy(buff, 0, data.Scan0, buff.Length);
            bmp.UnlockBits(data);
            return bmp;
        }

        // Handler for new frames from the camera
        private IEnumerator<ITask> DepthCameraUpdateFrameHandler(
            Microsoft.Robotics.Services.DepthCamSensor.Proxy.Replace replace)
        {
            if (_state.NumServices == _subscribedCtr)
            {
                int width = replace.Body.DepthImageSize.Width;
                int height = replace.Body.DepthImageSize.Height;
                Bitmap bmp = MakeDepthBitmap(width, height, replace.Body.DepthImage);
                var img = new Image<Bgr, byte>(bmp);

                //SpawnIterator<Bitmap>(bmp, DisplayImage);
                _internalPort.Post(new NotifyImageCapture(new CaptureResult(img.Data, 0)));
                _lastStateReadTime = Utilities.ElapsedSecondsSinceStart;
            }

            yield break;
        }

        ///// <summary>
        ///// Main read loop
        ///// Read raw frame from Kinect service, then process it asynchronously, then request UI update
        ///// </summary>
        ///// <returns>A standard CCR iterator.</returns>
        //private IEnumerator<ITask> ReadKinectLoop()
        //{
        //    while (true)
        //    {
        //        var frameRequest = new kinectProxy.QueryRawFrameRequest();
        //        frameRequest.IncludeDepth = this.IncludeDepth;
        //        frameRequest.IncludeVideo = this.IncludeVideo;

        //        if (!this.IncludeDepth && !this.IncludeVideo)
        //        {
        //            // poll 5 times a sec if user for some reason deselected all image options (this would turn
        //            // into a busy loop then)
        //            yield return TimeoutPort(200).Receive();
        //        }

        //        kinect.RawKinectFrames rawFrames = null;

        //        // poll depth camera
        //        yield return this._kinectServicePort.QueryRawFrame(frameRequest).Choice(
        //            rawFrameResponse =>
        //            {
        //                rawFrames = rawFrameResponse.RawFrames;
        //            },
        //            failure =>
        //            {
        //                if (!this.atLeastOneFrameQueryFailed)
        //                {
        //                    this.atLeastOneFrameQueryFailed = true;
        //                    LogError(failure);
        //                }
        //            });
        //    }
        //}
    }
}