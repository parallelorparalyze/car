// -----------------------------------------------------------------------
// Team Purple Threads
// CSE Capstone
// Arizona State University
// 2012
// Contains source whole or in part from Microsoft Robotics Developer Studio
// <copyright file="KinectCaptTypes.cs" company="Microsoft Corporation">
// Copyright (C) Microsoft Corporation.  All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.Dss.Core.Attributes;

namespace KinectCapt
{
    /// <summary>
    /// KinectCapt contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifier for KinectCapt
        /// </summary>
        [DataMember] public const string Identifier = "http://schemas.asu.edu/2012/02/kinectcapt.html";
    }
}