Building using MSBUILD
--------------------------
Create an environment variable called MRDS_DIR pointing to your RDS installation folder.
	MRDS_DIR=DRIVE:\Users\username\Microsoft Robotics Dev Studio 4 Beta 2

Start a Visual Studio Command Prompt:
	Click Start, point to All Programs, point to Microsoft Visual Studio, point to Visual Studio Tools, and then click Visual Studio Command Prompt.

Run MSBUILD:
	msbuild DRIVE:\path\to\project\TargetDrone\dirs.proj
	
Copy configuration and manifest files to the RDS installation:
	Copy the files in DRIVE:\path\to\project\TargetDrone\config\ to DRIVE:\path\to\Microsoft Robotics Dev Studio 4\config\
	
Running the Services
--------------------------
Real Kinect Sensor:
	Open a DSS Command Prompt window and type the following command.
		bin\dsshost32 /p:51000 /t:51001 /m:"config\KinectCapt.manifest.xml"