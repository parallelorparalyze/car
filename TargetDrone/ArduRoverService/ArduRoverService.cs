// -----------------------------------------------------------------------
// Team Purple Threads
// CSE Capstone
// Arizona State University
// 2012
// Contains source whole or in part from Microsoft Robotics Developer Studio
// <copyright file="ArduRoverService.cs" company="Microsoft Corporation">
// Copyright (C) Microsoft Corporation.  All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using serialcomservice = Microsoft.Robotics.Services.SerialComService.Proxy;
using gpiopinarray = Microsoft.Robotics.Services.GpioPinArray.Proxy;
using analogsensorarray = Microsoft.Robotics.Services.AnalogSensorArray.Proxy;
using battery = Microsoft.Robotics.Services.Battery.Proxy;
using drive = Microsoft.Robotics.Services.Drive.Proxy;
using infrared = Microsoft.Robotics.Services.Infrared.Proxy;
using sonar = Microsoft.Robotics.Services.Sonar.Proxy;
using pantilt = Microsoft.Robotics.Services.PanTilt.Proxy;

namespace ArizonaStateUniversity.Robotics.Services.ArduRover
{
    [Contract(Contract.Identifier)]
    [DisplayName("ArduRoverService")]
    [Description("ArduRoverService service (no description provided)")]
    [AlternateContract(gpiopinarray.Contract.Identifier)]
    [AlternateContract(analogsensorarray.Contract.Identifier)]
    [AlternateContract(battery.Contract.Identifier)]
    [AlternateContract(drive.Contract.Identifier)]
    [AlternateContract(infrared.Contract.Identifier)]
    [AlternateContract(sonar.Contract.Identifier)]
    [AlternateContract(pantilt.Contract.Identifier)]
    class ArduRoverService : DsspServiceBase
    {
        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        ArduRoverServiceState _state = new ArduRoverServiceState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/ArduRoverService", AllowMultipleInstances = true)]
        ArduRoverServiceOperations _mainPort = new ArduRoverServiceOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        /// <summary>
        /// SerialCOMService partner
        /// </summary>
        [Partner("SerialCOMService", Contract = serialcomservice.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        serialcomservice.SerialCOMServiceOperations _serialCOMServicePort = new serialcomservice.SerialCOMServiceOperations();
        serialcomservice.SerialCOMServiceOperations _serialCOMServiceNotify = new serialcomservice.SerialCOMServiceOperations();

        /// <summary>
        /// Alternate service port
        /// </summary>
        [AlternateServicePort(AlternateContract = gpiopinarray.Contract.Identifier)]
        gpiopinarray.GpioPinArrayOperations _gpioPinArrayPort = new gpiopinarray.GpioPinArrayOperations();

        /// <summary>
        /// Alternate service port
        /// </summary>
        [AlternateServicePort(AlternateContract = analogsensorarray.Contract.Identifier)]
        analogsensorarray.AnalogSensorOperations _analogSensorArrayPort = new analogsensorarray.AnalogSensorOperations();

        /// <summary>
        /// Alternate service port
        /// </summary>
        [AlternateServicePort(AlternateContract = battery.Contract.Identifier)]
        battery.BatteryOperations _batteryPort = new battery.BatteryOperations();

        /// <summary>
        /// Alternate service port
        /// </summary>
        [AlternateServicePort(AlternateContract = drive.Contract.Identifier)]
        drive.DriveOperations _driveDifferentialTwoWheelPort = new drive.DriveOperations();

        /// <summary>
        /// Alternate service port
        /// </summary>
        [AlternateServicePort(AlternateContract = infrared.Contract.Identifier)]
        infrared.InfraredOperations _infraredPort = new infrared.InfraredOperations();

        /// <summary>
        /// Alternate service port
        /// </summary>
        [AlternateServicePort(AlternateContract = sonar.Contract.Identifier)]
        sonar.SonarOperations _sonarPort = new sonar.SonarOperations();

        /// <summary>
        /// Alternate service port
        /// </summary>
        [AlternateServicePort(AlternateContract = pantilt.Contract.Identifier)]
        pantilt.PanTiltOperationsPort _panTiltPort = new pantilt.PanTiltOperationsPort();

        /// <summary>
        /// Service constructor
        /// </summary>
        public ArduRoverService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
        }

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Handles Get requests on alternate port GpioPinArray
        /// </summary>
        /// <param name="get">request message</param>
        [ServiceHandler(PortFieldName = "_gpioPinArrayPort")]
        public void GpioPinArrayGetHandler(gpiopinarray.Get get)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles HttpGet requests on alternate port GpioPinArray
        /// </summary>
        /// <param name="httpget">request message</param>
        [ServiceHandler(PortFieldName = "_gpioPinArrayPort")]
        public void GpioPinArrayHttpGetHandler(Microsoft.Dss.Core.DsspHttp.HttpGet httpget)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Replace requests on alternate port GpioPinArray
        /// </summary>
        /// <param name="replace">request message</param>
        [ServiceHandler(PortFieldName = "_gpioPinArrayPort")]
        public void GpioPinArrayReplaceHandler(gpiopinarray.Replace replace)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles ReliableSubscribe requests on alternate port GpioPinArray
        /// </summary>
        /// <param name="reliablesubscribe">request message</param>
        [ServiceHandler(PortFieldName = "_gpioPinArrayPort")]
        public void GpioPinArrayReliableSubscribeHandler(gpiopinarray.ReliableSubscribe reliablesubscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Subscribe requests on alternate port GpioPinArray
        /// </summary>
        /// <param name="subscribe">request message</param>
        [ServiceHandler(PortFieldName = "_gpioPinArrayPort")]
        public void GpioPinArraySubscribeHandler(gpiopinarray.Subscribe subscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles SetPin requests on alternate port GpioPinArray
        /// </summary>
        /// <param name="setpin">request message</param>
        [ServiceHandler(PortFieldName = "_gpioPinArrayPort")]
        public void GpioPinArraySetPinHandler(gpiopinarray.SetPin setpin)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Get requests on alternate port AnalogSensorArray
        /// </summary>
        /// <param name="get">request message</param>
        [ServiceHandler(PortFieldName = "_analogSensorArrayPort")]
        public void AnalogSensorArrayGetHandler(analogsensorarray.Get get)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles HttpGet requests on alternate port AnalogSensorArray
        /// </summary>
        /// <param name="httpget">request message</param>
        [ServiceHandler(PortFieldName = "_analogSensorArrayPort")]
        public void AnalogSensorArrayHttpGetHandler(Microsoft.Dss.Core.DsspHttp.HttpGet httpget)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Replace requests on alternate port AnalogSensorArray
        /// </summary>
        /// <param name="replace">request message</param>
        [ServiceHandler(PortFieldName = "_analogSensorArrayPort")]
        public void AnalogSensorArrayReplaceHandler(analogsensorarray.Replace replace)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles ReliableSubscribe requests on alternate port AnalogSensorArray
        /// </summary>
        /// <param name="reliablesubscribe">request message</param>
        [ServiceHandler(PortFieldName = "_analogSensorArrayPort")]
        public void AnalogSensorArrayReliableSubscribeHandler(analogsensorarray.ReliableSubscribe reliablesubscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Subscribe requests on alternate port AnalogSensorArray
        /// </summary>
        /// <param name="subscribe">request message</param>
        [ServiceHandler(PortFieldName = "_analogSensorArrayPort")]
        public void AnalogSensorArraySubscribeHandler(analogsensorarray.Subscribe subscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Get requests on alternate port Battery
        /// </summary>
        /// <param name="get">request message</param>
        [ServiceHandler(PortFieldName = "_batteryPort")]
        public void BatteryGetHandler(battery.Get get)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles HttpGet requests on alternate port Battery
        /// </summary>
        /// <param name="httpget">request message</param>
        [ServiceHandler(PortFieldName = "_batteryPort")]
        public void BatteryHttpGetHandler(Microsoft.Dss.Core.DsspHttp.HttpGet httpget)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Replace requests on alternate port Battery
        /// </summary>
        /// <param name="replace">request message</param>
        [ServiceHandler(PortFieldName = "_batteryPort")]
        public void BatteryReplaceHandler(battery.Replace replace)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Subscribe requests on alternate port Battery
        /// </summary>
        /// <param name="subscribe">request message</param>
        [ServiceHandler(PortFieldName = "_batteryPort")]
        public void BatterySubscribeHandler(battery.Subscribe subscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles SetCriticalLevel requests on alternate port Battery
        /// </summary>
        /// <param name="setcriticallevel">request message</param>
        [ServiceHandler(PortFieldName = "_batteryPort")]
        public void BatterySetCriticalLevelHandler(battery.SetCriticalLevel setcriticallevel)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Get requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="get">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelGetHandler(drive.Get get)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles HttpGet requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="httpget">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelHttpGetHandler(Microsoft.Dss.Core.DsspHttp.HttpGet httpget)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles HttpPost requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="httppost">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelHttpPostHandler(Microsoft.Dss.Core.DsspHttp.HttpPost httppost)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles ReliableSubscribe requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="reliablesubscribe">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelReliableSubscribeHandler(drive.ReliableSubscribe reliablesubscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Subscribe requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="subscribe">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelSubscribeHandler(drive.Subscribe subscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Update requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="update">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelUpdateHandler(drive.Update update)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles EnableDrive requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="enabledrive">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelEnableDriveHandler(drive.EnableDrive enabledrive)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles SetDrivePower requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="setdrivepower">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelSetDrivePowerHandler(drive.SetDrivePower setdrivepower)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles SetDriveSpeed requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="setdrivespeed">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelSetDriveSpeedHandler(drive.SetDriveSpeed setdrivespeed)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles RotateDegrees requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="rotatedegrees">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelRotateDegreesHandler(drive.RotateDegrees rotatedegrees)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles DriveDistance requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="drivedistance">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelDriveDistanceHandler(drive.DriveDistance drivedistance)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles AllStop requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="allstop">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelAllStopHandler(drive.AllStop allstop)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles ResetEncoders requests on alternate port DriveDifferentialTwoWheel
        /// </summary>
        /// <param name="resetencoders">request message</param>
        [ServiceHandler(PortFieldName = "_driveDifferentialTwoWheelPort")]
        public void DriveDifferentialTwoWheelResetEncodersHandler(drive.ResetEncoders resetencoders)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Get requests on alternate port Infrared
        /// </summary>
        /// <param name="get">request message</param>
        [ServiceHandler(PortFieldName = "_infraredPort")]
        public void InfraredGetHandler(infrared.Get get)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles HttpGet requests on alternate port Infrared
        /// </summary>
        /// <param name="httpget">request message</param>
        [ServiceHandler(PortFieldName = "_infraredPort")]
        public void InfraredHttpGetHandler(Microsoft.Dss.Core.DsspHttp.HttpGet httpget)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Replace requests on alternate port Infrared
        /// </summary>
        /// <param name="replace">request message</param>
        [ServiceHandler(PortFieldName = "_infraredPort")]
        public void InfraredReplaceHandler(infrared.Replace replace)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles ReliableSubscribe requests on alternate port Infrared
        /// </summary>
        /// <param name="reliablesubscribe">request message</param>
        [ServiceHandler(PortFieldName = "_infraredPort")]
        public void InfraredReliableSubscribeHandler(infrared.ReliableSubscribe reliablesubscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Subscribe requests on alternate port Infrared
        /// </summary>
        /// <param name="subscribe">request message</param>
        [ServiceHandler(PortFieldName = "_infraredPort")]
        public void InfraredSubscribeHandler(infrared.Subscribe subscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Get requests on alternate port Sonar
        /// </summary>
        /// <param name="get">request message</param>
        [ServiceHandler(PortFieldName = "_sonarPort")]
        public void SonarGetHandler(sonar.Get get)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles HttpGet requests on alternate port Sonar
        /// </summary>
        /// <param name="httpget">request message</param>
        [ServiceHandler(PortFieldName = "_sonarPort")]
        public void SonarHttpGetHandler(Microsoft.Dss.Core.DsspHttp.HttpGet httpget)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Replace requests on alternate port Sonar
        /// </summary>
        /// <param name="replace">request message</param>
        [ServiceHandler(PortFieldName = "_sonarPort")]
        public void SonarReplaceHandler(sonar.Replace replace)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles ReliableSubscribe requests on alternate port Sonar
        /// </summary>
        /// <param name="reliablesubscribe">request message</param>
        [ServiceHandler(PortFieldName = "_sonarPort")]
        public void SonarReliableSubscribeHandler(sonar.ReliableSubscribe reliablesubscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Subscribe requests on alternate port Sonar
        /// </summary>
        /// <param name="subscribe">request message</param>
        [ServiceHandler(PortFieldName = "_sonarPort")]
        public void SonarSubscribeHandler(sonar.Subscribe subscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Get requests on alternate port PanTilt
        /// </summary>
        /// <param name="get">request message</param>
        [ServiceHandler(PortFieldName = "_panTiltPort")]
        public void PanTiltGetHandler(pantilt.Get get)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Replace requests on alternate port PanTilt
        /// </summary>
        /// <param name="replace">request message</param>
        [ServiceHandler(PortFieldName = "_panTiltPort")]
        public void PanTiltReplaceHandler(pantilt.Replace replace)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Subscribe requests on alternate port PanTilt
        /// </summary>
        /// <param name="subscribe">request message</param>
        [ServiceHandler(PortFieldName = "_panTiltPort")]
        public void PanTiltSubscribeHandler(pantilt.Subscribe subscribe)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles Rotate requests on alternate port PanTilt
        /// </summary>
        /// <param name="rotate">request message</param>
        [ServiceHandler(PortFieldName = "_panTiltPort")]
        public void PanTiltRotateHandler(pantilt.Rotate rotate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles UpdateMotionBlocked requests on alternate port PanTilt
        /// </summary>
        /// <param name="updatemotionblocked">request message</param>
        [ServiceHandler(PortFieldName = "_panTiltPort")]
        public void PanTiltUpdateMotionBlockedHandler(pantilt.UpdateMotionBlocked updatemotionblocked)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles FindJointIndexPosition requests on alternate port PanTilt
        /// </summary>
        /// <param name="findjointindexposition">request message</param>
        [ServiceHandler(PortFieldName = "_panTiltPort")]
        public void PanTiltFindJointIndexPositionHandler(pantilt.FindJointIndexPosition findjointindexposition)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles SetHoldingTorque requests on alternate port PanTilt
        /// </summary>
        /// <param name="setholdingtorque">request message</param>
        [ServiceHandler(PortFieldName = "_panTiltPort")]
        public void PanTiltSetHoldingTorqueHandler(pantilt.SetHoldingTorque setholdingtorque)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles StartTrajectory requests on alternate port PanTilt
        /// </summary>
        /// <param name="starttrajectory">request message</param>
        [ServiceHandler(PortFieldName = "_panTiltPort")]
        public void PanTiltStartTrajectoryHandler(pantilt.StartTrajectory starttrajectory)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles SetPDGains requests on alternate port PanTilt
        /// </summary>
        /// <param name="setpdgains">request message</param>
        [ServiceHandler(PortFieldName = "_panTiltPort")]
        public void PanTiltSetPDGainsHandler(pantilt.SetPDGains setpdgains)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles HttpPost requests on alternate port PanTilt
        /// </summary>
        /// <param name="httppost">request message</param>
        [ServiceHandler(PortFieldName = "_panTiltPort")]
        public void PanTiltHttpPostHandler(Microsoft.Dss.Core.DsspHttp.HttpPost httppost)
        {
            throw new NotImplementedException();
        }
    }
}


