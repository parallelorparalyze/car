// -----------------------------------------------------------------------
// Team Purple Threads
// CSE Capstone
// Arizona State University
// 2012
// Contains source whole or in part from Microsoft Robotics Developer Studio
// <copyright file="AssemblyInfo.cs" company="Microsoft Corporation">
// Copyright (C) Microsoft Corporation.  All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Reflection;
using dss = Microsoft.Dss.Core.Attributes;
using interop = System.Runtime.InteropServices;

[assembly: dss.ServiceDeclaration(dss.DssServiceDeclaration.ServiceBehavior)]
[assembly: interop.ComVisible(false)]
[assembly: AssemblyTitle("ArduRoverService")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("ArduRoverService")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("1.0.0.0")]



