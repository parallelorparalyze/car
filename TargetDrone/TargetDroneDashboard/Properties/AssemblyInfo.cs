// -----------------------------------------------------------------------
// Team Purple Threads
// CSE Capstone
// Arizona State University
// 2012
// Contains source whole or in part from Microsoft Robotics Developer Studio
// <copyright file="AssemblyInfo.cs" company="Microsoft Corporation">
// Copyright (C) Microsoft Corporation.  All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Dss.Core.Attributes;
using dss = Microsoft.Dss.Core.Attributes;
using interop = System.Runtime.InteropServices;

[assembly: ServiceDeclaration(DssServiceDeclaration.ServiceBehavior)]
[assembly: ComVisible(false)]
[assembly: AssemblyTitle("TargetDroneDashboard")]
[assembly:
    AssemblyDescription(
        "The Target Drone Dashboard can be used to control a target drone. Use an Xbox Controller to drive the robot. The Depth and RGB data streams are displayed in separate windows."
        )]