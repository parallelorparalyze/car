// -----------------------------------------------------------------------
// Team Purple Threads
// CSE Capstone
// Arizona State University
// 2012
// Contains source whole or in part from Microsoft Robotics Developer Studio
// <copyright file="TargetDroneDashboard.cs" company="Microsoft Corporation">
// Copyright (C) Microsoft Corporation.  All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Ccr.Adapters.WinForms;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using Microsoft.Dss.Services.SubscriptionManager;
using Microsoft.Robotics.Services.Battery.Proxy;
using Microsoft.Robotics.Services.DepthCamSensor.Proxy;
using Microsoft.Robotics.Services.Drive.Proxy;
using Microsoft.Robotics.Services.GameController.Proxy;
using Microsoft.Robotics.Services.InfraredSensorArray.Proxy;
using Microsoft.Robotics.Services.PanTilt.Proxy;
using Microsoft.Robotics.Services.SingleAxisJoint.Proxy;
using Microsoft.Robotics.Services.SonarSensorArray.Proxy;
using Microsoft.Robotics.Services.WebCamSensor.Proxy;
using W3C.Soap;

namespace ArizonaStateUniversity.Robotics.Services.Dashboard
{
    using battery = Microsoft.Robotics.Services.Battery.Proxy;
    using depthcamsensor = Microsoft.Robotics.Services.DepthCamSensor.Proxy;
    using drive = Microsoft.Robotics.Services.Drive.Proxy;
    using game = Microsoft.Robotics.Services.GameController.Proxy;
    using infraredsensorarray = Microsoft.Robotics.Services.InfraredSensorArray.Proxy;
    using pantilt = Microsoft.Robotics.Services.PanTilt.Proxy;
    using sonarsensorarray = Microsoft.Robotics.Services.SonarSensorArray.Proxy;
    using submgr = Microsoft.Dss.Services.SubscriptionManager;
    using webcamsensor = Microsoft.Robotics.Services.WebCamSensor.Proxy;

    /// <summary>
    /// Main Target Drone Dashboard Service Class
    /// </summary>
    [Contract(Contract.Identifier)]
    [DisplayName("Target Drone Dashboard")]
    [Description("Target Drone Dashboard service for the Target Drone Platform")]
    public class TargetDroneDashboard : DsspServiceBase, IDisposable
    {
        /// <summary>
        /// Scaling for the joystick values to convert to motor power
        /// </summary>
        /// <remarks>
        /// This scale factor is applied to power settings which must be in the
        /// range -1 to +1. However, the game controller and trackball have a
        /// range of -1000 to +1000.
        /// </remarks>
        private const double MotorPowerSaleFactor = 0.001;

        /// <summary>
        /// Used for converting distance, received in meters to centimeters
        /// </summary>
        private const double ConvertMetersToCm = 100;

        /// <summary>
        /// The battery partner
        /// </summary>
        [Partner("Battery", Contract = Microsoft.Robotics.Services.Battery.Proxy.Contract.Identifier,
            CreationPolicy = PartnerCreationPolicy.UseExisting, Optional = true)] private readonly BatteryOperations
            batteryPort = new BatteryOperations();

        /// <summary>
        /// DepthCam Notifications Port
        /// </summary>
        private readonly DepthCamSensorOperationsPort depthCamSensorNotify = new DepthCamSensorOperationsPort();

        /// <summary>
        /// DepthCamSensor partner
        /// </summary>
        [Partner("DepthCam", Contract = Microsoft.Robotics.Services.DepthCamSensor.Proxy.Contract.Identifier,
            CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate, Optional = true)] private readonly
            DepthCamSensorOperationsPort depthCamSensorPort = new DepthCamSensorOperationsPort();

        /// <summary>
        /// Differential Drive partner
        /// </summary>
        [Partner("Drive", Contract = Microsoft.Robotics.Services.Drive.Proxy.Contract.Identifier,
            CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)] private readonly DriveOperations drivePort =
                new DriveOperations();

        /// <summary>
        /// Port for the UI to send messages back to here (main service)
        /// </summary>
        private readonly DashboardFormEvents eventsPort = new DashboardFormEvents();

        /// <summary>
        /// GameController Notifications Port
        /// </summary>
        private readonly GameControllerOperations gameControllerNotify = new GameControllerOperations();

        /// <summary>
        /// Game Controller partner
        /// </summary>
        /// <remarks>Always create one of these, even if there is no Game Controller attached</remarks>
        [Partner("GameController", Contract = Microsoft.Robotics.Services.GameController.Proxy.Contract.Identifier,
            CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)] private readonly GameControllerOperations
            gameControllerPort = new GameControllerOperations();

        /// <summary>
        /// IR Sensors partner
        /// </summary>
        [Partner("IRSensorArray", Contract = Microsoft.Robotics.Services.InfraredSensorArray.Proxy.Contract.Identifier,
            CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate, Optional = true)] private readonly
            InfraredSensorOperations irsensorArrayPort = new InfraredSensorOperations();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/TargetDroneDashboard", AllowMultipleInstances = true)] private readonly
            TargetDroneDashboardOperations mainPort = new TargetDroneDashboardOperations();

        /// <summary>
        /// The Pan Tilt notifications
        /// </summary>
        private readonly PanTiltOperationsPort panTiltNotify = new PanTiltOperationsPort();

        /// <summary>
        /// The Pan Tilt operations
        /// </summary>
        [Partner("Pan/Tilt", Contract = Microsoft.Robotics.Services.PanTilt.Proxy.Contract.Identifier,
            CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate, Optional = true)] private readonly
            PanTiltOperationsPort panTiltOps = new PanTiltOperationsPort();

        /// <summary>
        /// Sonar Sensors partner
        /// </summary>
        [Partner("SonarSensorArray", Contract = Microsoft.Robotics.Services.SonarSensorArray.Proxy.Contract.Identifier,
            CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate, Optional = true)] private readonly
            SonarSensorOperations sonarSensorArrayPort = new SonarSensorOperations();

        /// <summary>
        /// Subscription Manager port
        /// </summary>
        [SubscriptionManagerPartner] private readonly SubscriptionManagerPort submgrPort = new SubscriptionManagerPort();

        /// <summary>
        /// WebCamSensor Notifications Port
        /// </summary>
        private readonly WebCamSensorOperations webCamNotify = new WebCamSensorOperations();

        /// <summary>
        /// WebCamSensor partner
        /// </summary>
        [Partner("WebCam", Contract = Microsoft.Robotics.Services.WebCamSensor.Proxy.Contract.Identifier,
            CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate, Optional = true)] private readonly
            WebCamSensorOperations webCamPort = new WebCamSensorOperations();

        /// <summary>
        /// The Webcam Form instance
        /// </summary>
        private WebCamForm cameraForm;

        /// <summary>
        /// A handle to the main WinForm UI
        /// </summary>
        private DashboardForm dashboardForm;

        /// <summary>
        /// The Depthcam Form instance
        /// </summary>
        private DepthCamForm depthCameraForm;

        /// <summary>
        /// True if this object has been disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// The polling interval for reading the sensors (milliseconds)
        /// </summary>
        private int sensorPollingInterval = 100;

        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState] [InitialStatePartner(Optional = true, ServiceUri = ServicePaths.Store + "/TargetDroneDashboard.config.xml")] private TargetDroneDashboardState state = new TargetDroneDashboardState();

        /// <summary>
        /// Initializes a new instance of the <see cref="TargetDroneDashboard"/> class.
        /// </summary>
        /// <param name="creationPort">The creation port.</param>
        public TargetDroneDashboard(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        #region IDisposable Members

        /// <summary>
        /// Dispose both managed and native resources
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">The subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(submgrPort, subscribe.Body, subscribe.ResponsePort);
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {
            base.Start();

            InitializeState();

            // Handlers that need write or exclusive access to state go under
            // the exclusive group. Handlers that need read or shared access, and can be
            // concurrent to other readers, go to the concurrent group.
            // Other internal ports can be included in interleave so you can coordinate
            // intermediate computation with top level handlers.
            MainPortInterleave.CombineWith(
                Arbiter.Interleave(
                    new TeardownReceiverGroup(),
                    new ExclusiveReceiverGroup(
                        Arbiter.ReceiveWithIterator<OnLoad>(true, eventsPort, OnLoadHandler),
                        Arbiter.Receive<OnClosed>(true, eventsPort, OnClosedHandler),
                        Arbiter.ReceiveWithIterator<OnChangeJoystick>(true, eventsPort, OnChangeJoystickHandler),
                        Arbiter.ReceiveWithIterator<OnChangeTilt>(true, eventsPort, OnChangeTiltHandler),
                        Arbiter.Receive<Rotate>(true, panTiltNotify, OnRotateSingleAxis),
                        Arbiter.ReceiveWithIterator<OnOptionSettings>(true, eventsPort, OnOptionSettingsHandler),
                        Arbiter.Receive<Microsoft.Robotics.Services.WebCamSensor.Proxy.Replace>(true, webCamNotify,
                                                                                                CameraUpdateFrameHandler),
                        Arbiter.Receive<Microsoft.Robotics.Services.DepthCamSensor.Proxy.Replace>(true,
                                                                                                  depthCamSensorNotify,
                                                                                                  DepthCameraUpdateFrameHandler)),
                    new ConcurrentReceiverGroup(
                        Arbiter.Receive<OnResetEncoders>(true, eventsPort, OnResetEncodersHandler),
                        Arbiter.ReceiveWithIterator<Microsoft.Robotics.Services.GameController.Proxy.Replace>(true,
                                                                                                              gameControllerNotify,
                                                                                                              JoystickReplaceHandler),
                        Arbiter.ReceiveWithIterator<UpdateAxes>(true, gameControllerNotify, JoystickUpdateAxesHandler),
                        Arbiter.ReceiveWithIterator<UpdateButtons>(true, gameControllerNotify,
                                                                   JoystickUpdateButtonsHandler),
                        Arbiter.ReceiveWithIterator<OnMove>(true, eventsPort, OnMoveHandler),
                        Arbiter.ReceiveWithIterator<OnMotionCommand>(true, eventsPort, OnMotionCommandHandler))));

            SpawnIterator(Setup);
        }

        /// <summary>
        /// Dispose this object
        /// </summary>
        /// <param name="disposing">Indicates whether both native and managed resources should be cleaned up</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    depthCameraForm.Dispose();
                    cameraForm.Dispose();
                }

                disposed = true;
            }
        }

        /// <summary>
        /// Initialize the State with appropriate values
        /// </summary>
        private void InitializeState()
        {
            bool newState = false;

            if (state == null)
            {
                state = new TargetDroneDashboardState {TiltAngle = 0};
                newState = true;
            }

            if (state.Options == null)
            {
                state.Options = new GUIOptions
                                    {
                                        DeadZoneX = 150,
                                        DeadZoneY = 150,
                                        WindowStartX = 10,
                                        WindowStartY = 10
                                    };

                // Window geometry parameters
                state.Options.DepthcamWindowStartX = state.Options.WindowStartX + 450;
                state.Options.DepthcamWindowStartY = state.Options.WindowStartY;
                state.Options.DepthcamWindowWidth = 360;
                state.Options.DepthcamWindowHeight = 300;
                state.Options.WebcamWindowStartX = state.Options.WindowStartX + 450;
                state.Options.WebcamWindowStartY = state.Options.WindowStartY + 310;
                state.Options.WebcamWindowWidth = 360;
                state.Options.WebcamWindowHeight = 300;

                state.Options.TranslateScaleFactor = 0.7;
                state.Options.RotateScaleFactor = 0.4;

                // Camera update interval in milliseconds
                // Note that this is only required for the
                // simulated webcam because it does not provide
                // updates when you subscribe
                state.Options.CameraInterval = 250;
            }

            if (state.Options.CameraInterval < 100)
            {
                state.Options.CameraInterval = 100;
            }

            if (newState)
            {
                SaveState(state);
            }
        }

        /// <summary>
        /// Perform the initial setup
        /// </summary>
        /// <returns>An iterator</returns>
        private IEnumerator<ITask> Setup()
        {
            SpawnIterator(ConnectDriveHandler);
            SpawnIterator(ConnectPanTiltHandler);
            SpawnIterator(ConnectWebCamHandler);
            SpawnIterator(ConnectDepthCamHandler);

            WinFormsServicePort.Post(new RunForm(CreateForm));

            SpawnIterator(ProcessSensors);

            yield break;
        }

        /// <summary>
        /// Called when reset encoders is clicked.
        /// </summary>
        /// <param name="resetEncodersRequest">The reset encoders request.</param>
        private void OnResetEncodersHandler(OnResetEncoders resetEncodersRequest)
        {
            if (drivePort != null)
            {
                drivePort.ResetEncoders();
            }
        }

        /// <summary>
        /// Process the Sensor data
        /// </summary>
        /// <returns>An iterator</returns>
        private IEnumerator<ITask> ProcessSensors()
        {
            if (batteryPort == null)
            {
                WinFormsServicePort.FormInvoke(() => dashboardForm.UpdateBatteryLevel(null));
            }

            DateTime? lastBatteryPoll = null;
            while (true)
            {
                DateTime now = DateTime.Now;
                if (irsensorArrayPort != null)
                {
                    // Create a request that includes a Diagnostic Header so that it can be picked
                    // up by selective logging. If no logging is required, the code would just be:
                    // yield return this.irsensorArrayPort.Get().Choice(
                    var getRequest = new Microsoft.Robotics.Services.InfraredSensorArray.Proxy.Get();
                    getRequest.AddHeader(new DiagnosticsHeader());
                    irsensorArrayPort.Post(getRequest);
                    yield return getRequest.ResponsePort.Choice(
                        s =>
                            {
                                var irValues = new double[Constants.IRSensorCount];
                                for (var i = 0; i < Constants.IRSensorCount; i++)
                                {
                                    irValues[Constants.IRSensorCount - i - 1] = s.Sensors[i].DistanceMeasurement*
                                                                                ConvertMetersToCm;
                                }

                                WinFormsServicePort.FormInvoke(() => dashboardForm.UpdateIRSensors(irValues));
                            },
                        LogError);
                }

                if (sonarSensorArrayPort != null)
                {
                    // Create a request that includes a Diagnostic Header so that it can be picked
                    // up by selective logging. If no logging is required, the code would just be:
                    // yield return this.sonarSensorArrayPort.Get().Choice(
                    var getRequest = new Microsoft.Robotics.Services.SonarSensorArray.Proxy.Get();
                    getRequest.AddHeader(new DiagnosticsHeader());
                    sonarSensorArrayPort.Post(getRequest);
                    yield return getRequest.ResponsePort.Choice(
                        s =>
                            {
                                var sonarValues = new double[Constants.SonarSensorCount];
                                for (int i = 0; i < Constants.SonarSensorCount; i++)
                                {
                                    sonarValues[Constants.SonarSensorCount - i - 1] = s.Sensors[i].DistanceMeasurement*
                                                                                      ConvertMetersToCm;
                                }

                                WinFormsServicePort.FormInvoke(() => dashboardForm.UpdateSonarSensors(sonarValues));
                            },
                        LogError);
                }

                if (drivePort != null)
                {
                    yield return drivePort.Get().Choice(
                        s => WinFormsServicePort.FormInvoke(() => dashboardForm.UpdateWheelState(s)),
                        LogError);
                }

                // Only poll the battery every minute
                if (batteryPort != null &&
                    (!lastBatteryPoll.HasValue ||
                     DateTime.Now.Subtract(lastBatteryPoll.Value) > TimeSpan.FromMinutes(1)))
                {
                    yield return
                        batteryPort.Get().Choice(
                            s => WinFormsServicePort.FormInvoke(() => dashboardForm.UpdateBatteryLevel(s)),
                            LogError);

                    lastBatteryPoll = DateTime.Now;
                }

                // Just sleep for the remainder of our sensor polling timout
                int delay = Math.Max(0, (int) (sensorPollingInterval - (DateTime.Now - now).TotalMilliseconds));
                yield return TimeoutPort(delay).Receive();
            }
        }

        /// <summary>
        /// Initialize the pantilt ops
        /// </summary>
        /// <returns>An iterator</returns>
        private IEnumerator<ITask> ConnectPanTiltHandler()
        {
            Fault fault = null;

            if (panTiltOps != null)
            {
                // Try to subscribe
                yield return panTiltOps
                    .Subscribe(panTiltNotify, typeof (Rotate))
                    .Choice(EmptyHandler, f => fault = f);

                if (fault != null)
                {
                    // There is no Kinect partner
                    LogError(null, "Failed to subscribe to Pan/Tilt", fault);
                    yield break;
                }

                // Set the initial tilt angle now
                var request = new OnChangeTilt(dashboardForm, state.TiltAngle);
                SpawnIterator(request, OnChangeTiltHandler);
            }

            yield break;
        }

        /// <summary>
        /// Called on a rotate single axis.
        /// </summary>
        /// <param name="rotate">The rotation.</param>
        private void OnRotateSingleAxis(Rotate rotate)
        {
            var degrees = rotate.Body.RotateTiltRequest.TargetRotationAngleInRadians*180/Math.PI;
            state.TiltAngle = Math.Round(degrees);

            var angle = (int) state.TiltAngle;
            var update = new FormInvoke(() => { dashboardForm.TiltTextbox.Text = angle.ToString(); });
            WinFormsServicePort.Post(update);
        }

        /// <summary>
        /// Handle Tilt Angle requests
        /// </summary>
        /// <param name="tilt">The Tilt request</param>
        /// <returns>An iterator</returns>
        private IEnumerator<ITask> OnChangeTiltHandler(OnChangeTilt tilt)
        {
            Fault fault = null;
            var req = new RotateMessage
                          {
                              RotatePanRequest = null,
                              RotateTiltRequest = new RotateSingleAxisRequest
                                                      {
                                                          TargetRotationAngleInRadians =
                                                              (float) (tilt.Tilt*Math.PI/180.0),
                                                          IsRelative = false
                                                      }
                          };

            yield return panTiltOps.Rotate(req).Choice(EmptyHandler, f => fault = f);

            if (fault != null)
            {
                LogError("Update Tilt failed: ", fault.ToException());
            }
        }

        #region WinForms interaction

        /// <summary>
        /// Create the main Windows Form
        /// </summary>
        /// <returns>A Dashboard Form</returns>
        private Form CreateForm()
        {
            return new DashboardForm(eventsPort, state);
        }

        /// <summary>
        /// Handle the Form Load event for the Dashboard Form
        /// </summary>
        /// <param name="onLoad">The load message</param>
        /// <returns>An iterator</returns>
        private IEnumerator<ITask> OnLoadHandler(OnLoad onLoad)
        {
            dashboardForm = onLoad.DashboardForm;

            LogInfo("Loaded Form");

            yield return EnumerateJoysticks();

            yield return SubscribeToJoystick();
        }

        /// <summary>
        /// Handle the Form Closed event for the Dashboard Form
        /// </summary>
        /// <param name="onClosed">The closed message</param>
        private void OnClosedHandler(OnClosed onClosed)
        {
            if (onClosed.DashboardForm == dashboardForm)
            {
                LogInfo("Form Closed");

                mainPort.Post(new DsspDefaultDrop(DropRequestType.Instance));
                ControlPanelPort.Post(new DsspDefaultDrop(DropRequestType.Instance));

                if (cameraForm != null)
                {
                    var closeWebcam = new FormInvoke(
                        delegate
                            {
                                cameraForm.Close();
                                cameraForm = null;
                            });

                    WinFormsServicePort.Post(closeWebcam);
                }

                if (depthCameraForm != null)
                {
                    var closeDepthcam = new FormInvoke(
                        delegate
                            {
                                depthCameraForm.Close();
                                depthCameraForm = null;
                            });

                    WinFormsServicePort.Post(closeDepthcam);
                }
            }
        }

        /// <summary>
        /// Handle saving the Option Settings
        /// </summary>
        /// <param name="opt">An Option Settings object populated by the Options Form</param>
        /// <returns>An Iterator</returns>
        private IEnumerator<ITask> OnOptionSettingsHandler(OnOptionSettings opt)
        {
            state.Options = opt.Options;

            if (cameraForm != null)
            {
                state.Options.WebcamWindowStartX = cameraForm.Location.X;
                state.Options.WebcamWindowStartY = cameraForm.Location.Y;
                state.Options.WebcamWindowWidth = cameraForm.Width;
                state.Options.WebcamWindowHeight = cameraForm.Height;
            }

            if (depthCameraForm != null)
            {
                state.Options.DepthcamWindowStartX = depthCameraForm.Location.X;
                state.Options.DepthcamWindowStartY = depthCameraForm.Location.Y;
                state.Options.DepthcamWindowWidth = depthCameraForm.Width;
                state.Options.DepthcamWindowHeight = depthCameraForm.Height;
            }

            SaveState(state);

            yield break;
        }

        #endregion

        #region Joystick Handlers

        /// <summary>
        /// Connect the Handlers for the Joystick
        /// </summary>
        /// <returns>An Iterator</returns>
        private IEnumerator<ITask> ConnectJoystickHandlers()
        {
            yield return EnumerateJoysticks();

            yield return SubscribeToJoystick();
        }

        /// <summary>
        /// Enumerate the available Joysticks
        /// </summary>
        /// <returns>A Choice</returns>
        private Choice EnumerateJoysticks()
        {
            return Arbiter.Choice(
                gameControllerPort.GetControllers(new GetControllersRequest()),
                response =>
                WinFormsServicePort.FormInvoke(() => dashboardForm.ReplaceJoystickList(response.Controllers)),
                LogError);
        }

        /// <summary>
        /// Subscribe to the Joystick
        /// </summary>
        /// <returns>A Choice</returns>
        private Choice SubscribeToJoystick()
        {
            return Arbiter.Choice(gameControllerPort.Subscribe(gameControllerNotify), EmptyHandler, LogError);
        }

        /// <summary>
        /// Handle Joystick Replace messages
        /// </summary>
        /// <param name="replace">The replace message</param>
        /// <returns>An Iterator</returns>
        private IEnumerator<ITask> JoystickReplaceHandler(
            Microsoft.Robotics.Services.GameController.Proxy.Replace replace)
        {
            var p =
                (Port<Microsoft.Robotics.Services.GameController.Proxy.Replace>)
                gameControllerNotify[typeof (Microsoft.Robotics.Services.GameController.Proxy.Replace)];
            if (p.ItemCount > 10)
            {
                Console.WriteLine("Joystick backlog: " + p.ItemCount);
            }

            if (dashboardForm != null)
            {
                WinFormsServicePort.FormInvoke(
                    delegate
                        {
                            dashboardForm.UpdateJoystickButtons(replace.Body.Buttons);
                            dashboardForm.UpdateJoystickAxes(replace.Body.Axes);
                        });
            }

            yield break;
        }

        /// <summary>
        /// Handle changes to the Joystick position
        /// </summary>
        /// <param name="update">The updated Axis information</param>
        /// <returns>An Iterator</returns>
        private IEnumerator<ITask> JoystickUpdateAxesHandler(UpdateAxes update)
        {
            var p = (Port<UpdateAxes>) gameControllerNotify[typeof (UpdateAxes)];
            if (p.ItemCount > 10)
            {
                Console.WriteLine("Joystick Axes backlog: " + p.ItemCount);
            }

            if (dashboardForm != null)
            {
                WinFormsServicePort.FormInvoke(() => dashboardForm.UpdateJoystickAxes(update.Body));
            }

            yield break;
        }

        /// <summary>
        /// Handle updates to the buttons on the Gamepad
        /// </summary>
        /// <param name="update">The parameter is not used.</param>
        /// <returns>An Iterator</returns>
        private IEnumerator<ITask> JoystickUpdateButtonsHandler(UpdateButtons update)
        {
            if (dashboardForm != null)
            {
                WinFormsServicePort.FormInvoke(() => dashboardForm.UpdateJoystickButtons(update.Body));
            }

            yield break;
        }

        /// <summary>
        /// Handle changes to the Joystick
        /// </summary>
        /// <param name="onChangeJoystick">The change message</param>
        /// <returns>An Iterator</returns>
        private IEnumerator<ITask> OnChangeJoystickHandler(OnChangeJoystick onChangeJoystick)
        {
            if (onChangeJoystick.DashboardForm == dashboardForm)
            {
                Activate(Arbiter.Choice(
                    gameControllerPort.ChangeController(onChangeJoystick.Joystick),
                    response => LogInfo("Changed Joystick"),
                    f => LogError(null, "Unable to change Joystick", f)));
            }

            yield break;
        }

        #endregion

        #region Drive Operations

        /// <summary>
        /// Connect to the Diff Drive
        /// </summary>
        /// <returns>An Iterator</returns>
        private IEnumerator<ITask> ConnectDriveHandler()
        {
            var request = new EnableDriveRequest {Enable = true};

            if (drivePort != null)
            {
                yield return Arbiter.Choice(drivePort.EnableDrive(request), EmptyHandler, LogError);
            }
        }

        /// <summary>
        /// Handle Motion Commands
        /// </summary>
        /// <param name="onMove">The motion command</param>
        /// <returns>An Iterator</returns>
        private IEnumerator<ITask> OnMoveHandler(OnMove onMove)
        {
            var p = (Port<OnMove>) eventsPort[typeof (OnMove)];
            if (p.ItemCount > 10)
            {
                Console.WriteLine("OnMove backlog: " + p.ItemCount);
            }

            if (onMove.DashboardForm == dashboardForm && drivePort != null)
            {
                var request = new SetDrivePowerRequest
                                  {
                                      LeftWheelPower = onMove.Left*MotorPowerSaleFactor,
                                      RightWheelPower = onMove.Right*MotorPowerSaleFactor
                                  };

                yield return Arbiter.Choice(drivePort.SetDrivePower(request), EmptyHandler, LogError);
            }
        }

        /// <summary>
        /// Handle Motion Commands
        /// </summary>
        /// <param name="onCommand">The motion command</param>
        /// <returns>An Iterator</returns>
        private IEnumerator<ITask> OnMotionCommandHandler(OnMotionCommand onCommand)
        {
            if (onCommand.DashboardForm == dashboardForm && drivePort != null)
            {
                switch (onCommand.Command)
                {
                    case MOTIONCOMMANDS.Rotate:

                        var rotRequest = new RotateDegreesRequest
                                             {
                                                 Degrees = onCommand.Parameter,
                                                 Power = onCommand.Power*MotorPowerSaleFactor
                                             };

                        yield return Arbiter.Choice(drivePort.RotateDegrees(rotRequest), EmptyHandler, LogError);
                        break;

                    case MOTIONCOMMANDS.Translate:
                        var transRequest = new DriveDistanceRequest
                                               {
                                                   Distance = onCommand.Parameter,
                                                   Power = onCommand.Power*MotorPowerSaleFactor
                                               };

                        yield return Arbiter.Choice(drivePort.DriveDistance(transRequest), EmptyHandler, LogError);
                        break;

                    case MOTIONCOMMANDS.Enable:
                        var request = new EnableDriveRequest {Enable = true};

                        yield return Arbiter.Choice(drivePort.EnableDrive(request), EmptyHandler, LogError);
                        break;

                    default:
                        LogInfo("Requesting EStop");
                        var stopRequest = new AllStopRequest();

                        yield return Arbiter.Choice(drivePort.AllStop(stopRequest), EmptyHandler, LogError);
                        break;
                }
            }

            yield break;
        }

        #endregion

        #region Camera

        /// <summary>
        /// Initialize the Web camera
        /// </summary>
        /// <returns>An iterator</returns>
        private IEnumerator<ITask> ConnectWebCamHandler()
        {
            Fault fault = null;

            yield return
                Arbiter.Choice(
                    webCamPort.Subscribe(webCamNotify),
                    EmptyHandler,
                    f => fault = f);

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to webcam", fault);
                yield break;
            }

            var runForm = new RunForm(CreateWebCamForm);

            WinFormsServicePort.Post(runForm);

            yield return Arbiter.Choice(runForm.pResult, EmptyHandler, e => fault = Fault.FromException(e));

            if (fault != null)
            {
                LogError(null, "Failed to Create WebCam window", fault);
                yield break;
            }

            yield break;
        }

        /// <summary>
        /// Create a form for the webcam 
        /// </summary>
        /// <returns>A Webcam Form</returns>
        private Form CreateWebCamForm()
        {
            cameraForm = new WebCamForm(
                mainPort,
                state.Options.WebcamWindowStartX,
                state.Options.WebcamWindowStartY,
                state.Options.WebcamWindowWidth,
                state.Options.WebcamWindowHeight);
            return cameraForm;
        }

        /// <summary>
        /// Handler for new frames from the camera 
        /// </summary>
        /// <param name="replace">A webcamsensor Replace message with the image</param>
        private void CameraUpdateFrameHandler(Microsoft.Robotics.Services.WebCamSensor.Proxy.Replace replace)
        {
            // Make sure that there is a form to display the image on
            if (cameraForm == null)
            {
                return;
            }

            var bmp = new Bitmap(replace.Body.Width, replace.Body.Height, PixelFormat.Format24bppRgb);
            var bmpData = bmp.LockBits(
                new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.WriteOnly,
                bmp.PixelFormat);
            Marshal.Copy(replace.Body.Data, 0, bmpData.Scan0, replace.Body.Data.Length);
            bmp.UnlockBits(bmpData);

            // MakeBitmap(frame.Size.Width, frame.Size.Height, frame.Frame);
            DisplayImage(bmp);
        }

        /// <summary>
        /// Display an image in the WebCam Form
        /// </summary>
        /// <param name="bmp">
        /// The bitmap to display
        /// </param>
        private void DisplayImage(Bitmap bmp)
        {
            Fault fault = null;

            var setImage = new FormInvoke(() => cameraForm.CameraImage = bmp);

            WinFormsServicePort.Post(setImage);

            Arbiter.Choice(setImage.ResultPort, EmptyHandler, e => fault = Fault.FromException(e));

            if (fault == null)
            {
                // LogInfo("New camera frame");
                return;
            }

            LogError(null, "Unable to set camera image on form", fault);
            return;
        }

        #endregion

        #region Depth Camera

        /// <summary>
        /// Initialize the Depth Camera
        /// </summary>
        /// <returns>An iterator</returns>
        private IEnumerator<ITask> ConnectDepthCamHandler()
        {
            Fault fault = null;

            yield return Arbiter.Choice(
                depthCamSensorPort.Subscribe(depthCamSensorNotify),
                EmptyHandler,
                f => fault = f);

            if (fault != null)
            {
                LogError(null, "Failed to subscribe to DepthCam", fault);
                yield break;
            }

            var runForm = new RunForm(CreateDepthCamForm);

            WinFormsServicePort.Post(runForm);

            yield return Arbiter.Choice(runForm.pResult, EmptyHandler, e => fault = Fault.FromException(e));

            if (fault != null)
            {
                LogError(null, "Failed to Create DepthCam window", fault);
                yield break;
            }

            yield break;
        }

        /// <summary>
        /// Create a form for the Depthcam
        /// </summary>
        /// <returns>An iterator</returns>
        private Form CreateDepthCamForm()
        {
            depthCameraForm = new DepthCamForm(
                mainPort,
                state.Options.DepthcamWindowStartX,
                state.Options.DepthcamWindowStartY,
                state.Options.DepthcamWindowWidth,
                state.Options.DepthcamWindowHeight);
            return depthCameraForm;
        }

        /// <summary>
        /// Handler for new frames from the depth camera
        /// </summary>
        /// <param name="replace">A depthcamsensor Replace message containing the depth data</param>
        private void DepthCameraUpdateFrameHandler(Microsoft.Robotics.Services.DepthCamSensor.Proxy.Replace replace)
        {
            // Make sure that there is a form to display the image on
            if (depthCameraForm == null)
            {
                return;
            }

            int width = replace.Body.DepthImageSize.Width;
            int height = replace.Body.DepthImageSize.Height;
            Bitmap bmp = MakeDepthBitmap(width, height, replace.Body.DepthImage);
            DisplayDepthImage(bmp);

            return;
        }

        /// <summary>
        /// Creates a Bitmap from Depth Data
        /// </summary>
        /// <param name="width">Width of the image</param>
        /// <param name="height">Height of the image</param>
        /// <param name="depthData">Raw depth data (in millimeters)</param>
        /// <returns>A grayscale bitmap</returns>
        private Bitmap MakeDepthBitmap(int width, int height, short[] depthData)
        {
            // NOTE: This code implicitly assumes that the width is a multiple
            // of four bytes because Bitmaps have to be longword aligned.
            // We really should look at bmp.Stride to see if there is any padding.
            // However, the width and height come from the webcam and most cameras
            // have resolutions that are multiples of four.

            const short MaxDepthDataValue = 4003;

            var buff = new byte[width*height*3];
            var j = 0;
            for (var i = 0; i < width*height; i++)
            {
                // Convert the data to a suitable range
                byte val;
                if (depthData[i] >= MaxDepthDataValue)
                {
                    val = byte.MaxValue;
                }
                else
                {
                    val = (byte) ((depthData[i]/(double) MaxDepthDataValue)*byte.MaxValue);
                }

                // Set all R, G and B values the same, i.e. gray scale
                buff[j++] = val;
                buff[j++] = val;
                buff[j++] = val;
            }

            // NOTE: Windows Forms do not support Format16bppGrayScale which would be the
            // ideal way to display the data. Instead it is converted to RGB with all the
            // color values the same, i.e. 8-bit gray scale.
            Bitmap bmp = null;

            try
            {
                bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);

                var data = bmp.LockBits(
                    new Rectangle(0, 0, bmp.Width, bmp.Height),
                    ImageLockMode.WriteOnly,
                    PixelFormat.Format24bppRgb);

                Marshal.Copy(buff, 0, data.Scan0, buff.Length);

                bmp.UnlockBits(data);
            }
            catch (Exception ex)
            {
                if (bmp != null)
                {
                    bmp.Dispose();
                    bmp = null;
                }

                LogError(ex);
            }

            return bmp;
        }

        /// <summary>
        /// Display an image in the DepthCam Form
        /// </summary>
        /// <param name="bmp">
        /// The bitmap to display
        /// </param>
        private void DisplayDepthImage(Bitmap bmp)
        {
            Fault fault = null;

            var setImage = new FormInvoke(() => depthCameraForm.CameraImage = bmp);

            WinFormsServicePort.Post(setImage);

            Arbiter.Choice(setImage.ResultPort, EmptyHandler, e => fault = Fault.FromException(e));

            if (fault != null)
            {
                LogError(null, "Unable to set depth camera image on form", fault);
                return;
            }

            // LogInfo("New camera frame");
            return;
        }

        #endregion
    }
}