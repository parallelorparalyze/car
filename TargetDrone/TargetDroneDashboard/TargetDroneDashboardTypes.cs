// -----------------------------------------------------------------------
// Team Purple Threads
// CSE Capstone
// Arizona State University
// 2012
// Contains source whole or in part from Microsoft Robotics Developer Studio
// <copyright file="TargetDroneDashboardTypes.cs" company="Microsoft Corporation">
// Copyright (C) Microsoft Corporation.  All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using W3C.Soap;

namespace ArizonaStateUniversity.Robotics.Services.Dashboard
{
    /// <summary>
    /// The number of Proximity Sensors
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// The number of proximity sensors
        /// </summary>
        public const int IRSensorCount = 3;

        /// <summary>
        /// The number of sonar sensors
        /// </summary>
        public const int SonarSensorCount = 2;
    }

    /// <summary>
    /// TargetDroneDashboard contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifier for TargetDroneDashboard
        /// </summary>
        [DataMember] public const string Identifier =
            "http://schemas.asu.edu/2012/02/targetdronedashboard.html";
    }

    /// <summary>
    /// Names of the Proximity Sensors
    /// </summary>
    public enum IrSensorNames
    {
        /// <summary>
        /// Left IR Sensor
        /// </summary>
        IrLeft = 0,

        /// <summary>
        /// Center IR Sensor
        /// </summary>
        IrCenter = 1,

        /// <summary>
        /// Right IR Sensor
        /// </summary>
        IrRight = 2
    }

    /// <summary>
    /// Names of the Sensor Sensors
    /// </summary>
    public enum SonarSensorNames
    {
        /// <summary>
        /// Left Sonar Sensor
        /// </summary>
        SonarLeft = 0,

        /// <summary>
        /// Right Sonar Sensor
        /// </summary>
        SonarRight = 1,
    }

    /// <summary>
    /// TargetDroneDashboard state
    /// </summary>
    [DataContract]
    public class TargetDroneDashboardState
    {
        /// <summary>
        /// Option settings from the GUI
        /// </summary>
        [DataMember] public GUIOptions Options;

        /// <summary>
        /// The current Tilt Angle in degrees
        /// </summary>
        [DataMember] public double TiltAngle;
    }

    /// <summary>
    /// GUIOptions -- Properties "bag" for option settings
    /// </summary>
    [DataContract]
    [DisplayName("GUI Option Settings")]
    public class GUIOptions
    {
        /// <summary>
        /// Update interval for simulated camera -- OBSOLETE!
        /// </summary>
        [DataMember] public int CameraInterval;

        /// <summary>
        /// X range at center of joystick that is "dead"
        /// </summary>
        [DataMember] public double DeadZoneX;

        /// <summary>
        /// Y range at center of joystick that is "dead"
        /// </summary>
        [DataMember] public double DeadZoneY;

        /// <summary>
        /// Height of the Depthcam Window in screen coords
        /// </summary>
        [DataMember] public int DepthcamWindowHeight;

        /// <summary>
        /// Initial X position of the Depthcam Window in screen coords
        /// </summary>
        [DataMember] public int DepthcamWindowStartX;

        /// <summary>
        /// Initial Y position of the Depthcam Window in screen coords
        /// </summary>
        [DataMember] public int DepthcamWindowStartY;

        /// <summary>
        /// Width of the Depthcam Window in screen coords
        /// </summary>
        [DataMember] public int DepthcamWindowWidth;

        /// <summary>
        /// Adjusts the sensitivity for rotating
        /// </summary>
        [DataMember] public double RotateScaleFactor;

        /// <summary>
        /// Adjusts the sensitivity for driving forwards/backwards
        /// </summary>
        [DataMember] public double TranslateScaleFactor;

        /// <summary>
        /// Height of the Webcam Window in screen coords
        /// </summary>
        [DataMember] public int WebcamWindowHeight;

        /// <summary>
        /// Initial X position of the Webcam Window in screen coords
        /// </summary>
        [DataMember] public int WebcamWindowStartX;

        /// <summary>
        /// Initial Y position of the Webcam Window in screen coords
        /// </summary>
        [DataMember] public int WebcamWindowStartY;

        /// <summary>
        /// Width of the Webcam Window in screen coords
        /// </summary>
        [DataMember] public int WebcamWindowWidth;

        /// <summary>
        /// Initial X position of the Window in screen coords
        /// </summary>
        [DataMember] public int WindowStartX;

        /// <summary>
        /// Initial Y position of the Window in screen coords
        /// </summary>
        [DataMember] public int WindowStartY;
    }

    /// <summary>
    /// TargetDroneDashboard main operations port
    /// </summary>
    [ServicePort]
    public class TargetDroneDashboardOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe>
    {
    }

    /// <summary>
    /// TargetDroneDashboard get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<TargetDroneDashboardState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">The request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">The request message body</param>
        /// <param name="responsePort">The response port for the request</param>
        public Get(GetRequestType body, PortSet<TargetDroneDashboardState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// TargetDroneDashboard subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">The request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">The request message body</param>
        /// <param name="responsePort">The response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }
}