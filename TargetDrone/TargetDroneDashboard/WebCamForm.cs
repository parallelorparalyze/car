// -----------------------------------------------------------------------
// Team Purple Threads
// CSE Capstone
// Arizona State University
// 2012
// Contains source whole or in part from Microsoft Robotics Developer Studio
// <copyright file="WebCamForm.cs" company="Microsoft Corporation">
// Copyright (C) Microsoft Corporation.  All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Drawing;
using System.Windows.Forms;

namespace ArizonaStateUniversity.Robotics.Services.Dashboard
{
    /// <summary>
    /// WebCamForm - Form for displaying webcam video
    /// </summary>
    public partial class WebCamForm : Form
    {
        /// <summary>
        /// A bitmap to hold the camera image
        /// </summary>
        private Bitmap cameraImage;

        /// <summary>
        /// Main Port for the Service
        /// </summary>
        /// <remarks>This port is for communicating with the Dashboard.
        /// However, it is not currently used</remarks>
        private TargetDroneDashboardOperations mainPort;

        /// <summary>
        /// Initializes a new instance of the WebCamForm class
        /// </summary>
        /// <param name="port">The main Target Drone Dashboard operations port</param>
        /// <param name="startX">Initial X position</param>
        /// <param name="startY">Initial Y position</param>
        /// <param name="width">Initial Width</param>
        /// <param name="height">Initial Height</param>
        public WebCamForm(TargetDroneDashboardOperations port, int startX, int startY, int width, int height)
        {
            InitializeComponent(); // Required for a Windows Form
            mainPort = port;

            StartPosition = FormStartPosition.Manual;
            Location = new Point(startX, startY);
            Size = new Size(width, height);
        }

        /// <summary>
        /// Gets or sets the Camera Image
        /// </summary>
        /// <remarks>Provides external access for updating the camera image</remarks>
        public Bitmap CameraImage
        {
            get { return cameraImage; }

            set
            {
                cameraImage = value;

                Image old = picCamera.Image;
                picCamera.Image = value;

                // Dispose of the old bitmap to save memory
                // (It will be garbage collected eventually, but this is faster)
                if (old != null)
                {
                    old.Dispose();
                }
            }
        }
    }
}