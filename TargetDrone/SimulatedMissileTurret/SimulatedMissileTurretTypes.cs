using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using Microsoft.Robotics.PhysicalModel;
using Microsoft.Robotics.Simulation;
using Microsoft.Robotics.Simulation.Engine;
using Microsoft.Robotics.Simulation.Physics;
using W3C.Soap;
using xna = Microsoft.Xna.Framework;
using xnagrfx = Microsoft.Xna.Framework.Graphics;

namespace ArizonaStateUniversity.Robotics.Simulation.SimulatedMissileTurret
{
    /// <summary>
    /// SimulatedMissileTurret contract class
    /// </summary>
    public sealed class Contract
    {
        /// <summary>
        /// DSS contract identifer for SimulatedMissileTurret
        /// </summary>
        [DataMember]
        public const string Identifier = "http://schemas.asu.edu/2012/03/simulatedmissileturret.html";
    }

    /// <summary>
    /// SimulatedMissileTurret state
    /// </summary>
    [DataContract]
    public class SimulatedMissileTurretState
    {
    }

    /// <summary>
    /// SimulatedMissileTurret main operations port
    /// </summary>
    [ServicePort]
    public class SimulatedMissileTurretOperations : PortSet<DsspDefaultLookup, DsspDefaultDrop, Get, Subscribe>
    {
    }

    /// <summary>
    /// SimulatedMissileTurret get operation
    /// </summary>
    public class Get : Get<GetRequestType, PortSet<SimulatedMissileTurretState, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        public Get()
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        public Get(GetRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Get
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Get(GetRequestType body, PortSet<SimulatedMissileTurretState, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    /// <summary>
    /// SimulatedMissileTurret subscribe operation
    /// </summary>
    public class Subscribe : Subscribe<SubscribeRequestType, PortSet<SubscribeResponseType, Fault>>
    {
        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        public Subscribe()
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        public Subscribe(SubscribeRequestType body)
            : base(body)
        {
        }

        /// <summary>
        /// Creates a new instance of Subscribe
        /// </summary>
        /// <param name="body">the request message body</param>
        /// <param name="responsePort">the response port for the request</param>
        public Subscribe(SubscribeRequestType body, PortSet<SubscribeResponseType, Fault> responsePort)
            : base(body, responsePort)
        {
        }
    }

    [DataContract]
    [EditorAttribute(typeof(GenericObjectEditor), typeof(System.Drawing.Design.UITypeEditor))]
    public class MissileEntity : SingleShapeEntity
    {
        static Random _random = new Random();

        [DataMember]
        public float Radius { get; set; }

        [DataContract]
        public enum MissileMode
        {
            Holding,
            Position,
            ReadyToDrop,
            Active
        }

        [DataMember]
        public MissileMode CurrentMissileMode { get; set; }

        public MissileEntity()
        {
        }

        public MissileEntity(float radius, Vector3 position)
        {
            Radius = radius;
            base.State.Pose = new Pose(position);
        }

        public override void Initialize(Microsoft.Xna.Framework.Graphics.GraphicsDevice device, PhysicsEngine physicsEngine)
        {
            var capsule = new CapsuleShape(
                new CapsuleShapeProperties(
                    1.0f, // kg
                    new Pose(), // default pose
                    0.070f, 1.0f));
            capsule.CapsuleState.DiffuseColor.X = (float)_random.NextDouble();
            capsule.CapsuleState.DiffuseColor.Y = (float)_random.NextDouble();
            capsule.CapsuleState.DiffuseColor.Z = (float)_random.NextDouble();
            capsule.CapsuleState.DiffuseColor.W = 1.0f;

            // prevent the balls from rolling forever on the floor
            this.State.MassDensity.LinearDamping = 0.05f;

            base.CapsuleShape = capsule;

            base.Initialize(device, physicsEngine);

            base.PhysicsEntity.SolverIterationCount = 128;
        }

        public override void Update(FrameUpdate update)
        {
            switch (CurrentMissileMode)
            {
                case MissileMode.Position:
                    PhysicsEntity.IsKinematic = true;
                    State.Pose.Position = new Vector3((float)(_random.NextDouble() * 0.075 - 0.075), 4.2f, 0);
                    if (PhysicsEntity != null)
                        PhysicsEntity.SetPose(State.Pose);

                    CurrentMissileMode = MissileMode.ReadyToDrop;
                    break;

                case MissileMode.ReadyToDrop:
                    if (PhysicsEntity != null)
                        PhysicsEntity.IsKinematic = false;

                    CurrentMissileMode = MissileMode.Active;
                    break;
            }

            base.Update(update);
        }
    }

    [DataContract]
    [EditorAttribute(typeof(GenericObjectEditor), typeof(System.Drawing.Design.UITypeEditor))]
    public class CapsuleArray : VisualEntity
    {
        int _rows;
        int _columns;
        float _columnSpacing;
        float _rowSpacing;

        [DataMember]
        public int Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }

        [DataMember]
        public int Columns
        {
            get { return _columns; }
            set { _columns = value; }
        }

        [DataMember]
        public float ColumnSpacing
        {
            get { return _columnSpacing; }
            set { _columnSpacing = value; }
        }

        [DataMember]
        public float RowSpacing
        {
            get { return _rowSpacing; }
            set { _rowSpacing = value; }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CapsuleArray()
        {
        }

        /// <summary>
        /// Initial (non deserialized) constructor
        /// </summary>
        public CapsuleArray(int Rows, int Columns, float RowSpacing, float ColumnSpacing, Vector3 position)
        {
            _rows = Rows;
            _columns = Columns;
            _rowSpacing = RowSpacing;
            _columnSpacing = ColumnSpacing;

            State.Pose.Position = position;
            State.Flags |= EntitySimulationModifiers.Kinematic;
        }

        /// <summary>
        /// Initialization
        /// </summary>
        /// <param name="device"></param>
        /// <param name="physicsEngine"></param>
        public override void Initialize(xnagrfx.GraphicsDevice device, PhysicsEngine physicsEngine)
        {
            try
            {
                float halfOffset = _columnSpacing / 2.0f;
                float pegRadius = _columnSpacing * 0.1f;
                float pegHeight = _columnSpacing * 1.2f;

                InitError = string.Empty;
                Pose cPose = new Pose(new Vector3(0, 0, 0), UIMath.EulerToQuaternion(new xna.Vector3(90, 0, 0)));
                float xOffset = (_columns - 1) * _columnSpacing * 0.5f;
                for (int r = 0; r < _rows; r++)
                {
                    cPose.Position.Y = r * _rowSpacing + pegRadius;
                    for (int c = 0; c < _columns; c++)
                    {
                        cPose.Position.X = c * _columnSpacing - xOffset;
                        if ((r & 1) != 0)
                            cPose.Position.X += halfOffset;

                        CapsuleShape peg = new CapsuleShape(
                            new CapsuleShapeProperties(
                                1,
                                new Pose(cPose.Position, cPose.Orientation),
                                pegRadius,
                                pegHeight));
                        peg.State.Material = new MaterialProperties("bouncy", 0.9f, 0.2f, 0.5f);

                        peg.State.Name = "Peg_" + r.ToString() + "_" + c.ToString();
                        base.State.PhysicsPrimitives.Add(peg);
                    }
                }

                // To be visible to the physics simulation we have to explicitly insert ourselves
                CreateAndInsertPhysicsEntity(physicsEngine);

                // call base initialize so it can load effect, cache handles, compute
                // bounding geometry
                base.Initialize(device, physicsEngine);

            }
            catch (Exception ex)
            {
                HasBeenInitialized = false;
                InitError = ex.ToString();
            }
        }
    }
}


