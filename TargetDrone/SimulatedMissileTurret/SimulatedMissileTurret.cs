using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Ccr.Core;
using Microsoft.Dss.Core.Attributes;
using Microsoft.Dss.ServiceModel.Dssp;
using Microsoft.Dss.ServiceModel.DsspServiceBase;
using W3C.Soap;
using submgr = Microsoft.Dss.Services.SubscriptionManager;
using engineProxy = Microsoft.Robotics.Simulation.Engine.Proxy;
using engine = Microsoft.Robotics.Simulation.Engine;
using Microsoft.Robotics.PhysicalModel;
using xna = Microsoft.Xna.Framework;
using xnagrfx = Microsoft.Xna.Framework.Graphics;
using Microsoft.Robotics.Simulation.Physics;
using Microsoft.Dss.Services.Constructor;
using System.Runtime.InteropServices;


namespace ArizonaStateUniversity.Robotics.Simulation.SimulatedMissileTurret
{
    [Contract(Contract.Identifier)]
    [DisplayName("SimulatedMissileTurret")]
    [Description("SimulatedMissileTurret service (no description provided)")]
    class SimulatedMissileTurretService : DsspServiceBase
    {
        const int MissileRate = 2000;     // ms between projectile launch
        const int maxMissileCount = 200; // maximum number of missiles at one time
        int _missileCount = 0;           // total number of projectiles created
        Random _random = new Random();  // random number generator
        Queue<engine.VisualEntity> missiles = new Queue<engine.VisualEntity>();    // keeps track of projectiles

        /// <summary>
        /// Service state
        /// </summary>
        [ServiceState]
        SimulatedMissileTurretState _state = new SimulatedMissileTurretState();

        /// <summary>
        /// Main service port
        /// </summary>
        [ServicePort("/SimulatedMissileTurret", AllowMultipleInstances = false)]
        SimulatedMissileTurretOperations _mainPort = new SimulatedMissileTurretOperations();

        [SubscriptionManagerPartner]
        submgr.SubscriptionManagerPort _submgrPort = new submgr.SubscriptionManagerPort();

        /// <summary>
        /// SimulationEngine partner
        /// </summary>
        [Partner("SimulationEngine", Contract = engine.Contract.Identifier, CreationPolicy = PartnerCreationPolicy.UseExistingOrCreate)]
        engine.SimulationEnginePort _simulationEnginePort = new engine.SimulationEnginePort();

        /// <summary>
        /// Service constructor
        /// </summary>
        public SimulatedMissileTurretService(DsspServiceCreationPort creationPort)
            : base(creationPort)
        {
        }

        /// <summary>
        /// Service start
        /// </summary>
        protected override void Start()
        {

            // 
            // Add service specific initialization here
            // 

            base.Start();
            // Orient sim camera view point
            SetupCamera();
            // Add objects (entities) in our simulated world
            PopulateWorld();
        }

        private void SetupCamera()
        {
            // Set up initial view
            var view = new engine.CameraView();
            view.EyePosition = new Vector3(-2.72f, 4.66f, -3.51f);
            view.LookAtPoint = new Vector3(-2.14f, 4.18f, -2.84f);
            engine.SimulationEngine.GlobalInstancePort.Update(view);
        }
        private void PopulateWorld()
        {
            // add some additional cameras
            engine.CameraEntity camera2 = new engine.CameraEntity();
            camera2.Location = new xna.Vector3(-2.65f, 0.07f, 1.42f);
            camera2.LookAt = new xna.Vector3(-1.98f, 0.37f, 0.74f);
            camera2.State.Name = "MissileMill View";
            engine.SimulationEngine.GlobalInstancePort.Insert(camera2);

            engine.CameraEntity camera3 = new engine.CameraEntity();
            camera3.Location = new xna.Vector3(0, 4.86f, 0);
            camera3.LookAt = new xna.Vector3(0, 3.86f, 0);
            camera3.State.Name = "View From Above";
            engine.SimulationEngine.GlobalInstancePort.Insert(camera3);

            // add a SkyDome and Ground
            engine.SkyDomeEntity sky = new engine.SkyDomeEntity("skyDome.dds", "sky_diff.dds");
            engine.SimulationEngine.GlobalInstancePort.Insert(sky);

            // Add a directional light to simulate the sun.
            engine.LightSourceEntity sun = new engine.LightSourceEntity();
            sun.State.Name = "Sun";
            sun.Type = engine.LightSourceEntityType.Directional;
            sun.Color = new Vector4(0.8f, 0.8f, 0.8f, 1);
            sun.Direction = new Vector3(0.5f, -.75f, 0.5f);
            engine.SimulationEngine.GlobalInstancePort.Insert(sun);

            // create a large horizontal plane, at zero elevation.
            engine.HeightFieldEntity ground = new engine.HeightFieldEntity(
                "simple ground", // name
                "HexTile.dds", // texture image
                new MaterialProperties("ground",
                    0.2f, // restitution
                    0.5f, // dynamic friction
                    0.5f) // static friction
                );
            engine.SimulationEngine.GlobalInstancePort.Insert(ground);

            // Build an array of kinematic capsules
            CapsuleArray pegboard = new CapsuleArray(10, 10, 0.2f, 0.2f, new Vector3(0, 2.0f, 0));
            pegboard.State.Name = "Pegboard";
            engine.SimulationEngine.GlobalInstancePort.Insert(pegboard);

            // Build a ramp to collect the missiles
            BoxShape[] rails = new BoxShape[2];
            BoxShape rail = new BoxShape(
                new BoxShapeProperties(
                  10, //kg
                  new Pose(),
                  new Vector3(2.5f, 0.06f, 0.2f)));
            rail.State.Name = "railshape";

            engine.SingleShapeEntity ramp = new engine.SingleShapeEntity(rail, new Vector3());
            ramp.State.Flags |= EntitySimulationModifiers.Kinematic;
            ramp.State.Name = "Ramp";
            ramp.State.Pose = new Pose(
                new Vector3(-0.025961088f, 1.5949626f, 0),
                UIMath.EulerToQuaternion(new xna.Vector3(0, 0, 11.06f)));
            ramp.State.Assets.DefaultTexture = "Ground2.bmp";
            engine.SimulationEngine.GlobalInstancePort.Insert(ramp);


            CreateMissiles();
            Activate(Arbiter.Receive(false, TimeoutPort(MissileRate), DropAMissile));
        }

        void CreateMissiles()
        {
            Vector3 holdingPosition = new Vector3(1000, 1000, 1000);
            for (_missileCount = 0; _missileCount <= maxMissileCount; _missileCount++)
            {
                MissileEntity missile = new MissileEntity(0.07f, holdingPosition);
                missile.State.Name = "Missile" + _missileCount++;
                missile.State.Flags = EntitySimulationModifiers.Kinematic;

                // Insert entity in simulation.  
                engine.SimulationEngine.GlobalInstancePort.Insert(missile);
                missiles.Enqueue(missile);
            }
        }

        void DropAMissile(DateTime time)
        {
            MissileEntity missile = (MissileEntity)missiles.Dequeue();
            missile.CurrentMissileMode = MissileEntity.MissileMode.Position;
            missiles.Enqueue(missile);
            Activate(Arbiter.Receive(false, TimeoutPort(MissileRate), DropAMissile));
        }

       

        /// <summary>
        /// Handles Subscribe messages
        /// </summary>
        /// <param name="subscribe">the subscribe request</param>
        [ServiceHandler]
        public void SubscribeHandler(Subscribe subscribe)
        {
            SubscribeHelper(_submgrPort, subscribe.Body, subscribe.ResponsePort);
        }
    }
}


