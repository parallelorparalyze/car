/*#include "libfreenect.hpp"
#include <iostream>
#include <vector>
#include <cmath>
#include <pthread.h>
#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <assert.h>
#include "libfreenect_cv.h"
#include "libfreenect_sync.h"
*/
//#include <stdlib.h>
//#include <stdio.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <libfreenect_sync.h>
#include <math.h>

float y_elevation=3.00;
float gravity = 32.174;
float velocity = 30;
float pi = 3.14159265;
using namespace std;

struct angles
{
	float theta;
	float phi;
};

struct angles setangle(float x, float y, float distance);

angles setangle(float x, float y, float distance)
{
	angles reddit;
	float x_feet, d_feet,t,p,thetadeg,phideg, x_pixels, x_pixelsfoot;
	
	//d_feet = exp((distance-542.4451052979)/210.594467926)*1.174213646;
	if(distance > 950)
	{
		d_feet = (distance-824.8)/15.2;
	}
	else
	{
	    d_feet = (0.1236*tan((distance/2842.5)+1.1863))*3.2808399;
	}
	printf("Distance in Feet: %f\n",d_feet);
	//d_feet=(0.490592*d_feet-2.751692);
	if (x < 320.00)
	{
		x_pixels = 320.00 - x;
		x_pixelsfoot = -55.91836*log(d_feet)+177.49225974457;
		printf("Pixels per foot%f\n",x_pixelsfoot);
		x_feet=x_pixels/x_pixelsfoot;
		thetadeg = asin(x_feet/d_feet) * (180.0 / pi);
		thetadeg = 90 - thetadeg;
		reddit.theta=thetadeg;
	}
	else if (x > 320.00)
	{
		x_pixels = x-320;
		x_pixelsfoot = -55.91836*log(d_feet)+177.49225974457;
		printf("Pixels per foot%f\n",x_pixelsfoot);
		x_feet=x_pixels/x_pixelsfoot;
		thetadeg = asin(x_feet/d_feet) * (180.0 / pi);
		thetadeg = 90 + thetadeg;
		reddit.theta= thetadeg;
	}
	else if (x == 320.0)
	{
		x_feet = x - 320.0;
		thetadeg= 0.0;	
		reddit.theta=90.0;
	}
	printf("Feet from Center Axis: %f\n",x_feet);
	//x_feet = exp((x_feet-46.0508955701)/-12.108254467);
	//thetadeg = acos(x_feet/d_feet) * (180.0 / pi);
	//reddit.theta= 90.0 -thetadeg;
	// not now reddit.phi=acos(sqrt(((pow(distance,2)*g*y_elevation)/()));
	phideg = (((1.0/2.0)*asin((d_feet*gravity)/(velocity*velocity))))*(180.0/pi);
	//phideg = (phideg *(180.0/pi);
	phideg = phideg + 90;
	reddit.phi= phideg;
	return reddit;
	
}

IplImage *GlViewColor(IplImage *depth)
{
	static IplImage *image = 0;
	if (!image) image = cvCreateImage(cvSize(640,480), 8, 3);
	unsigned char *depth_mid = (unsigned char*)(image->imageData);
	int i;
	for (i = 0; i < 640*480; i++) {
		int lb = ((short *)depth->imageData)[i] % 256;
		int ub = ((short *)depth->imageData)[i] / 256;
		switch (ub) {
			case 0:
				depth_mid[3*i+2] = 255;
				depth_mid[3*i+1] = 255-lb;
				depth_mid[3*i+0] = 255-lb;
				break;
			case 1:
				depth_mid[3*i+2] = 255;
				depth_mid[3*i+1] = lb;
				depth_mid[3*i+0] = 0;
				break;
			case 2:
				depth_mid[3*i+2] = 255-lb;
				depth_mid[3*i+1] = 255;
				depth_mid[3*i+0] = 0;
				break;
			case 3:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 255;
				depth_mid[3*i+0] = lb;
				break;
			case 4:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 255-lb;
				depth_mid[3*i+0] = 255;
				break;
			case 5:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 0;
				depth_mid[3*i+0] = 255-lb;
				break;
			default:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 0;
				depth_mid[3*i+0] = 0;
				break;
		}
	}
	return image;
}

int main ()
{
	int time = 200;
	CvSize size = cvSize(640,480);
	freenect_sync_set_tilt_degs(0, 0);
	//Created Windows
	cvNamedWindow("Camera", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("HSV", CV_WINDOW_AUTOSIZE);
	//cvNamedWindow("Threshold", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("Depth", CV_WINDOW_AUTOSIZE);

	//min & max Hue Saturation Value
	CvScalar hsv_min = cvScalar( 0, 100 , 100);
	CvScalar hsv_max = cvScalar( 5, 200, 200 );

	CvScalar hsv_min2 = cvScalar( 170, 50, 170);
	CvScalar hsv_max2 = cvScalar( 256, 180, 256);
	clock_t previous_t = clock() , current_t = clock();

	//Kalman filer initialization

		CvRandState rng;
		cvRandInit( &rng, 0, 1, -1, CV_RAND_UNI );
		CvKalman* kalman = cvCreateKalman(4,2,0);

		//Create Initial transitionMatrix
		//CV_32FC1 means 32 floating point type

		CvMat* x_k = cvCreateMat(4,1,CV_32FC1);
		float initVals[] = { 0, 0, 0, 0};
		memcpy ( x_k->data.fl, initVals, sizeof(initVals));
		//Initial process noise
		
		CvMat* w_k = cvCreateMat( 4, 1, CV_32FC1 );

		//measurements two parameters x and y
		CvMat* z_k = cvCreateMat(2,1,CV_32FC1);
		cvZero(z_k);

		//Transition Matrix F
		const float F[] = { 1, 0, 1, 0,
							0, 1, 0, 1,
							0, 0, 1, 0,
							0, 0, 0, 1};
		memcpy ( kalman->transition_matrix->data.fl, F, sizeof(F));
		
		//Measurement matrix set to identity matrix
		//process noise set to low value
		//measurement noise set to value
		//error_conv set to identity
		cvSetIdentity( kalman->measurement_matrix, cvRealScalar(1) );
		cvSetIdentity( kalman->process_noise_cov, cvRealScalar(1e-5) );
		cvSetIdentity( kalman->measurement_noise_cov, cvRealScalar(1e-1) );
		cvSetIdentity( kalman->error_cov_post, cvRealScalar(1) );

		//initial state needs to be set
		kalman->state_pre->data.fl[0] = 95;
		kalman->state_pre->data.fl[0] = 145;
		kalman->state_pre->data.fl[2] = 0;
		kalman->state_pre->data.fl[3] = 0;

		CvScalar yellow = CV_RGB(255,255,0);
		CvScalar white = CV_RGB(255,255,255);
		CvScalar red = CV_RGB(255,0,0);

	while(1)
	{
		//Get frame from Camera then transform the RGB image to HSV for thresholding
		angles cordinates;
		char *data;
		char *datadepth;
		IplImage *image = cvCreateImageHeader(cvSize(640,480), 8, 3);
		IplImage *depthImage = cvCreateImageHeader(cvSize(640,480), 16, 1);
		CvScalar s;
		uint32_t timestamp;
		freenect_sync_get_video((void**)(&data), &timestamp, 0, FREENECT_VIDEO_RGB);
		freenect_sync_get_depth((void**)(&datadepth), &timestamp, 0, FREENECT_DEPTH_11BIT);
		cvSetData(depthImage, datadepth, 640*2);
		cvSetData(image, data, 640*3);
		cvCvtColor(image, image, CV_RGB2BGR);
		
		previous_t = current_t;
		current_t = clock();
		

		IplImage* hsv_frame = cvCreateImage(cvGetSize(image),8,3);
		//IplImage* threshold_frame = cvCreateImage(cvGetSize(image),8,1);
		//IplImage* threshold_frame2 = cvCreateImage(cvGetSize(image),8,1);
		//Red Detection Code
		IplImage* red = cvCreateImage(cvGetSize(image),8,1);
		IplImage* sat = cvCreateImage(cvGetSize(image),8,1);
		IplImage* car = cvCreateImage(cvGetSize(image),8,1);


		cvCvtColor(image,hsv_frame,CV_BGR2HSV);
		//grab the saturation from sv_image
		cvSplit(hsv_frame,NULL,sat,NULL,NULL);

		//split BGR image into red only
		cvSplit(image,NULL,NULL,red,NULL);

		//filter out low red and low saturation
		cvThreshold(red,red,128,255,CV_THRESH_BINARY);
		cvThreshold(sat,sat,128,255,CV_THRESH_BINARY);

		//combine images
		cvMul(red,sat,car);

		cvErode(car,car, NULL, 2);
		cvDilate(car,car,NULL, 5);

		//Filter image twice then OR the images together
		
		//cvInRangeS(hsv_frame, hsv_min , hsv_max , threshold_frame);
		//cvInRangeS(hsv_frame, hsv_min2 , hsv_max2, threshold_frame2);
		//cvOr(threshold_frame,threshold_frame2,threshold_frame);
		
		//Discard Image
		//cvReleaseImage(&threshold_frame2);

		//Smooth Image for Circle Detect
		//cvSmooth(threshold_frame,threshold_frame, CV_GAUSSIAN, 5 , 5);

		//ASSUMPTION OF ONLY ONE COLORED OBJECT ON SCREEN
		//moments are used to compare two contours 
		//a countour is a series of pixels that describe a curve or division of an object
		//can be done with a pos/neg threshold or a cvCanny

		CvMoments *moments = (CvMoments*)malloc(sizeof(CvMoments));
		cvMoments(car, moments, 1);

		double moment10 = cvGetSpatialMoment(moments,1,0);
		double moment01 = cvGetSpatialMoment(moments,0,1);
		double area = cvGetCentralMoment(moments,0,0);

		//Dividing moment10 by area gives x-cord
		//Dividing moment01 by area gives y-cord

		static int posX = 0;
		static int posY = 0;

		int lastX = posX;
		int lastY = posY;

		if( area != 0 )
		{
			posX = moment10/area;
			posY = moment01/area;
		
			int deltaX = posX - lastX;
			int deltaY = posY - lastY;
			int deltaTime = current_t - previous_t;

			printf("Position X = %d\n", posX);
			printf("Position Y = %d\n", posY);

			cvCircle(image,cvPoint(posX,posY),4,yellow,-1,4);

			if ( deltaTime != 0 && (40 < posX) && (posX < 640) && (posY < 480))
			{
				double slopeX = ((double)deltaX/deltaTime);
				double slopeY = ((double)deltaY/deltaTime);
				s=cvGet2D(depthImage,posY,posX); // get the (i,j) pixel value
				if(s.val[0] != 2047)
				{
					cordinates = setangle(posX,posY,s.val[0]);
					//printf("Depth=%f\n",(s.val[0]));
					printf("Vertical: %f, Horizontal: %f\n",cordinates.phi,cordinates.theta);
					cvCircle(image, cvPoint(slopeX*time + posX, slopeY*time + posY),4,CV_RGB(0,0,255),-1,4);
					//printf("The slope of X is %f \n",slopeX);
					//printf("The slope of y is %f \n",slopeY);
				}
			}
		}
		/*
		if ( !(posX < 0 && posX > 640) && !(posY < 0 && posY > 480) )
		{
			const CvMat* y_k = cvKalmanPredict( kalman, 0 );
		
			//Generate Measurement
			float vals[] = { posX, posY };
			memcpy( z_k->data.fl , vals, sizeof(vals));
			//cvMatMulAdd( kalman->measurement_matrix, x_k, z_k, z_k);
		
			// Yellow is observed state
			cvCircle(image,cvPoint(posX,posY),4,yellow,-1,4);
		
			// White is the predicted state via the filter
			cvCircle( image, 
				cvPoint(	y_k->data.fl[0],
							y_k->data.fl[1]),
					  4,white,-1,4);


			//Predict where the object will be at time t
			//get velocity from kalman for x and y
			// velocity * time = new pos 
			// draw 0 at new pos

			double estimatedPosX = y_k->data.fl[2] * time + y_k->data.fl[0];
			double estimatedPosY = y_k->data.fl[3] * time + y_k->data.fl[1];

			cvCircle(image, cvPoint(estimatedPosX,estimatedPosY),4,red,-1,4);

			cvKalmanCorrect( kalman, z_k);
		}
		*/
		// apply process noise
		//cvRandSetRange( &rng, 0, sqrt( kalman->process_noise_cov->data.fl[0] ) ,0);
		//cvRand ( &rng, w_k);
		//cvMatMulAdd( kalman->transition_matrix, x_k,w_k,x_k );
		
		//cvCircle(frame,cvPoint(posX,posY),4,CV_RGB(255,0,0),-1,4);

		

		//HoughCircle Detect
		/*
		//Allocate Memory for Output of HoughCircle
		CvMemStorage* storage = cvCreateMemStorage(0);

		CvSeq* results = cvHoughCircles(threshold_frame,storage,CV_HOUGH_GRADIENT, 2, threshold_frame->width , 100, 40, 20 , 200);

		for( int i = 0; i < results->total; i++)
		{
			float* p = (float*) cvGetSeqElem( results, i);
			CvPoint pt = cvPoint( cvRound( p[0] ), cvRound( p[1] ) );
			cvCircle( frame, pt, cvRound( p[2] ), CV_RGB(0,0,255) );
		}
		*/
		cvShowImage("Camera", image);
		cvShowImage("HSV" , hsv_frame);
		//cvShowImage("Threshold", threshold_frame);
		cvShowImage("Depth", GlViewColor(depthImage));
		cvShowImage("Car",car);

		// Wait for a keypress
        int c = cvWaitKey(10);
        if(c!=-1)
        {
            // If pressed, break out of the loop
            break;
        }

		//cvReleaseImage(&threshold_frame);
		cvReleaseImage(&hsv_frame);
		//free(&moments);
		cvReleaseImage(&car);
		//cvReleaseImage(&image);
		//cvReleaseImage(&depthImage);
		cvReleaseImage(&red);
		cvReleaseImage(&sat);
		
	}
}
