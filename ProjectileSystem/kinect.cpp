/*
Arizona State University
Capstone Fall 2011/Spring 2012
Team: Purple Threads - Nick Moulin, Gabe Silva, Nadim Hoque, Craig Hartmann, Anthony Russo, Duc Tran
Sponsor: Dr. Avrial Shrivastava

Description: This is the main program to control the operation of the turret system for our Computer System Engineering Capstone project
The program is to take RGB and Depth data from a kinect sensor, 
detect a moving orange object to be the target, 
calculate where the object will be at time t,
calculate the angle of rotation for the servos to shoot a dart at the object at time t,
send the commands to move the servos,
calculate if we have a high probability of hitting the target,
and send the command to fire the dart at the target.

The program is also to simulate the penalty that would be incurred if the program was run on a single core machine and used a object detection algorithm that had a larger CPU penalty

un-parallelized Cascade Classifier object detection takes on average 600ms
parallelized the Cascade Classifier can achieve a speed up of about 24X.

For our simulation a blob detection algoritm simulates the time it would take a parallelized cascade classifier to calculate
and a 600ms penalty is added if we wish to demonstrate the functionality in a un-parallelized state.

*/
#include <stdlib.h>
#include <stdio.h>
#include <cv.h>
#include <highgui.h>
#include <cmath>
#include <libfreenect.h>
#include <libfreenect_sync.h>
#include <math.h>
#include <pthread.h>
#include <iostream>
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <termios.h>
#include <usb.h>
#include <string.h>

#define DEFAULT_BAUDRATE 9600
#define DEFAULT_SERIALPORT "/dev/ttyUSB0"
#define VERBOSE if(VERBOSE_MODE)

int VERBOSE_MODE=0; //Print Extra Debug Info. Default Info is Only FPS
int LIVE_FIRE=0; //Turn on the Fireing solution algorithm
int MOVE_TURRET=1; //Allow the program to send commands to the servos

//Global variable for the turrent handle
usb_dev_handle* launcher;

//Global data for use with serial
FILE *fpSerial = NULL;   //serial port file pointer
int ucIndex;             //ucontroller index number

//Global data for kinect calculations
float gravity = 32.174; //Force of gravity in ft
float velocity = 47.5; //Initial Velocity of the Foam Dart
float pi = 3.14159265; 
float timeToTarget = 1; 
float y_calibration = 90; //Used for quick calibration of vertical angle

using namespace std;

/**Initialize serial port, return file descriptor
** @param char * port, port number for serial 
** @param int baud, baud rate for serial 
** @return FILE* as handle for serial connection
**/
FILE *serialInit(char * port, int baud)
{
	int BAUD = 0;
	int fd = -1;
	struct termios newtio;
	FILE *fp = NULL;

	//Open the serial port as a file descriptor for low level configuration
	// read/write, not controlling terminal for process,
	fd = open(port, O_RDWR | O_NOCTTY | O_NDELAY );
	if ( fd<0 )
	{
		printf("serialInit: Could not open serial device %s",port);
		return fp;
	}

	// set up new settings
	memset(&newtio, 0,sizeof(newtio));
	newtio.c_cflag =  CS8 | CLOCAL | CREAD;  //no parity, 1 stop bit

	newtio.c_iflag = IGNCR;    //ignore CR, other options off
	newtio.c_iflag |= IGNBRK;  //ignore break condition

	newtio.c_oflag = 0;        //all options off
	newtio.c_lflag = ICANON;     //process input as lines

	// activate new settings
	tcflush(fd, TCIFLUSH);
	
	//Look up appropriate baud rate constant
	switch (baud)
	{
	    case 38400:
	   	default:
		BAUD = B38400;
		break;
	    case 19200:
		BAUD  = B19200;
		break;
	    case 9600:
		BAUD  = B9600;
		break;
	    case 4800:
		BAUD  = B4800;
		break;
	    case 2400:
		BAUD  = B2400;
		break;
	    case 1800:
		BAUD  = B1800;
		break;
	    case 1200:
		BAUD  = B1200;
		break;
	}  //end of switch baud_rate
	
	if (cfsetispeed(&newtio, BAUD) < 0 || cfsetospeed(&newtio, BAUD) < 0)
	{
	    printf("seralInit: Failed to set serial baud rate: %d", baud);
	    close(fd);
	    return NULL;
	}
	
	tcsetattr(fd, TCSANOW, &newtio);
	tcflush(fd, TCIOFLUSH);

	//Open file as a standard I/O stream
	fp = fdopen(fd, "r+");
	if (!fp) {
	    printf("serialInit: Failed to open serial stream %s", port);
	    fp = NULL;
	}
	return fp;
} //end of serialInit

/**Process command message, send to uController
** @param const char* msg - will be in the format of 
** 'E (char)' or 'B (char)' to send over serial 
**/
void ucCommandCallback(const char * msg)
{
  VERBOSE printf("in CommandCall: '%s'\n", msg);
  fprintf(fpSerial, "%s", msg); //appends newline
  fflush(fpSerial);
} //ucCommandCallback

struct angles{
    float theta;
    float phi;
};

struct angles setangle(float x, float y, float distance);

//setangle calculates the vertical and horizontal rotation of the servos based off the x, y, and distance values passed in.
// X value is a pixel point on the x-axis of an openCV IplImage. 
// Y value is a pixel point on the y-axis of an openCV IplImage.
// The distance value is the depth disparity value returned by the Kinect Sensor
// For all calculations we assume the RGB camera and Turret is located at the center of each IplImage at point (320,240)
// The Origin of an IplImage is set to be in the upper left of the Image
// The Horizontal Servo is designed such that 90 degrees is the halfway point of rotation
// Far left position of turret 0 degrees
// Center position 90
// Far right position 180
//
// The vertical servo is designed such that 90 degrees is level with the kinect
// 0 degrees (point straight down)
// 90 degrees (horizontal)
// 180 degrees (point straight up)

angles setangle(float x, float y, float distance)
{
    angles reddit;
    float x_feet, d_feet,thetadeg,phideg, x_pixels, x_pixelsfoot, y_feet, y_pixels, y_pixelsfoot, quadSoln, angle1, angle2;

	//Two different equations exist to minimize the percent error for different depth values.
	
	if(distance > 950) //If Distance is greater than 9 feet
	{
		d_feet = (distance-824.8)/15.2;
	}
	else //If distance is between 1 and 9 feet
	{
	    d_feet = (0.1236*tan((distance/2842.5)+1.1863))*3.2808399;
	}

	VERBOSE printf("Distance in Feet: %f\n",d_feet);

	if (x < 320.00) //Point is on left side of image
	{
		x_pixels = 320.00 - x;
		x_pixelsfoot = -55.91836*log(d_feet)+177.49225974457;
		VERBOSE printf("Pixels per foot%f\n",x_pixelsfoot);
		x_feet=x_pixels/x_pixelsfoot;
		thetadeg = asin(x_feet/d_feet) * (180.0 / pi);
		thetadeg = 90 - thetadeg;
		reddit.theta=thetadeg;
	}
	else if (x > 320.00) //Point is on the right side of image
	{
		x_pixels = x-320;
		x_pixelsfoot = -55.91836*log(d_feet)+177.49225974457;
		x_feet=x_pixels/x_pixelsfoot;
		thetadeg = asin(x_feet/d_feet) * (180.0 / pi);
		thetadeg = 90 + thetadeg;
		reddit.theta= thetadeg;
	}
	else if (x == 320.0)
	{
		x_feet = x - 320.0;
		thetadeg= 0.0;	
		reddit.theta=90.0;
	}

	if (y < 240.00) //upper half of image
	{
		y_pixels = 240 - y;
		y_pixelsfoot = -55.91836*log(d_feet)+177.49225974457;
		y_feet=y_pixels/y_pixelsfoot;
	}
	else if (y > 240.00) //lower half of image
	{
		y_pixels = y - 240;
		y_pixelsfoot = -55.91836*log(d_feet)+177.49225974457;
		y_feet=-1*(y_pixels/y_pixelsfoot);
	}
	else
	{	
		y_feet = 0;
	}

	quadSoln = sqrt(pow(velocity,4) - (gravity * ( (gravity * pow(d_feet,2)) + ( 2 * y_feet * pow(velocity,2) ) ) ) );
	
	//Two possible angles are returned from the quadratic equation. We choose the smaller of the two.
	if( quadSoln == quadSoln)
	{
		angle1 = atan((pow(velocity,2) + quadSoln)/(gravity*d_feet));	
		angle2 = atan((pow(velocity,2) - quadSoln)/(gravity*d_feet));
		angle1 = angle1 * (180.0/pi);
		angle2 = angle2 * (180.0/pi);
		angle2 = angle2;
		VERBOSE printf("y_pixels: %f y_feet: %f\n",y_pixels,y_feet);
		VERBOSE printf("Angle1: %f Angle:2 %f\n",angle1,angle2);
	}
	else
	{
		printf("Not a Number");
		phideg = 0;
	}
	phideg = angle2 + y_calibration;
	reddit.phi= phideg;

	timeToTarget = d_feet/(velocity * cos(phideg)); //Determine how long it will take the dart to travel to the target

	return reddit;
}

// Convert the depth desparity matrix held in the depth image into a very basic image
// the closer the object the closer to the color red it will be.
IplImage *GlViewColor(IplImage *depth)
{
	static IplImage *image = 0;
	if (!image) image = cvCreateImage(cvSize(640,480), 8, 3);
	unsigned char *depth_mid = (unsigned char*)(image->imageData);
	int i;

	for (i = 0; i < 640*480; i++) {
		int lb = ((short *)depth->imageData)[i] % 256;
	int ub = ((short *)depth->imageData)[i] / 256;
		switch (ub) {
			case 0:
				depth_mid[3*i+2] = 255;
				depth_mid[3*i+1] = 255-lb;
				depth_mid[3*i+0] = 255-lb;
				break;
			case 1:
				depth_mid[3*i+2] = 255;
				depth_mid[3*i+1] = lb;
				depth_mid[3*i+0] = 0;
				break;
			case 2:
				depth_mid[3*i+2] = 255-lb;
				depth_mid[3*i+1] = 255;
				depth_mid[3*i+0] = 0;
				break;
			case 3:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 255;
				depth_mid[3*i+0] = lb;
				break;
			case 4:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 255-lb;
				depth_mid[3*i+0] = 255;
				break;
			case 5:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 0;
				depth_mid[3*i+0] = 255-lb;
				break;
			default:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 0;
				depth_mid[3*i+0] = 0;
				break;
		}
	}
	return image;
}



int main (int argc, char **argv)
{
	setbuf(stdout, NULL);
	struct usb_bus *busses;
	
	usb_init();
	usb_find_busses();
	usb_find_devices();	
	
	char msg[8];
	//reset
	msg[0] = 0x0;
	msg[1] = 0x0;
	msg[2] = 0x0;
	msg[3] = 0x0;
	msg[4] = 0x0;
	msg[5] = 0x0;
	msg[6] = 0x0;
	msg[7] = 0x0;

	char resp[8];
	resp[0] = 0x01;
	resp[1] = 0x0;
	resp[2] = 0x0;
	resp[3] = 0x0;
	resp[4] = 0x0;
	resp[5] = 0x0;
	resp[6] = 0x0;
	resp[7] = 0x0;

	int rdyToFire = 0; //Track the number of positive Locks from the firing solution
	double timer = 2; //The time it takes the turret to fire and reload. The fastest possible turnaround time
	int ammo = 4; //Number of Darts the turret has
	int SINGLECORE = 0;
	    
    busses = usb_get_busses();
        
   	struct usb_bus *bus;

	//loop through the busses to find the correct device
    for (bus = busses; bus; bus = bus->next) 
	{
    	struct usb_device *dev;
		printf("in busses loop");
		//loop through the devices on each bus 
    	for (dev = bus->devices; dev; dev = dev->next) 
		{
			printf("id of Vendor: %d\n",dev->descriptor.idVendor);
    		// Check if this device is the turret
			// if a new turret is used, the Vendor ID may change
			// if that is the case, then change this appropriately
    		if (dev->descriptor.idVendor == 8483)
			{
				launcher = usb_open(dev);
				printf("in vendorId loop\n\n");
				
				// if being claimed by usb_storage, need to release so we can use
				int retval; 
				char dname[32] = {0}; 
				retval = usb_get_driver_np(launcher, 0, dname, 31); 
				printf("retval : %d\n", retval);

				if (!retval) {
				      usb_detach_kernel_driver_np(launcher, 0); 
				}

				//claim device 
				int claimed = usb_claim_interface(launcher, 0);
				printf("claimed : %d\n", claimed);
				char a = getchar();
				int input = -1;

				if (claimed == 0)
				{
					printf("USB Turret Claimed and ready to fire");
				}
				else 
				{
					printf("USB Turrent ERROR in claiming, Turret WILL NOT fire");
				}
			}
		}
	}	
		
	CvSize size = cvSize(640,480); //Specifiy the Size of the Image
	freenect_sync_set_tilt_degs(0, 0); //Set the Kinect to the default angle

	//Setup Windows to view Images
	cvNamedWindow("Camera", CV_WINDOW_AUTOSIZE); 
	cvNamedWindow("Car", CV_WINDOW_AUTOSIZE);
	//cvNamedWindow("Depth", CV_WINDOW_AUTOSIZE);

	clock_t previous_t = clock() , current_t = clock(); //To track the time between each iteration of the while loop

  	char port[20];    //port name
  	int baud;     //baud rate 

  	char topicSubscribe[20];
  	char topicPublish[20];
	int err;

/**Open and initialize the serial port to the uController
** will be looking for ttyUSB0 by default 
**
**/
	if (argc > 1) 
	{
	    if(sscanf(argv[1],"%d", &ucIndex)==1) {
	      sprintf(topicSubscribe, "uc%dCommand",ucIndex);
	      sprintf(topicPublish, "uc%dResponse",ucIndex);
	    }
	    else {
	      return 1;
	    }
	}
	else 
	{
	    strcpy(topicSubscribe, "uc0Command");
	    strcpy(topicPublish, "uc0Response");
	}

	strcpy(port, DEFAULT_SERIALPORT);
	if (argc > 2)
	     strcpy(port, argv[2]);

	baud = DEFAULT_BAUDRATE;
	if (argc > 3) 
	{
	    if(sscanf(argv[3],"%d", &baud)!=1) {
	      printf("ucontroller baud rate parameter invalid\n");
	      return 1;
	    }
	}

	printf("connection initializing (%s) at %d baud\n", port, baud);
	fpSerial = serialInit(port, baud);
	if (!fpSerial )
	{
	    printf("unable to create a new serial port\n");
	    //return 1;
	}
	else
	{
	    printf("serial connection successful\n");
	}

//End of SerialPortInit

	getchar();

	CvKalman* kalman = cvCreateKalman(4,2,0);

	//Create Initial transitionMatrix
	//CV_32FC1 means 32 floating point type

	CvMat* x_k = cvCreateMat(4,1,CV_32FC1);
	float initVals[] = { 0, 0, 0, 0};
	memcpy ( x_k->data.fl, initVals, sizeof(initVals));
	//Initial process noise
	
	CvMat* w_k = cvCreateMat( 4, 1, CV_32FC1 );
	//measurements two parameters x and y
	CvMat* z_k = cvCreateMat(2,1,CV_32FC1);
	cvZero(z_k);

	//Transition Matrix F
	const float F[] = { 1, 0, 10, 0,
						0, 1, 0, 10,
						0, 0, 1, 0,
						0, 0, 0, 1};

	memcpy ( kalman->transition_matrix->data.fl, F, sizeof(F));

	//Measurement matrix set to identity matrix
	//process noise set to low value
	//measurement noise set to value
	//error_conv set to identity

	cvSetIdentity( kalman->measurement_matrix, cvRealScalar(1) );
	cvSetIdentity( kalman->process_noise_cov, cvRealScalar(1e-5) );
	cvSetIdentity( kalman->measurement_noise_cov, cvRealScalar(1e-1) );
	cvSetIdentity( kalman->error_cov_post, cvRealScalar(1) );

	//initial state needs to be set
	kalman->state_pre->data.fl[0] = 95;
	kalman->state_pre->data.fl[0] = 145;
	kalman->state_pre->data.fl[2] = 0;
	kalman->state_pre->data.fl[3] = 0;

	CvScalar yellow = CV_RGB(255,255,0);
	CvScalar white = CV_RGB(255,255,255);
	CvScalar redColor = CV_RGB(255,0,0);

	

	while(1)
	{
		string horizontal = "B ";
		string vertical = "E ";

		//Get RGB and Depth Data from Kinect
		angles cordinates;
		char *data;
		char *datadepth;
		IplImage *image = cvCreateImageHeader(cvSize(640,480), 8, 3);
		IplImage *depthImage = cvCreateImageHeader(cvSize(640,480), 16, 1);

		CvScalar s;
		uint32_t timestamp;
		freenect_sync_get_video((void**)(&data), &timestamp, 0, FREENECT_VIDEO_RGB);
		freenect_sync_get_depth((void**)(&datadepth), &timestamp, 0, FREENECT_DEPTH_11BIT);
		cvSetData(depthImage, datadepth, 640*2);
		cvSetData(image, data, 640*3);
		cvCvtColor(image, image, CV_RGB2BGR);
		
		//Update the timers
		previous_t = current_t;
		current_t = clock();
		
		//Calculate how much time the last iteration took
		double deltaTime = current_t - previous_t;
		deltaTime = deltaTime/CLOCKS_PER_SEC;
		VERBOSE printf("DeltaTime %.21f\n",deltaTime);

		//Calculate and report the FPS, Each iteration processes one frame.
		double fps = 1/deltaTime;
		printf("FPS: %.21f\n",fps);
		
		//Update the refire timer for the turret.
		//The turret cannot fire until the refire timer is 0
		if (timer > 0)
		{
			timer = timer - deltaTime;
		}
		else
		{
			timer = 0;
		}
		VERBOSE printf("Timer %.21f\n",timer);
		
		//Set up frames to object detect
		IplImage* hsv_frame = cvCreateImage(cvGetSize(image),8,3);
		IplImage* red = cvCreateImage(cvGetSize(image),8,1);
		IplImage* sat = cvCreateImage(cvGetSize(image),8,1);
		IplImage* car = cvCreateImage(cvGetSize(image),8,1);

		//Convert the RGB image to a HSV image so we can split out the saturation values
		cvCvtColor(image,hsv_frame,CV_BGR2HSV);
		//grab the saturation from sv_image
		cvSplit(hsv_frame,NULL,sat,NULL,NULL);

		//split BGR image into red only
		cvSplit(image,NULL,NULL,red,NULL);

		//filter out low red and low saturation
		cvThreshold(red,red,128,255,CV_THRESH_BINARY);
		cvThreshold(sat,sat,128,255,CV_THRESH_BINARY);

		//combine images
		cvMul(red,sat,car);

		//Erode out very small positive detections
		cvErode(car,car, NULL, 2);
		//Increase the size of the positive detections still left
		cvDilate(car,car,NULL, 5);

		//ASSUMPTION OF ONLY ONE COLORED OBJECT ON SCREEN
		//moments are used to compare two contours 
		//a countour is a series of pixels that describe a curve or division of an object
		//can be done with a pos/neg threshold or a cvCanny

		CvMoments *moments = (CvMoments*)malloc(sizeof(CvMoments));
		cvMoments(car, moments, 1);

		double moment10 = cvGetSpatialMoment(moments,1,0);
		double moment01 = cvGetSpatialMoment(moments,0,1);
		double area = cvGetCentralMoment(moments,0,0);

		//Dividing moment10 by area gives x-cord
		//Dividing moment01 by area gives y-cord

		static int posX = 0;
		static int posY = 0;

		int lastX = posX;
		int lastY = posY;

		//Protect against div-by-zero
		if( area != 0 )
		{
			posX = moment10/area;
			posY = moment01/area;
		
			//find the difference between the last frame and current frame
			int deltaX = posX - lastX;
			int deltaY = posY - lastY;
			

			VERBOSE printf("Position X = %d\n", posX);
			VERBOSE printf("Position Y = %d\n", posY);

			//PosX and PosY sometimes report very huge and very small values, filter them out
			if ( deltaTime != 0 && (40 < posX) && (posX < 640) && (posY < 480))
			{
				//Calculate the slow at this exact moment in time based off of the difference between this image and the previous image
				double slopeX = ((double)deltaX/deltaTime);
				double slopeY = ((double)deltaY/deltaTime);
				s=cvGet2D(depthImage,posY,posX); // get the (i,j) pixel value for depth
				if(s.val[0] != 2047) //if depth value is not recorded a value of 2047 is stored in the matrix
				{
					const CvMat* y_k = cvKalmanPredict( kalman, 0 ); //get the current prediction of the next frame
				
					//Generate Measurement
					float vals[] = { posX, posY };
					memcpy( z_k->data.fl , vals, sizeof(vals));

					//Yellow Circle is the current position
					cvCircle(image,cvPoint(posX,posY),4,yellow,-1,4);
					// White is the predicted position via the filter
					cvCircle( image, 
						cvPoint(	y_k->data.fl[0],
									y_k->data.fl[1]),
								  4,white,-1,4);

					//Predict where the object will be at time t
					//get velocity from kalman for x and y
					// velocity * time = new pos 
					// draw 0 at new pos

					double estimatedPosX = y_k->data.fl[2] * timeToTarget + y_k->data.fl[0];
					double estimatedPosY = y_k->data.fl[3] * timeToTarget + y_k->data.fl[1];

					//Red Circle is where we predict the object will be at time t
					cvCircle(image, cvPoint(estimatedPosX,estimatedPosY),4,redColor,-1,4);

					//add data from the current frame to the kalman
					cvKalmanCorrect( kalman, z_k);

					//Calculate the percent error between where we think the object will be and where it currently is
					//This will filter out extremely fast moving objects and quick changes in direction
					double percentErrorX = (abs(y_k->data.fl[0]-posX))/(double)(posX);
					double percentErrorY = (abs(y_k->data.fl[1]-posY))/(double)(posY);

					VERBOSE printf("PercentErrorX: %f PercentErrorY: %f\n",percentErrorX,percentErrorY);
					
					//Get the horizontal and vertical angle based off of where we think the object will be at time t
					cordinates = setangle(estimatedPosX,estimatedPosY,s.val[0]);

					VERBOSE printf("Depth=%f\n",(s.val[0]));
					VERBOSE printf("Vertical: %f, Horizontal: %f\n",cordinates.phi,cordinates.theta);
					VERBOSE printf("The slope of X is %f \n",slopeX);
					VERBOSE printf("The slope of y is %f \n",slopeY);
					
					//Move the turret to our predicted angles
					if(MOVE_TURRET)
					{
						//change the float to int then corresponding ascii char for serial 	
						int temp = (int)cordinates.theta;
						char tempc = char(temp);		
						horizontal += tempc; 
						VERBOSE printf("-----sHorizontal: %s,    ", horizontal.c_str());	
						//pass the command over the function handling communication with serial
						ucCommandCallback(horizontal.c_str());
	
						int vtemp = (int)cordinates.phi;
						char tempv = char(vtemp);
						vertical += tempv; 
						VERBOSE printf("-----sVertical: %s,    ", vertical.c_str());
						ucCommandCallback(vertical.c_str());
					}
					
					//Only if the percent error is under 5% do we consider the kalman to be predicting correctly and we "lock" onto the target
					//If we have one "no-lock" we must restart the locking procedure
					if( (percentErrorX < .05) && (percentErrorY < .05) )
					{
						rdyToFire++;
					}
					else
					{
						rdyToFire = 0;
					}

					if(LIVE_FIRE)
					{
						//If we have 10 positive locks, the re-fire timer is at zero, and there is ammo in the turret
						//Then send the command to fire
						if(rdyToFire >= 10 && timer <= 0 && ammo > 0)
						{
							//Send the turret the command to fire
							//usb_control_msg is the method that sends the command
							printf("Fire Turret\n");
							msg[0] = 0x02;
							msg[1] = 0x10;
							int j = usb_control_msg(launcher, 0x221, 0x09, 0x0200, 0, msg, 8, 1000);
							//Decrease the amount of ammo
							ammo = ammo - 1;
							//Reset the refire timer
							timer = 2;
						}
					}
				}
			}

		}
		
		//Show the image to the screen 
		cvShowImage("Camera", image);
		//cvShowImage("Depth", GlViewColor(depthImage));
		cvShowImage("Car",car);

		// Wait for a keypress for 10us
        int c = cvWaitKey(10);
		// -1 is returned if no key is pressed
        if(c!=-1)
        {
			//Manual command to fire the turret without the fireing solution
			if(c == 102)//ASCII 'f'
			{
				printf("Fire turret\n");
				msg[0] = 0x02;
				msg[1] = 0x10;
				int j = usb_control_msg(launcher, 0x221, 0x09, 0x0200, 0, msg, 8, 1000);

			}
			//Manual reload command
			else if( c == 114 )//ASCII 'r'
			{
				printf("Reloaded\n");
				ammo = 4;
			}
			//Set the program to multithreaded mode
			else if( c == 109 )// ASCII 'm'
			{
				printf("Multithread Mode\n");
				SINGLECORE = 0;
			}
			//Set the program to single core mode
			else if( c == 115 )// ASCII 's'
			{
				printf("Single Core Mode\n");
				SINGLECORE = 1;
			}
			//If any other key is pressed end the program
			else
			{
            		// If pressed, break out of the loop
            		break;
			}
        }

		//Protect against memory leaks

		//cvReleaseImage(&threshold_frame);
		cvReleaseImage(&hsv_frame);
		//free(&moments);
		cvReleaseImage(&car);
		//cvReleaseImage(&image);
		//cvReleaseImage(&depthImage);
		cvReleaseImage(&red);
		cvReleaseImage(&sat);

		//If emulating a single core system there is a 600ms penalty for object detection
		if(SINGLECORE)
		{
			usleep(600000);
		}
	}
	
	//release interfaces
	usb_release_interface(launcher, 0);
	usb_close(launcher);
	printf("usb connection closed");
	fclose(fpSerial);
	printf("serial connection closed");
	
	return 0;
}
