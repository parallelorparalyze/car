﻿using System;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;

namespace FireControl
{
    public class GunTurret : IDisposable
    {
        private bool _disposed;
        private Servo _horizontalServo;
        private Servo _verticalServo;

        public GunTurret(Cpu.Pin pin1, Cpu.Pin pin2)
        {
            _horizontalServo = new Servo(pin1);
            _verticalServo = new Servo(pin2);
        }

        public GunTurret()
        {
            _horizontalServo = new Servo(Pins.GPIO_PIN_D9);
            _verticalServo = new Servo(Pins.GPIO_PIN_D10);
        }

        public double Elevation
        {
            get { return _verticalServo.Degree; }
            set { _verticalServo.Degree = value; }
        }
        public double Bearing
        {
            get { return _horizontalServo.Degree; }
            set { _horizontalServo.Degree = value; }
        }

        public void Fire()
        {
            //throw new NotImplementedException();
        }

        public void Fire(double bearing, double elevation)
        {
            Bearing = bearing;
            Elevation = elevation;
            Fire();
        }

        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            // If you need thread safety, use a lock around these 
            // operations, as well as in your methods that use the resource.
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_horizontalServo != null)
                        _horizontalServo.Dispose();
                    if (_verticalServo != null)
                        _verticalServo.Dispose();
                }

                // Indicate that the instance has been disposed.
                _horizontalServo = null;
                _verticalServo = null;
                _disposed = true;
            }
        }
    }
}
