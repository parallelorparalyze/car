﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;

namespace FireControl
{
    public class Program
    {
        public static void Main()
        {
            //var servo = new Servo(Pins.GPIO_PIN_D9);

            //while (true)
            //{
            //    for (var i = 0; i <= 180; i++)
            //    {
            //        servo.Degree = i;
            //        Thread.Sleep(10);
            //    }

            //    for (var i = 180; i >= 0; i--)
            //    {
            //        servo.Degree = i;
            //        Thread.Sleep(10);
            //    }
            //}
            var gunTurret = new GunTurret();
            while (true)
            {
                gunTurret.Fire(0, 0);
                Thread.Sleep(1000);
                gunTurret.Fire(0, 130);
                Thread.Sleep(1000);
                gunTurret.Fire(145, 0);
                Thread.Sleep(1000);
                gunTurret.Fire(0, 0);
                Thread.Sleep(1000);
                gunTurret.Fire(123, 123);
                Thread.Sleep(1000);
            }
        }
    }
}
