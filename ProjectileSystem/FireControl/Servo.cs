/*
 * Servo NETMF Driver
 *      Coded by Chris Seto August 2010
 *      <chris@chrisseto.com> 
 *      
 * Use this code for whatveer you want. Modify it, redistribute it, I don't care.
 * I do ask that you please keep this header intact, however.
 * If you modfy the driver, please include your contribution below:
 * 
 * Chris Seto: Inital release (1.0)
 * Chris Seto: Netduino port (1.0 -> Netduino branch)
 * Chris Seto: bool pin state fix (1.1 -> Netduino branch)
 * 
 * 
 * */

using System;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;

namespace FireControl
{
    public class Servo : IDisposable
    {
        private double _degree;
        /// <summary>
        /// PWM handle
        /// </summary>
        private readonly PWM _servo;

        /// <summary>
        /// Timings range
        /// </summary>
        private readonly int[] _range = new int[2];

        /// <summary>
        /// Set Servo inversion
        /// </summary>
        private bool _inverted = false;
        public bool Inverted
        {
            get
            {
                return _inverted;
            }
            set { _inverted = value; }
        }

        /// <summary>
        /// Create the PWM pin, set it low and configure timings
        /// </summary>
        /// <param name="pin"></param>
        public Servo(Cpu.Pin pin)
        {
            // Init the PWM pin
            _servo = new PWM((Cpu.Pin)pin);

            _servo.SetDutyCycle(0);

            // Typical settings
            _range[0] = 1000;
            _range[1] = 2000;
        }

        public void Dispose()
        {
            Disengage();
            _servo.Dispose();
        }

        /// <summary>
        /// Allow the user to set custom timings
        /// </summary>
        /// <param name="fullLeft"></param>
        /// <param name="fullRight"></param>
        public void SetRange(int fullLeft, int fullRight)
        {
            _range[1] = fullLeft;
            _range[0] = fullRight;
        }

        /// <summary>
        /// Disengage the servo. 
        /// The Servo motor will stop trying to maintain an angle
        /// </summary>
        public void Disengage()
        {
            // See what the Netduino team say about this... 
            _servo.SetDutyCycle(0);
        }

        /// <summary>
        /// Set the Servo degree
        /// </summary>
        public double Degree
        {
            get { return _degree; }
            set
            {
                _degree = value;
                // Range checks
                if (_degree > 180)
                    _degree = 180;

                if (_degree < 0)
                    _degree = 0;

                // Are we _inverted?
                if (_inverted)
                    _degree = 180 - _degree;

                // Set the pulse
                _servo.SetPulse(20000, (uint)Map((long)_degree, 0, 180, _range[0], _range[1]));
            }
        }

        /// <summary>
        /// Used internally to Map a value of one scale to another
        /// </summary>
        /// <param name="x"></param>
        /// <param name="in_min"></param>
        /// <param name="in_max"></param>
        /// <param name="out_min"></param>
        /// <param name="out_max"></param>
        /// <returns></returns>
        private long Map(long x, long in_min, long in_max, long out_min, long out_max)
        {
            return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }
    }

}

