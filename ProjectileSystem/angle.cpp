#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "/usr/include/opencv/cv.h"
#include "/usr/include/opencv/highgui.h"

float y_elevation=3.00;
float gravity = 32.174;
float velocity = 30;
float pi = 3.14159265;
using namespace std;

struct angles
{
	float theta;
	float phi;
};

struct angles setangle(float x, float y, float distance);

int main(int argc, char* argv[])
{
	angles test;
	float x, y, d;
	cout <<"enter x y and d"<<"\n";
	cin>>x>>y>>d;
	cout<<"x is "<<x<<" y is "<<y<<" d is "<<d<<"\n";
	test = setangle(x,y,d);
	//printf("theta %d and phi %d \n",&test.theta,&test.phi);
	cout<<"phi is "<<test.phi<<" theta is "<<test.theta<<"\n";
	return 0;
}

angles setangle(float x, float y, float distance)
{
	angles reddit;
	float x_feet, d_feet,t,p,thetadeg,phideg, x_pixels, x_pixelsfoot;
	float top1,bottom1,top2,bottom2,top3,bottom3,ubbersqrt;	
	//d_feet = exp((distance-542.4451052979)/210.594467926)*1.174213646;
	//d_feet = (distance-824.8)/15.2;
	d_feet = (0.1236*tan((distance/2842.5)+1.1863))*3.2808399;
	//d_feet=(0.490592*d_feet-2.751692);
	cout<<"d_feet is "<<d_feet<<"\n";
	if (x < 320.00)
	{
		//cout<<"x is again "<<x<<"\n";
		//d_feet = 3.0;
		x_pixels = 320.00 - x;
		cout<<"x is on the left side and after change is "<<x_pixels<<"\n";
		x_pixelsfoot = -55.91836*log(d_feet)+177.49225974457;
		printf("x_pixelsfoot in feet %f\n",x_pixelsfoot);
		x_feet=x_pixels/x_pixelsfoot;
		thetadeg = asin(x_feet/d_feet) * (180.0 / pi);
		thetadeg = 90 - thetadeg;
		printf("theta deg is %f\n",thetadeg);
		reddit.theta=thetadeg;
	}
	else if (x > 320.00)
	{
		x_pixels = x-320;
		cout<<"x is on right side and after change is "<<x_pixels<<"\n";
		x_pixelsfoot = -55.91836*log(d_feet)+177.49225974457;
		printf("x_pixelsfoot in feet %f\n",x_pixelsfoot);
		x_feet=x_pixels/x_pixelsfoot;
		thetadeg = asin(x_feet/d_feet) * (180.0 / pi);
		printf("theta deg is %f\n",thetadeg);
		reddit.theta= thetadeg;
	}
	else if (x == 320.0)
	{
		x_feet = x - 320.0;
		thetadeg= 0.0;	
		reddit.theta=0.0;
	}
	//x_feet = exp((x_feet-46.0508955701)/-12.108254467);
	//thetadeg = acos(x_feet/d_feet) * (180.0 / pi);
	//reddit.theta= 90.0 -thetadeg;
	// not now reddit.phi=acos(sqrt(((pow(distance,2)*g*y_elevation)/()));
	cout<<"d_feet is "<<d_feet<<"\n";
	cout<<"gravity is "<<gravity<<"\n";
	cout<<"velocity squared is "<<pow(velocity,2)<<"\n";
	cout<<"d_feet*gravity= "<<d_feet*gravity<<"\n";
	cout<<"d_feet*gravity / pow(velocity,2) = "<<(d_feet*gravity)/pow(velocity,2)<<"\n";
	cout<<"a sin of crap is "<<asin((d_feet*gravity)/(velocity*velocity))<<"\n";
	printf("a sin of crap * 1 / 2 = %f \n",((1.0/2.0)*asin((d_feet*gravity)/pow(velocity,2))));
	top1=pow(d_feet,2)*gravity*pow(velocity,2)*y_elevation;
	bottom1=(pow(d_feet,2)*pow(velocity,4))+(pow(velocity,4)*pow(y_elevation,2));
	top2=pow(d_feet,2)*pow(velocity,4);
	bottom2=(pow(d_feet,2)*pow(velocity,4))+(pow(velocity,4)*pow(y_elevation,2));
	top3=sqrt((-pow(d_feet,4)*pow(velocity,4))*((pow(d_feet,2)*pow(gravity,2))-(2*gravity*pow(velocity,2)*y_elevation)-pow(velocity,4)));
	bottom3=(pow(d_feet,2)*pow(velocity,4))+(pow(velocity,4)*pow(y_elevation,2));
	ubbersqrt=sqrt((top1/bottom1)+(top2/bottom2)+(top3/bottom3));
	phideg = acos(ubbersqrt/sqrt(2))*(180.0/pi);
	//phideg = (phideg *(180.0/pi);
	printf("phi in degrees is: %f\n",phideg);
	
	reddit.phi = phideg;
	return reddit;
	
}
