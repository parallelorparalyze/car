#include <Servo.h> 

const int MIN_BEARING = 10;
const int  MAX_BEARING = 170;
const int  MIN_ELEVATION = 30;
const int  MAX_ELEVATION = 150;

Servo servoBearing;
Servo servoElevation;

volatile int bearing = 90;
volatile int elevation = 90;

volatile int previousBytes[2] = { 0 };  // for state machine

void fireControlCommandError(int, int);

void setup() 
{
  servoBearing.attach(2);
  servoElevation.attach(3);
  Serial.begin(9600);
} 

void loop() 
{ 
  int incomingByte;
  if (Serial.available())
  {
    incomingByte = Serial.read();
    //Serial.println("Previous Bytes:"); 
    //Serial.println(previousBytes[0],DEC);
    //Serial.println(previousBytes[1],DEC);
    //Serial.println("Incoming byte:"); 
    //Serial.println(incomingByte,DEC);
    if (previousBytes[0] == 66 && previousBytes[1] == 32)  // B and space  
    {
      if(incomingByte >= 10 && incomingByte <= 170)
      {
        bearing = incomingByte;
        Serial.print("Bearing Set: ");
        Serial.println(bearing,DEC);
        
      }
      else
      {
        Serial.print("Bearing range error: ");
        Serial.println(bearing,DEC);
        fireControlCommandError(bearing, elevation);
      }
    }
    if (previousBytes[0] == 69 && previousBytes[1] == 32) // E and space   
    {
      if(incomingByte >= MIN_ELEVATION && incomingByte <= MAX_ELEVATION)
      {
        elevation = incomingByte;
        Serial.print("Elevation Set: ");
        Serial.println(elevation,DEC);
      }
      else
      {
        Serial.print("Elevation range error: ");
        Serial.println(elevation,DEC);
        fireControlCommandError(bearing, elevation);
      }
    }
    
                    
    previousBytes[0] = previousBytes[1]; 
    previousBytes[1] = incomingByte;
  }
    servoBearing.write(bearing);
    servoElevation.write(elevation);
}

void fireControlCommandError(int bearing, int elevation)
{
        Serial.print("Fire Control error using bearing ");
        Serial.print(bearing,DEC);
        Serial.print(" degrees and ");
        Serial.print(elevation,DEC);
        Serial.println(" degrees elevation."); 
}
