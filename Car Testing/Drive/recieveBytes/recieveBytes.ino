//Realeased under really Creative Commons! 
//This just a basic demo code...
//Reads and pulses the 8 channels... 
//By Jordi Munoz
#include <avr/interrupt.h>

volatile unsigned int Start_Pulse =0;
volatile unsigned int Stop_Pulse =0;
volatile unsigned int Pulse_Width =0;

volatile int Test=0;
volatile int Test2=0;
volatile int Temp=0;
volatile int Counter=0;
volatile byte PPM_Counter=0;
volatile int PWM_RAW[8] = {2400,2400,2400,2400,2400,2400,2400,2400};
int const MAX_INST = 30;
int stopAll = 0;
int trottleSpeed = 1375;
int SteeringAngle = 1375;
int upDown = 1;
int randArray[4] = {200, 500, 300, 700};
int loopCount1 = 0;
int loopCount2 = 0;
int directionFlag = 0;
int i;
int inByte = 0;
int bit0;
int bit1;
int bit2;
int bit3;
int bit4;
int bit5;
int bit6;
int bit7;
int nbit0 = 0;
int nbit1to7 = 128;

long timer=0;
long timer2=0;
int instCMLeft = 0;
int const TIME_PER_CM = 10;
int instructions[MAX_INST][2] = {{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
                                 {0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
                                 {0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0}};
int instIter = 0;
int running = 0;

void resetOutput() {
  OutputCh(0, 1375); // Steering
  OutputCh(1, 1375); // Throttle
}

void printBuffer()
{
  int j;
  Serial.write("Buffer: [");
  for(j = 0; j < MAX_INST; j++) {
    Serial.write("{");
    Serial.write((int)instructions[j][0] + 60);
    Serial.write(",");
    Serial.write((int)instructions[j][1] + 60);
    Serial.write("}");
  }
  Serial.write("]\n");
  //delay(10);
}

void setup()
{
  Init_PWM1(); //OUT2&3 
  Init_PWM3(); //OUT6&7
  Init_PWM5(); //OUT0&1
  Init_PPM_PWM4(); //OUT4&5
  Serial.begin(57600);
  resetOutput();
  clearInstructions();
  delay(2500);
  Serial.write("Buffer initially empty\n");

}

int instructionEmpty(byte inst1,byte inst2)
{
  if(inst1 == 0 && inst2 == 0) {
    return 1;
  } else {
    return 0;
  }
}

void clearInstructions()
{
  int j = 0;
  instIter = -1;
  instCMLeft = 0;
  running = 0;
  for(j = 0; j < MAX_INST; j++) {
    instructions[j][0] = 0;
    instructions[j][1] = 0;
    //Serial.write("CLEAR");
    //Serial.write(j+60);
  }
  delay(10);
}

void performInstruction()
{
  int forward,turning,left;
  if(running == 0) {
    return;
  }
  
  if(instCMLeft <= 0) {
    Serial.print("\n\n Processing New Instruction: ");

    instIter++;
    // if it loops past array or next inst completely empty
    // important to check second byte, where bit0 is 1 always
    if(instIter >= MAX_INST || 
      instructionEmpty(
        instructions[instIter][0],instructions[instIter][1]))
    {
      resetOutput();
      clearInstructions();
      // possibly use codes to request more instructions?
      return;
    }
    forward = instructions[instIter][0] & 0x10;
    turning = instructions[instIter][0] & 0x20;
    left = instructions[instIter][0] & 0x40;
    
    if(forward > 0){
      Serial.print("\n Forward! ");
      OutputCh(1, 1475); // Throttle Forward Slow
    } else {
      Serial.print("\n Stop! ");
      OutputCh(1, 1375); // Throttle Stopped
    }
    
    if(turning > 0){
      if(left > 0){ 
        Serial.print("\n Turning Left! ");
        OutputCh(0, 875); // Steering all the way left.
      } else {
        Serial.print("\n Turning Right! ");
        OutputCh(0, 1750); // Steering all the way right.
      }
    } else {
      Serial.print("\n Going Straight! ");
      OutputCh(0,1375); // no turning
    }
    
    // dont bother masking bit 0, should always be 1
    // this gets decremented immediately to 0 below
    instCMLeft = instructions[instIter][1];
    Serial.print("\n Distance (CM): ");
    Serial.print(instCMLeft - 1);
  }
  
  instCMLeft--;
  delay(TIME_PER_CM);
}

void addInstruction(byte inByte1,byte inByte2)
{
  int j;
  running = 1;
  Serial.print("\n\n Adding Instruction: {");
  Serial.print((int)inByte1);
  Serial.write(",");
  Serial.print((int)inByte2);
  Serial.write("}");
  if(inByte1 & 0x08) {
    Serial.print("Override! Clearing Queue!");
    clearInstructions();
    resetOutput();
  }
  
  for(j = 0; j < MAX_INST; j++) {
    
    // find first empty instruction that is not the last
    // and insert this instruction next in the array
    if(instructionEmpty(instructions[j][0],instructions[j][1]))
    {
      instructions[j][0] = inByte1;
      instructions[j][1] = inByte2;
      //printBuffer();
      return;
    }
  }
  // should wait for instructions to be cleared
  Serial.print(" Buffer Full! ");
}

void loop()
{
    /* Throttling Range (1000-2000) () */
    /* Steering Range (1000-2000) () */
    
    // If statment to check if arduino or user is in control. Am I correc?
    if(stopAll > 0){
       // Control has been given back to the user, route the ports.
       
       OutputCh(0, InputCh(0)); // Steering
       OutputCh(1, InputCh(1)); // Throttle
       
    } else {
      byte inByte1,inByte2;
      inByte1 = 0;
      inByte2 = 1;
      if (!(Serial.available() < 1)) {
        inByte1 = Serial.read();
        
        while (Serial.available() < 1){
          delay(10);
        }
        inByte2 = Serial.read();
        addInstruction(inByte1,inByte2);
      }
      performInstruction();
      
      // Check if steering input is recieved from controller, if so break loop and give control back to the user.
      if((InputCh(0) > 1525) || (InputCh(0) < 1175)){
        stopAll = 99; 
      }
    }
    delay(5);
}



