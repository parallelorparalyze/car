//Realeased under really Creative Commons! 
//This just a basic demo code...
//Reads and pulses the 8 channels... 
//By Jordi Munoz
#include <avr/interrupt.h>

volatile unsigned int Start_Pulse =0;
volatile unsigned int Stop_Pulse =0;
volatile unsigned int Pulse_Width =0;

volatile int Test=0;
volatile int Test2=0;
volatile int Temp=0;
volatile int Counter=0;
volatile byte PPM_Counter=0;
volatile int PWM_RAW[8] = {2400,2400,2400,2400,2400,2400,2400,2400};
int stopAll = 0;
int trottleSpeed = 1375;
int SteeringAngle = 1375;
int upDown = 1;
int randArray[4] = {200, 500, 300, 700};
int loopCount1 = 0;
int loopCount2 = 0;
int directionFlag = 0;
int i;

long timer=0;
long timer2=0;

void setup()
{
  Init_PWM1(); //OUT2&3 
  Init_PWM3(); //OUT6&7
  Init_PWM5(); //OUT0&1
  Init_PPM_PWM4(); //OUT4&5
  Serial.begin(57600);
  OutputCh(0, 1375); // Steering
  OutputCh(1, 1375); // Throttle
  delay(2500);

}
void loop()
{
    /* Throttling Range (1000-2000) () */
    /* Steering Range (1000-2000) () */
    
    // If statment to check if arduino or user is in control. Am I correc?
    if(stopAll > 0){
       // Control has been given back to the user, route the ports.
       
       OutputCh(0, InputCh(0)); // Steering
       OutputCh(1, InputCh(1)); // Throttle
       
    } else {
      // Autonomously dive in a circle.
      
       // Set steering to turn all the way left.
       OutputCh(0, 1875); // Steering
       
       // Random loop count checker, if true change motor direction to throw off turret.
       if(loopCount1 > randArray[loopCount2]){
         // Check current direction and change it to the other way and reset variables.
         if(directionFlag == 0){
           directionFlag = 1;
           loopCount1 = 0;
           loopCount2++;
           if(loopCount2 > 4){
             loopCount2 = 0;
           }
           OutputCh(1, 1300); // Throttle 
           delay(500);
           OutputCh(1, 1375); // Throttle 
           delay(500);
         } else {
           directionFlag = 0;
           loopCount1 = 0;
           loopCount2++;
           if(loopCount2 > 4){
             loopCount2 = 0;
           }
           OutputCh(1, 1450); // Throttle 
           delay(500);
           OutputCh(1, 1375); // Throttle 
           delay(500);
         }
       }
       
       // Check direction flag, send correct signal to motor.
       if(directionFlag == 0){
         OutputCh(1, 1450); // Throttle 
       } else {
         OutputCh(1, 1300); // Throttle 
       }
       
       // Increment the loop counter.
       loopCount1++;
       
       // Check if steering input is recieved from controller, if so break loop and give control back to the user.
       if((InputCh(0) > 1525) || (InputCh(0) < 1175)){
           stopAll = 99; 
       }
    }
  
  //Printing all values. Uncomment for debugging.
  if((millis()- timer) >= 250)
  {
    timer=millis();
    Serial.print("Ch0:");
    Serial.print(InputCh(0));
    Serial.print(" Ch1:");
    Serial.print(InputCh(1));
    Serial.print(" Ch2:");
    Serial.print(InputCh(2));
    Serial.print(" Ch3:");
    Serial.print(InputCh(3));
    Serial.print(" Ch4:");
    Serial.print(InputCh(4));
    Serial.print(" Ch5:");
    Serial.print(InputCh(5));
    Serial.print(" Ch6:");
    Serial.print(InputCh(6));
    Serial.print(" Ch7:");
    Serial.println(InputCh(7));
  }
  
   delay(20);
}


