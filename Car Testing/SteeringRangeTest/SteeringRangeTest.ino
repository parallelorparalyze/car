//Realeased under really Creative Commons! 
//This just a basic demo code...
//Reads and pulses the 8 channels... 
//By Jordi Munoz
#include <avr/interrupt.h>

volatile unsigned int Start_Pulse =0;
volatile unsigned int Stop_Pulse =0;
volatile unsigned int Pulse_Width =0;

volatile int Test=0;
volatile int Test2=0;
volatile int Temp=0;
volatile int Counter=0;
volatile byte PPM_Counter=0;
volatile int PWM_RAW[8] = {2400,2400,2400,2400,2400,2400,2400,2400};
int stopAll = 0;
int trottleSpeed = 1375;
int SteeringAngle = 1375;
int upDown = 1;

long timer=0;
long timer2=0;

void setup()
{
  Init_PWM1(); //OUT2&3 
  Init_PWM3(); //OUT6&7
  Init_PWM5(); //OUT0&1
  Init_PPM_PWM4(); //OUT4&5
  Serial.begin(57600);
  OutputCh(0, 1375); // Steering
  OutputCh(1, 1375); // Throttle
  delay(5000);

}
void loop()
{
    /* Throttling Range (1000-2000) () */
    /* Steering Range (1000-2000) () */
   // Serial.print("Ch0:");
   // Serial.print(InputCh(0));
   // Serial.print(" Ch1:");
   // Serial.print(InputCh(1));
    if(stopAll > 0){
       OutputCh(0, InputCh(0)); // Steering
       OutputCh(1, InputCh(1)); // Throttle
    } else {
           if ((SteeringAngle < 1875) && (upDown))
           {
             //trottleSpeed+=100;
             SteeringAngle+=100;  
             if ( trottleSpeed == 1875)
               upDown = 0;
             OutputCh(0, SteeringAngle); // Steering
             //OutputCh(1, trottleSpeed); // Throttle
             delay(1000);
           }
           else if ((SteeringAngle > 875) &&(!upDown))
           {
             //trottleSpeed-=100;
             SteeringAngle-=100;  
             if ( trottleSpeed == 875)
               upDown = 1;
             OutputCh(0, SteeringAngle); // Steering
             //OutputCh(1, trottleSpeed); // Throttle
             delay(1000);
           }
       //OutputCh(0, 1300); // Steering
       //OutputCh(1, 10); // Throttle
       //OutputCh(1, 1000); // Throttle
       /*if((InputCh(0) > 1750) || (InputCh(0) < 1250)){
           stopAll = 99; 
       }*/
       /* else if((InputCh(1) > 1600) || (InputCh(1) < 1400)){
           stopAll = 99;
       }*/
    }
     //for(int i = 0; i < 1; i++){
     //  ;
     //  
     //}
    // OutputCh(2, 1000);
    // OutputCh(3, 2000);
    // OutputCh(4, InputCh(4));
    // OutputCh(5, InputCh(5));
    // OutputCh(6, InputCh(6));
    // OutputCh(7, InputCh(7));
  
  //Printing all values. 
  if((millis()- timer) >= 250)
  {
    timer=millis();
    Serial.print("Ch0:");
    Serial.print(InputCh(0));
    Serial.print(" Ch1:");
    Serial.print(InputCh(1));
    Serial.print(" Ch2:");
    Serial.print(InputCh(2));
    Serial.print(" Ch3:");
    Serial.print(InputCh(3));
    Serial.print(" Ch4:");
    Serial.print(InputCh(4));
    Serial.print(" Ch5:");
    Serial.print(InputCh(5));
    Serial.print(" Ch6:");
    Serial.print(InputCh(6));
    Serial.print(" Ch7:");
    Serial.println(InputCh(7));
  }
   delay(20);
}


