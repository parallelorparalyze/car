#include <avr/interrupt.h>
#include <SoftwareSerial.h>
#include <Timers.h>

SoftwareSerial mySerial(10,11);

void setup()
{
  Init_PWM1(); //OUT2&3 
  Init_PWM3(); //OUT6&7
  Init_PWM5(); //OUT0&1
  Init_PPM_PWM4(); //OUT4&5
  Serial.begin(9600);
  OutputCh(0, 1375); // Steering
  OutputCh(1, 1375); // Throttle
  delay(2500);
  while(!Serial){;}

}
void loop()
{
  if(mySerial.available())
  {
    Serial.write(mySerial.read());
    mySerial.write(mySerial.read());
  }
  if(Serial.available())
  {
    Serial.write(Serial.read());
    mySerial.write(Serial.read());
  }
}


